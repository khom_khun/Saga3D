include_guard()

function(buildVEZ)
  if(NOT EXISTS "${CMAKE_CURRENT_BINARY_DIR}/v-ez")
    message(STATUS "Cloning V-EZ (master branch)")

    execute_process(COMMAND
      git clone https://github.com/GPUOpen-LibrariesAndSDKs/V-EZ.git v-ez
      WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
    )

    execute_process(COMMAND
      git submodule init WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/v-ez
    )

    execute_process(COMMAND
      git submodule update WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/v-ez
    )
  endif()

  if(NOT EXISTS "${CMAKE_CURRENT_BINARY_DIR}/v-ez/build")
    file(MAKE_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/v-ez/build")
  endif()

  message(STATUS "Updating V-EZ (master branch)")

  #execute_process(COMMAND
  #  git pull WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/v-ez
  #)

  execute_process(COMMAND ${CMAKE_COMMAND}
    ..
    -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
    -G "${CMAKE_GENERATOR}"
    -DVEZ_COMPILE_SAMPLES=OFF
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/v-ez/build
  )

  execute_process(COMMAND ${CMAKE_COMMAND}
    --build
    ${CMAKE_CURRENT_BINARY_DIR}/v-ez/build
    -j 4
  )
endfunction()
