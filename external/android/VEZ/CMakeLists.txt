add_library(Dependencies::VEZ SHARED IMPORTED GLOBAL)
target_include_directories(Dependencies::VEZ
  INTERFACE ${CMAKE_SOURCE_DIR}/android/app/imported-libs/include/VEZ
)
#set_property(TARGET Dependencies::VEZ PROPERTY IMPORTED_LOCATION ${CMAKE_CURRENT_BINARY_DIR}/v-ez/Bin/x86_64/libVEZ.so)
