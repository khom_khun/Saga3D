#include <Saga.h>
#include <chrono>

using namespace saga;
using namespace core;
using namespace video;

struct CameraMatrix {
  glm::mat4 View;
  glm::mat4 Projection;
};

int main(int argc, char* argv[]) {
  auto device = createDevice(E_DRIVER_TYPE::VULKAN, {1280, 720}, 16, false, false, false);
  device->setWindowCaption("Saga 3D Mesh");
  auto& driver = device->getVideoDriver();
  auto& smgr = device->getSceneManager();

  auto& cam = smgr->addCameraSceneNodeFPS();
  // auto& cam = smgr->addCameraSceneNode();
  cam->setPosition({0, 50, 0});
  cam->setFarValue(500); // increase view range to see further

  auto mesh = smgr->getMesh("../media/sibenik.x");
  auto texture = driver->createTexture("../media/sibenik_albedo.jpg");
  if (!mesh) return 1;
  if (!texture) return 1;
  auto node = smgr->createMeshSceneNode(mesh->getMesh(0));
  node->setTexture(0, texture);

  auto shaderInfo = driver->createShader();
  shaderInfo.VSSource = R"(
    #version 450
    layout (location = 0) in vec4 position;
    layout (location = 1) in vec2 texCoord;

    layout(binding = 0) uniform CameraMatrix
    {
      mat4 View;
      mat4 Projection;
    } camera;

    layout(binding = 1) uniform ModelMatrix
    {
      mat4 Model;
    } model;

    layout(location = 0) out vec2 uv;

    void main() {
      uv = texCoord;
      gl_Position = camera.Projection * camera.View * model.Model * position;
    }
  )";

  shaderInfo.FSSource = R"(
    #version 450

    layout(binding = 2) uniform sampler2D texture0;
    layout(location = 0) in vec2 uv;
    layout(location = 0) out vec4 fragColor;

    void main() {
      fragColor = texture(texture0, uv);
    }
  )";

  auto pipelineInfo = driver->createPipeline();
  pipelineInfo.Shaders = driver->createResource(std::move(shaderInfo));

  pipelineInfo.Layout.Attributes[1] = {
    E_ATTRIBUTE_TYPE::TEXTURE_COORDINATE,
    E_ATTRIBUTE_FORMAT::FLOAT2,
    1, 3 * sizeof(float)
  };

  auto cameraUniformInfo = driver->createShaderUniform();
  cameraUniformInfo.Size = sizeof(CameraMatrix);
  auto cameraUniform = driver->createResource(std::move(cameraUniformInfo));

  auto modelUniformInfo = driver->createShaderUniform();
  modelUniformInfo.Size = sizeof(glm::mat4);
  auto modelUniform = driver->createResource(std::move(modelUniformInfo));

  auto passInfo = driver->createRenderPass();
  auto pipeline = driver->createResource(std::move(pipelineInfo));
  node->setPipeline(pipeline);

  auto pass = driver->createResource(std::move(passInfo));
  smgr->registerSceneNode(node, pass);

  node->setOnRender([node, driver, modelUniform]() {
    auto matrix = node->getAbsoluteTransformation();
    driver->updateShaderUniform(modelUniform, &matrix);
    driver->bindShaderUniform(modelUniform, 1);
    driver->bindTexture(node->getTexture(0), 2);
  });

  auto Then = std::chrono::high_resolution_clock::now();
  while (device->run()) {
    smgr->animate();
    driver->beginPass(pass);

    CameraMatrix matrix;
    cam->updateViewMatrix();
    matrix.View = cam->getViewMatrix();
    matrix.Projection = cam->getProjectionMatrix();
    driver->updateShaderUniform(cameraUniform, &matrix);
    driver->bindShaderUniform(cameraUniform, 0);

    driver->render();
    driver->endPass();
    driver->submit();
    driver->present();

    auto Now = std::chrono::high_resolution_clock::now();
    auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(Now - Then).count();
    Then = Now;
    if (millis > 0)
    {
      auto sec = millis / 1000.f;
      int fps = 60.f / sec;
      device->setWindowCaption("Saga 3D Mesh - FPS: " + std::to_string(fps));
    }
  }

  return 0;
}

