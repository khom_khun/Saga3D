# Saga3D Samples

## [01 Window](https://gitlab.com/InnerPieceOSS/Saga3D/tree/master/samples/01_window)
Create a window and clear it to a color

![](https://i.imgur.com/hfSHc5F.png)

## [02 Triangle](https://gitlab.com/InnerPieceOSS/Saga3D/tree/master/samples/02_triangle)
Draw a triangle and color it using fragment shader

![](https://i.imgur.com/jdaSOqy.png)

## [03 Mesh](https://gitlab.com/InnerPieceOSS/Saga3D/tree/master/samples/03_mesh)
Load a mesh and do texture mapping via shader

Move your mouse to look around

![](https://i.imgur.com/vXyxfgu.png)

## [04 Multiple render targets](https://gitlab.com/InnerPieceOSS/Saga3D/tree/master/samples/04_multiple_render_targets)
Render scene to multiple textures

Then copy 3 textures to one texture and display

Because Vulkan's swapchain format is different, we use blitTexture to convert color format

Texture contents:
- Left: position texture
- Middle: albedo texture
- Right: depth texture (why this looks abnormal? depth is a single value while pixel has 4 channel values)

![](https://i.imgur.com/jQz0gkH.jpg)

## 05 Physically based rendering
