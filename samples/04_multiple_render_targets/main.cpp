#include <Saga.h>
#include <chrono>

using namespace saga;
using namespace core;
using namespace video;

struct CameraMatrix {
  glm::mat4 View;
  glm::mat4 Projection;
};

int main(int argc, char* argv[]) {
  auto device = createDevice(E_DRIVER_TYPE::VULKAN, {1280, 720}, 16, false, false, false);
  device->setWindowCaption("Saga 3D MRT");
  auto& driver = device->getVideoDriver();
  auto& smgr = device->getSceneManager();

  auto& cam = smgr->addCameraSceneNodeFPS();
  cam->setPosition({0, 50, 0});
  cam->setFarValue(500); // increase view range to see further

  auto mesh = smgr->getMesh("../media/sibenik.x");
  auto texture = driver->createTexture("../media/sibenik_albedo.jpg");
  if (!mesh) return 1;
  if (!texture) return 1;
  auto node = smgr->createMeshSceneNode(mesh->getMesh(0));
  node->setTexture(0, texture);

  auto shaderInfo = driver->createShader();
  shaderInfo.VSSource = R"(
    #version 450
    layout (location = 0) in vec4 position;
    layout (location = 1) in vec2 texCoord;

    layout(binding = 0) uniform CameraMatrix
    {
      mat4 View;
      mat4 Projection;
    } camera;

    layout(binding = 1) uniform ModelMatrix
    {
      mat4 Model;
    } model;

    layout(location = 0) out vec4 out_pos;
    layout(location = 1) out vec2 out_uv;

    void main() {
      out_uv = texCoord;
      out_pos = camera.Projection * camera.View * model.Model * position;
      gl_Position = camera.Projection * camera.View * model.Model * position;
    }
  )";

  shaderInfo.FSSource = R"(
    #version 450

    layout(binding = 2) uniform sampler2D texture0;
    layout(location = 0) in vec4 out_position;
    layout(location = 1) in vec2 out_uv;
    layout(location = 0) out vec4 position;
    layout(location = 1) out vec4 albedo;

    void main() {
      position = out_position;
      albedo = texture(texture0, out_uv);
    }
  )";

  auto pipelineInfo = driver->createPipeline();
  pipelineInfo.Shaders = driver->createResource(std::move(shaderInfo));

  pipelineInfo.Layout.Attributes[1] = {
    E_ATTRIBUTE_TYPE::TEXTURE_COORDINATE,
    E_ATTRIBUTE_FORMAT::FLOAT2,
    1, 3 * sizeof(float)
  };

  auto cameraUniformInfo = driver->createShaderUniform();
  cameraUniformInfo.Size = sizeof(CameraMatrix);
  auto cameraUniform = driver->createResource(std::move(cameraUniformInfo));

  auto modelUniformInfo = driver->createShaderUniform();
  modelUniformInfo.Size = sizeof(glm::mat4);
  auto modelUniform = driver->createResource(std::move(modelUniformInfo));

  auto passInfo = driver->createRenderPass();
  passInfo.UseDefaultAttachments = false;
  auto pipeline = driver->createResource(std::move(pipelineInfo));
  node->setPipeline(pipeline);

  auto depthTextureInfo = driver->createTexture();
  depthTextureInfo.Format = E_PIXEL_FORMAT::D32;
  depthTextureInfo.IsRenderTarget = true;
  depthTextureInfo.IsDepthAttachment = true;
  depthTextureInfo.Width = driver->getWidth();
  depthTextureInfo.Height = driver->getHeight();

  auto renderTargetInfo = driver->createTexture();
  renderTargetInfo.Format = E_PIXEL_FORMAT::R8G8B8A8;
  renderTargetInfo.Width = driver->getWidth();
  renderTargetInfo.Height = driver->getHeight();
  renderTargetInfo.IsRenderTarget = true;

  passInfo.DepthStencilAttachment = driver->createTexture(std::move(depthTextureInfo));
  passInfo.ColorAttachments[0] = driver->createTexture(STexture{renderTargetInfo});
  passInfo.ColorAttachments[1] = driver->createTexture(STexture{renderTargetInfo});

  auto resultTexture = driver->createTexture(STexture{renderTargetInfo});
  renderTargetInfo.Format = E_PIXEL_FORMAT::B8G8R8A8;
  auto screen = driver->createTexture(STexture{renderTargetInfo});

  passInfo.State.Colors[0] = {
    E_RENDER_PASS_STATE::CLEAR,
    { 0.5f, 0.5f, 0.5f, 1.f }
  };
  passInfo.State.Colors[1] = {
    E_RENDER_PASS_STATE::CLEAR,
    { 0.5f, 0.5f, 0.5f, 1.f }
  };
  passInfo.State.Depth = {
    E_RENDER_PASS_STATE::CLEAR,
    1.f
  };

  auto pass = driver->createResource(std::move(passInfo));
  smgr->registerSceneNode(node, pass);

  node->setOnRender([node, driver, modelUniform]() {
    auto matrix = node->getAbsoluteTransformation();
    driver->updateShaderUniform(modelUniform, &matrix);
    driver->bindShaderUniform(modelUniform, 1);
    driver->bindTexture(node->getTexture(0), 2);
  });

  auto Then = std::chrono::high_resolution_clock::now();
  while (device->run()) {
    smgr->animate();
    driver->beginPass(pass);

    CameraMatrix matrix;
    cam->updateViewMatrix();
    matrix.View = cam->getViewMatrix();
    matrix.Projection = cam->getProjectionMatrix();
    driver->updateShaderUniform(cameraUniform, &matrix);
    driver->bindShaderUniform(cameraUniform, 0);
    driver->render();

    driver->copyTexture(
      passInfo.ColorAttachments[0], resultTexture, {}, {},
      { driver->getWidth() / 3, driver->getHeight() }
    );
    driver->copyTexture(
      passInfo.ColorAttachments[1], resultTexture, {},
      { driver->getWidth() / 3, 0},
      { driver->getWidth() / 3, driver->getHeight() }
    );
    driver->copyTexture(
      passInfo.DepthStencilAttachment, resultTexture, {},
      { (driver->getWidth() / 3) * 2, 0},
      { driver->getWidth() / 3, driver->getHeight() }
    );
    driver->blitTexture(resultTexture, screen);
    driver->endPass();
    driver->submit();
    driver->present(screen);

    auto Now = std::chrono::high_resolution_clock::now();
    auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(Now - Then).count();
    Then = Now;
    if (millis > 0)
    {
      auto sec = millis / 1000.f;
      int fps = 60.f / sec;
      device->setWindowCaption("Saga 3D MRT - FPS: " + std::to_string(fps));
    }
  }

  return 0;
}

