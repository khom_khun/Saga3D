// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __S_3D_VERTEX_H_INCLUDED__
#define __S_3D_VERTEX_H_INCLUDED__

#include "SColor.h"

namespace saga
{
namespace scene
{

//! standard vertex used by the Irrlicht engine.
struct S3DVertex
{
  //! default constructor
  S3DVertex() {}

  S3DVertex(float x, float y, float z)
    : Pos(x, y, z) {}

  S3DVertex(const glm::vec3& pos, const glm::vec3& normal,
    glm::vec4 color, const glm::vec2& tcoords)
    : Pos(pos), Normal(normal), Color(color), TCoords(tcoords) {}

  S3DVertex(const glm::vec3& pos, const glm::vec3& normal,
    glm::vec4 color, const glm::vec2& tcoords, const glm::vec3& tangent, const glm::vec3& bitangent)
    : Pos(pos), Normal(normal), Color(color), TCoords(tcoords), Tangent(tangent), BiTangent(bitangent) {}

  glm::vec3 Pos;
  glm::vec3 Normal;
  glm::vec4 Color;
  glm::vec2 TCoords;
  glm::vec3 Tangent;
  glm::vec3 BiTangent;

  bool operator==(const S3DVertex& other) const
  {
    return ((Pos == other.Pos) && (Normal == other.Normal) &&
      (Color == other.Color) && (TCoords == other.TCoords)) &&
      (Tangent == other.Tangent) && (BiTangent == other.BiTangent);
  }

  bool operator!=(const S3DVertex& other) const
  {
    return ((Pos != other.Pos) || (Normal != other.Normal) ||
      (Color != other.Color) || (TCoords != other.TCoords)) ||
      (Tangent != other.Tangent) || (BiTangent != other.BiTangent);
  }

  bool operator<(const S3DVertex& other) const
  {
    return (glm::all(glm::lessThan(Pos, other.Pos)) ||
        ((glm::all(glm::equal(Pos, other.Pos)) && glm::all(glm::lessThan(Normal, other.Normal))) ||
        (glm::all(glm::equal(Pos, other.Pos)) && glm::all(glm::equal(Normal, other.Normal)) && glm::all(glm::lessThan(Color, other.Color))) ||
        (glm::all(glm::equal(Pos, other.Pos)) && glm::all(glm::equal(Normal, other.Normal)) && glm::all(glm::equal(Color, other.Color)) &&
        glm::all(glm::lessThan(TCoords, other.TCoords)))));
  }

  S3DVertex getInterpolated(const S3DVertex& other, float d)
  {
    d = glm::clamp(d, 0.0f, 1.0f);
    return S3DVertex(glm::mix(Pos, other.Pos, d),
        glm::mix(Normal, other.Normal, d),
        glm::mix(Color, other.Color, d),
        glm::mix(TCoords, other.TCoords, d),
        glm::mix(Tangent, other.Tangent, d),
        glm::mix(BiTangent, other.BiTangent, d));
  }
};

} // namespace video
} // namespace saga

#endif

