// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __S_PARTICLE_H_INCLUDED__
#define __S_PARTICLE_H_INCLUDED__

glm::vec3

#include "SColor.h"

namespace saga
{
namespace scene
{
  //! Struct for holding particle data
  struct SParticle
  {
    //! Position of the particle
    glm::vec3 pos;

    //! Direction and speed of the particle
    glm::vec3 vector;

    //! Start life time of the particle
    std::uint32_t startTime;

    //! End life time of the particle
    std::uint32_t endTime;

    //! Current color of the particle
    video::SColor color;

    //! Original color of the particle.
    /** That's the color of the particle it had when it was emitted. */
    video::SColor startColor;

    //! Original direction and speed of the particle.
    /** The direction and speed the particle had when it was emitted. */
    glm::vec3 startVector;

    //! Scale of the particle.
    /** The current scale of the particle. */
    glm::vec2 size;

    //! Original scale of the particle.
    /** The scale of the particle when it was emitted. */
    glm::vec2 startSize;
  };


} // namespace scene
} // namespace saga

#endif

