#ifndef __E_ATTRIBUTE_TYPES_H_INCLUDED__
#define __E_ATTRIBUTE_TYPES_H_INCLUDED__

namespace saga
{
namespace video
{

enum class E_ATTRIBUTE_TYPE {
  INVALID,
  POSITION,
  NORMAL,
  COLOR,
  TEXTURE_COORDINATE,
  TANGENT,
  BITANGENT
};

} // namespace video
} // namespace saga

#endif // __E_ATTRIBUTE_TYPES_H_INCLUDED__

