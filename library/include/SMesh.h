// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __S_MESH_H_INCLUDED__
#define __S_MESH_H_INCLUDED__

#include "IMesh.h"
#include "IMeshBuffer.h"
#include "aabbox3d.h"

namespace saga
{
namespace scene
{
  //! Simple implementation of the IMesh interface.
  struct SMesh : public IMesh
  {
    //! constructor
    SMesh()
    {

    }

    //! destructor
    virtual ~SMesh()
    {
      MeshBuffers.clear();
    }

    //! clean mesh
    virtual void clear()
    {
      MeshBuffers.clear();
      BoundingBox.reset (0.f, 0.f, 0.f);
    }

    //! returns amount of mesh buffers.
    virtual std::uint32_t getMeshBufferCount() const override
    {
      return MeshBuffers.size();
    }

    //! returns pointer to a mesh buffer
    virtual IMeshBuffer& getMeshBuffer(std::uint32_t nr = 0) const override
    {
      return *MeshBuffers.at(nr);
    }

    //! returns an axis aligned bounding box
    virtual const core::aabbox3d<float>& getBoundingBox() const override
    {
      return BoundingBox;
    }

    //! set user axis aligned bounding box
    virtual void setBoundingBox(const core::aabbox3df& box) override
    {
      BoundingBox = box;
    }

    //! recalculates the bounding box
    void recalculateBoundingBox()
    {
      bool hasMeshBufferBBox = false;
      for (std::uint32_t i = 0; i < MeshBuffers.size(); ++i)
      {
        const core::aabbox3df& bb = MeshBuffers[i]->getBoundingBox();
        if (!bb.isEmpty())
        {
          if (!hasMeshBufferBBox)
          {
            hasMeshBufferBBox = true;
            BoundingBox = bb;
          }
          else
          {
            BoundingBox.addInternalBox(bb);
          }

        }
      }

      if (!hasMeshBufferBBox)
        BoundingBox.reset(0.0f, 0.0f, 0.0f);
    }

    //! adds a MeshBuffer
    /** The bounding box is not updated automatically. */
    void addMeshBuffer(std::unique_ptr<IMeshBuffer> buf)
    {
      if (buf)
      {
        MeshBuffers.push_back(std::move(buf));
      }
    }

    //! The meshbuffers of this mesh
    std::vector<std::unique_ptr<IMeshBuffer>> MeshBuffers;

    //! The bounding box of this mesh
    core::aabbox3d<float> BoundingBox;
  };


} // namespace scene
} // namespace saga

#endif

