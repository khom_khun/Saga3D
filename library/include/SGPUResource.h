#ifndef __SGPU_RESOURCE_H_INCLUDED__
#define __SGPU_RESOURCE_H_INCLUDED__

#include <string>

namespace saga
{
namespace video 
{

  struct SGPUResource
  {
    using HandleType = std::uint64_t;
    HandleType Handle;
  };

  constexpr SGPUResource::HandleType NULL_GPU_RESOURCE_HANDLE = 0;

} // namespace scene
} // namespace saga

#endif // __SGPU_RESOURCE_H_INCLUDED__
