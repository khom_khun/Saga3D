#ifndef __E_PIXEL_FORMAT_H_INCLUDED__
#define __E_PIXEL_FORMAT_H_INCLUDED__

namespace saga
{
namespace video
{

  //! Enumeration for all primitive types there are.
  enum class E_PIXEL_FORMAT
  {
    //! 16 bit color format used by the software driver.
    /** It is thus preferred by all other irrlicht engine video drivers.
    There are 5 bits for every color component, and a single bit is left
    for alpha information. */
    A1R5G5B5 = 0,

    //! Standard 16 bit color format.
    R5G6B5,

    //! 24 bit color, no alpha channel, but 8 bit for red, green and blue.
    R8G8B8,

    //! Default 32 bit color format. 8 bits are used for every component: red, green, blue and alpha.
    R8G8B8A8,

    //! 32 bit color format for displaying. 8 bits are used for every component: red, green, blue and alpha.
    B8G8R8A8,

    /** Compressed image formats. **/

    //! DXT1 color format.
    DXT1,

    //! DXT2 color format.
    DXT2,

    //! DXT3 color format.
    DXT3,

    //! DXT4 color format.
    DXT4,

    //! DXT5 color format.
    DXT5,

    //! PVRTC RGB 2bpp.
    PVRTC_RGB2,

    //! PVRTC ARGB 2bpp.
    PVRTC_ARGB2,

    //! PVRTC RGB 4bpp.
    PVRTC_RGB4,

    //! PVRTC ARGB 4bpp.
    PVRTC_ARGB4,

    //! PVRTC2 ARGB 2bpp.
    PVRTC2_ARGB2,

    //! PVRTC2 ARGB 4bpp.
    PVRTC2_ARGB4,

    //! ETC1 RGB.
    ETC1,

    //! ETC2 RGB.
    ETC2_RGB,

    //! ETC2 ARGB.
    ETC2_ARGB,

    /** The following formats may only be used for render target textures. */

    /** Floating point formats. */

    //! 16 bit format using 16 bits for the red channel.
    R16F,

    //! 32 bit format using 16 bits for the red and green channels.
    G16R16F,

    //! 64 bit format using 16 bits for the red, green, blue and alpha channels.
    A16B16G16R16F,

    //! 32 bit format using 32 bits for the red channel.
    R32F,

    //! 64 bit format using 32 bits for the red and green channels.
    G32R32F,

    //! 128 bit format using 32 bits for the red, green, blue and alpha channels.
    A32B32G32R32F,

    /** Unsigned normalized integer formats. */

    //! 8 bit format using 8 bits for the red channel.
    R8,

    //! 16 bit format using 8 bits for the red and green channels.
    R8G8,

    //! 16 bit format using 16 bits for the red channel.
    R16,

    //! 32 bit format using 16 bits for the red and green channels.
    R16G16,

    /** Depth and stencil formats. */

    //! 16 bit format using 16 bits for depth.
    D16,

    //! 32 bit format using 32 bits for depth.
    D32,

    //! 32 bit format using 24 bits for depth and 8 bits for stencil.
    D24S8,

    //! Unknown color format:
    UNKNOWN
  };

} // namespace scene
} // namespace saga

#endif

