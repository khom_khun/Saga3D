#ifndef __E_SAMPLER_ADDRESS_MODES_H_INCLUDED__
#define __E_SAMPLER_ADDRESS_MODES_H_INCLUDED__

namespace saga
{
namespace video
{

enum class E_SAMPLER_ADDRESS_MODE {
  REPEAT,
  MIRRORED_REPEAT,
  CLAMP_TO_EDGE,
  CLAMP_TO_BORDER,
  MIRROR_CLAMP_TO_EDGE
};

} // namespace video
} // namespace saga

#endif // __E_SAMPLER_ADDRESS_MODES_H_INCLUDED__

