// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __I_FILE_SYSTEM_H_INCLUDED__
#define __I_FILE_SYSTEM_H_INCLUDED__

#include <string>

namespace saga
{
namespace video
{
  class IVideoDriver;
} // namespace video
namespace io
{

class IReadFile;
class IWriteFile;
class IFileList;
class IXMLWriter;
class IAttributes;


//! The FileSystem manages files and archives and provides access to them.
/** It manages where files are, so that modules which use the the IO do not
need to know where every file is located. A file could be in a .zip-Archive or
as file on disk, using the IFileSystem makes no difference to this. */
class IFileSystem
{
public:

  //! Opens a file for read access.
  /** \param filename: Name of file to open.
  \return Pointer to the created file interface.
  The returned pointer should be dropped when no longer needed.
  See IReferenceCounted::drop() for more information. */
  virtual IReadFile* createAndOpenFile(const std::string& filename) = 0;

  //! Creates an IReadFile interface for accessing memory like a file.
  /** This allows you to use a pointer to memory where an IReadFile is requested.
  \param memory: A pointer to the start of the file in memory
  \param len: The length of the memory in bytes
  \param fileName: The name given to this file
  \param deleteMemoryWhenDropped: True if the memory should be deleted
  along with the IReadFile when it is dropped.
  \return Pointer to the created file interface.
  The returned pointer should be dropped when no longer needed.
  See IReferenceCounted::drop() for more information.
  */
  virtual IReadFile* createMemoryReadFile(const void* memory, std::int32_t len, const std::string& fileName, bool deleteMemoryWhenDropped=false) = 0;

  //! Creates an IReadFile interface for accessing files inside files.
  /** This is useful e.g. for archives.
  \param fileName: The name given to this file
  \param alreadyOpenedFile: Pointer to the enclosing file
  \param pos: Start of the file inside alreadyOpenedFile
  \param areaSize: The length of the file
  \return A pointer to the created file interface.
  The returned pointer should be dropped when no longer needed.
  See IReferenceCounted::drop() for more information.
  */
  virtual IReadFile* createLimitReadFile(const std::string& fileName,
      IReadFile* alreadyOpenedFile, long pos, long areaSize) = 0;

  //! Creates an IWriteFile interface for accessing memory like a file.
  /** This allows you to use a pointer to memory where an IWriteFile is requested.
    You are responsible for allocating enough memory.
  \param memory: A pointer to the start of the file in memory (allocated by you)
  \param len: The length of the memory in bytes
  \param fileName: The name given to this file
  \param deleteMemoryWhenDropped: True if the memory should be deleted
  along with the IWriteFile when it is dropped.
  \return Pointer to the created file interface.
  The returned pointer should be dropped when no longer needed.
  See IReferenceCounted::drop() for more information.
  */
  virtual IWriteFile* createMemoryWriteFile(void* memory, std::int32_t len, const std::string& fileName, bool deleteMemoryWhenDropped=false) = 0;

  //! Changes the search order of attached archives.
  /**
  \param sourceIndex: The index of the archive to change
  \param relative: The relative change in position, archives with a lower index are searched first */
  virtual bool moveFileArchive(std::uint32_t sourceIndex, std::int32_t relative) = 0;

  //! Get the current working directory.
  /** \return Current working directory as a string. */
  virtual const std::string& getWorkingDirectory() = 0;

  //! Changes the current working directory.
  /** \param newDirectory: A string specifying the new working directory.
  The string is operating system dependent. Under Windows it has
  the form "<drive>:\<directory>\<sudirectory>\<..>". An example would be: "C:\Windows\"
  \return True if successful, otherwise false. */
  virtual bool changeWorkingDirectoryTo(const std::string& newDirectory) = 0;

  //! Converts a relative std::string to an absolute (unique) std::string, resolving symbolic links if required
  /** \param filename Possibly relative file or directory name to query.
  \result Absolute filename which points to the same file. */
  virtual std::string getAbsolutePath(const std::string& filename) const = 0;

  //! Get the directory a file is located in.
  /** \param filename: The file to get the directory from.
  \return String containing the directory of the file. */
  virtual std::string getFileDir(const std::string& filename) const = 0;

  //! Get the base part of a filename, i.e. the name without the directory part.
  /** If no directory is prefixed, the full name is returned.
  \param filename: The file to get the basename from
  \param keepExtension True if filename with extension is returned otherwise everything
  after the final '.' is removed as well. */
  virtual std::string getFileBasename(const std::string& filename, bool keepExtension=true) const = 0;

  //! flatten a std::string and file name for example: "/you/me/../." becomes "/you"
  virtual std::string& flattenFilename(std::string& directory, const std::string& root="/") const = 0;

  //! Get the relative filename, relative to the given directory
  virtual std::string getRelativeFilename(const std::string& filename, const std::string& directory) const = 0;

  //! Creates a list of files and directories in the current working directory and returns it.
  /** \return a Pointer to the created IFileList is returned. After the list has been used
  it has to be deleted using its IFileList::drop() method.
  See IReferenceCounted::drop() for more information. */
  virtual IFileList* createFileList() = 0;

  //! Creates an empty filelist
  /** \return a Pointer to the created IFileList is returned. After the list has been used
  it has to be deleted using its IFileList::drop() method.
  See IReferenceCounted::drop() for more information. */
  virtual IFileList* createEmptyFileList(const std::string& path, bool ignoreCase, bool ignorePaths) = 0;

  //! Determines if a file exists and could be opened.
  /** \param filename is the string identifying the file which should be tested for existence.
  \return True if file exists, and false if it does not exist or an error occurred. */
  virtual bool existFile(const std::string& filename) const = 0;

  //! Creates a new empty collection of attributes, usable for serialization and more.
  /** \param driver: Video driver to be used to load textures when specified as attribute values.
  Can be null to prevent automatic texture loading by attributes.
  \return Pointer to the created object.
  If you no longer need the object, you should call IAttributes::drop().
  See IReferenceCounted::drop() for more information. */
  virtual IAttributes* createEmptyAttributes(video::IVideoDriver* driver= 0) = 0;
};


} // namespace io
} // namespace saga

#endif

