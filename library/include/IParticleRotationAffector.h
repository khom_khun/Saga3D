// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __I_PARTICLE_ROTATION_AFFECTOR_H_INCLUDED__
#define __I_PARTICLE_ROTATION_AFFECTOR_H_INCLUDED__

#include "IParticleAffector.h"

namespace saga
{
namespace scene
{

//! A particle affector which rotates the particle system.
class IParticleRotationAffector : public IParticleAffector
{
public:

  //! Set the point that particles will rotate around
  virtual void setPivotPoint(const glm::vec3& point) = 0;

  //! Set the speed in degrees per second in all 3 dimensions
  virtual void setSpeed(const glm::vec3& speed) = 0;

  //! Get the point that particles are attracted to
  virtual const glm::vec3& getPivotPoint() const = 0;

  //! Get the speed in degrees per second in all 3 dimensions
  virtual const glm::vec3& getSpeed() const = 0;

  //! Get emitter type
  virtual E_PARTICLE_AFFECTOR_TYPE getType() const { return EPAT_ROTATE; }
};

} // namespace scene
} // namespace saga


#endif // __I_PARTICLE_ROTATION_AFFECTOR_H_INCLUDED__

