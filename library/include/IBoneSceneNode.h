// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __I_BONE_SCENE_NODE_H_INCLUDED__
#define __I_BONE_SCENE_NODE_H_INCLUDED__

#include "ISceneNode.h"

namespace saga
{
namespace scene
{

  //! Enumeration for different bone animation modes
  enum class E_BONE_ANIMATION_MODE
  {
    //! The bone is usually animated, unless it's parent is not animated
    AUTOMATIC,

    //! The bone is animated by the skin, if it's parent is not animated then animation will resume from this bone onward
    ANIMATED,

    //! The bone is not animated by the skin
    UNANIMATED,
  };

  enum class E_BONE_SKINNING_SPACE
  {
    //! local skinning, standard
    LOCAL,

    //! global skinning
    GLOBAL
  };

  //! Interface for bones used for skeletal animation.
  /** Used with ISkinnedMesh and IAnimatedMeshSceneNode. */
  class IBoneSceneNode : public ISceneNode
  {
  public:

    IBoneSceneNode(const std::shared_ptr<ISceneNode>& parent, const std::shared_ptr<ISceneManager>& mgr) :
      ISceneNode(parent, mgr), positionHint(-1), scaleHint(-1), rotationHint(-1) { }

    //! Get the index of the bone
    virtual std::uint32_t getBoneIndex() const = 0;

    //! Sets the animation mode of the bone.
    /** \return True if successful. (Unused) */
    virtual bool setAnimationMode(E_BONE_ANIMATION_MODE mode) = 0;

    //! Gets the current animation mode of the bone
    virtual E_BONE_ANIMATION_MODE getAnimationMode() const = 0;

    //! Get the axis aligned bounding box of this node
    virtual const core::aabbox3d<float>& getBoundingBox() const = 0;

    //! Returns the relative transformation of the scene node.
    //virtual glm::mat4 getRelativeTransformation() const = 0;

    //! The animation method.
    virtual void OnAnimate(std::uint32_t timeMs) = 0;

    //! How the relative transformation of the bone is used
    virtual void setSkinningSpace(E_BONE_SKINNING_SPACE space) = 0;

    //! How the relative transformation of the bone is used
    virtual E_BONE_SKINNING_SPACE getSkinningSpace() const = 0;

    //! Updates the absolute position based on the relative and the parents position
    virtual void updateAbsolutePositionOfAllChildren()= 0;

    std::int32_t positionHint;
    std::int32_t scaleHint;
    std::int32_t rotationHint;
  };


} // namespace scene
} // namespace saga

#endif

