#ifndef __SSHADER_H_INCLUDED__
#define __SSHADER_H_INCLUDED__

#include "SGPUResource.h"
#include "GraphicsConstants.h"
#include <array>
#include <string>

namespace saga
{
namespace video 
{

  struct SShader : public SGPUResource
  {
    std::string VSSource;
    std::string GSSource;
    std::string FSSource;
    std::string CSource;
  };

  using ShaderHandle = SGPUResource::HandleType;

} // namespace scene
} // namespace saga

#endif // __SSHADER_H_INCLUDED__

