#ifndef __DRAW_STATE_H_INCLUDED__
#define __DRAW_STATE_H_INCLUDED__

#include "Pipeline.h"
#include "Texture.h"

namespace saga
{
namespace video
{

  struct DrawState
  {
    Pipeline::HandleType Pipeline;
    // TODO: add vertex buffer, index buffer for draw state
    Texture::HandleType VSImages[MAX_SHADER_IMAGES];
    Texture::HandleType PSImages[MAX_SHADER_IMAGES];
  };

} // namespace scene
} // namespace saga

#endif

