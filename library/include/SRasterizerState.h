#ifndef __SRASTERIZER_STATE_H_INCLUDED__
#define __SRASTERIZER_STATE_H_INCLUDED__

#include "ECullMode.h"

namespace saga
{
namespace video
{
  struct SRasterizerState
  {
    E_CULL_MODE CullMode;
    int SampleCount;
  };

} // namespace scene
} // namespace saga

#endif // __SRASTERIZER_STATE_H_INCLUDED__

