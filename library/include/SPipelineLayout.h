#ifndef __SPIPELINE_LAYOUT_H_INCLUDED__
#define __SPIPELINE_LAYOUT_H_INCLUDED__

#include "SGPUResource.h"
#include "GraphicsConstants.h"
#include "EAttributeTypes.h"
#include "EAttributeFormats.h"
#include <array>

namespace saga
{
namespace video
{
  struct SVertexAttribute
  {
    SVertexAttribute() : Type(E_ATTRIBUTE_TYPE::INVALID) {}
    SVertexAttribute(E_ATTRIBUTE_TYPE type, E_ATTRIBUTE_FORMAT format,
      std::uint32_t binding, std::uint32_t offset)
      : Type(type), Format(format), Binding(binding), Offset(offset) {}

    SVertexAttribute& operator=(const SVertexAttribute& other) = default;

    E_ATTRIBUTE_TYPE Type;
    E_ATTRIBUTE_FORMAT Format;
    std::uint32_t Binding = 0;
    std::uint32_t Offset = 0;
  };

  struct SPipelineLayout : public SGPUResource
  {
    SPipelineLayout()
    {
      Attributes[0] = {
        E_ATTRIBUTE_TYPE::POSITION,
        E_ATTRIBUTE_FORMAT::FLOAT3,
        0, 0
      };
    }
   std::array<SVertexAttribute, MAX_VERTEX_ATTRIBUTES> Attributes;
  };

} // namespace scene
} // namespace saga

#endif // __SPIPELINE_LAYOUT_H_INCLUDED__
