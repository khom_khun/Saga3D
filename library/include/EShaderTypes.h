#ifndef __E_SHADER_TYPES_H_INCLUDED__
#define __E_SHADER_TYPES_H_INCLUDED__



namespace saga
{
namespace video
{

enum class E_SHADER_TYPE {
  UNKNOWN,
  VERTEX,
  GEOMETRY,
  FRAGMENT,
  COMPUTE
};

} // namespace video
} // namespace saga

#endif // __E_SHADER_TYPES_H_INCLUDED__

