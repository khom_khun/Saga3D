#ifndef __E_TEXTURE_TYPES_H_INCLUDED__
#define __E_TEXTURE_TYPES_H_INCLUDED__

namespace saga
{
namespace video 
{

  //! Enumeration for all primitive types there are.
  enum class E_TEXTURE_TYPE
  {
    //! 2D texture.
    TEXTURE_2D,

    //! Cubemap texture.
    CUBE_MAP
};

} // namespace scene
} // namespace saga

#endif

