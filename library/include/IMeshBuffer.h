// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __I_MESH_BUFFER_H_INCLUDED__
#define __I_MESH_BUFFER_H_INCLUDED__

#include "aabbox3d.h"
#include "S3DVertex.h"
#include "EPrimitiveTypes.h"
#include "SRenderPass.h"
#include "SPipelineLayout.h"
#include <vector>

namespace saga
{
namespace scene
{
  //! Struct for holding a mesh with a single material.
  /** A part of an IMesh which has the same material on each face of that
  group. Logical groups of an IMesh need not be put into separate mesh
  buffers, but can be. Separately animated parts of the mesh must be put
  into separate mesh buffers.
  Some mesh buffer implementations have limitations on the number of
  vertices the buffer can hold. In that case, logical grouping can help.
  Moreover, the number of vertices should be optimized for the GPU upload,
  which often depends on the type of gfx card. Typical figures are
  1000-10000 vertices per buffer.
  SMeshBuffer is a simple implementation of a MeshBuffer, which supports
  up to 65535 vertices.

  Since meshbuffers are used for drawing, and hence will be exposed
  to the driver, chances are high that they are grab()'ed from somewhere.
  It's therefore required to dynamically allocate meshbuffers which are
  passed to a video driver and only drop the buffer once it's not used in
  the current code block anymore.
  */
  class IMeshBuffer
  {
  public:
    virtual ~IMeshBuffer() {}

    //! Returns internal ID to identify mesh buffer
    /** Each mesh buffer will be assigned an unique ID in its creation */
    virtual std::uint64_t getID() const = 0;

    //! Generate GPU-compatible data
    /** Depending on render pass's pipeline layout, this function generates vertex data for that pass */
    virtual void buildBuffer(const video::RenderPassHandle pass, const video::SPipelineLayout& layout) = 0;

    //! Get pointer to GPU staging buffer
    /** \return Pointer to staging buffer. */
    virtual void* getData(const video::RenderPassHandle pass) = 0;

    //! Get size of GPU staging buffer
    /** \return Size of staging buffer. */
    virtual std::size_t getDataSize(const video::RenderPassHandle pass) = 0;

    //! Get access to vertex data. The data is an array of vertices.
    /** Which vertex type is used can be determined by getVertexType().
    \return Pointer to array of vertices. */
    virtual void* getVertices() = 0;
    
    //! Get amount of vertices in meshbuffer.
    /** \return Number of vertices in this buffer. */
    virtual std::uint32_t getVertexCount() const = 0;

    //! Get access to indices.
    /** \return Pointer to indices array. */
    virtual const std::uint32_t* getIndices() const = 0;

    //! Get access to indices.
    /** \return Pointer to indices array. */
    virtual std::uint32_t* getIndices() = 0;

    //! Get amount of indices in this meshbuffer.
    /** \return Number of indices in this buffer. */
    virtual std::uint32_t getIndexCount() const = 0;

    //! Get the axis aligned bounding box of this meshbuffer.
    /** \return Axis aligned bounding box of this buffer. */
    virtual const core::aabbox3df& getBoundingBox() const = 0;

    //! Set axis aligned bounding box
    /** \param box User defined axis aligned bounding box to use
    for this buffer. */
    virtual void setBoundingBox(const core::aabbox3df& box) = 0;

    //! Recalculates the bounding box. Should be called if the mesh changed.
    virtual void recalculateBoundingBox() = 0;

    //! returns position of vertex i
    virtual const glm::vec3& getPosition(std::uint32_t i) const = 0;

    //! returns normal of vertex i
    virtual const glm::vec3& getNormal(std::uint32_t i) const = 0;

    //! returns texture coord of vertex i
    virtual const glm::vec2& getTCoords(std::uint32_t i) const = 0;

    //! returns tangent of vertex i
    virtual const glm::vec3& getTangent(std::uint32_t i) const = 0;

    //! returns bi-tangent of vertex i
    virtual const glm::vec3& getBiTangent(std::uint32_t i) const = 0;

    //! Append the vertices and indices to the current buffer
    /** Only works for compatible vertex types.
    \param vertices Pointer to a vertex array.
    \param numVertices Number of vertices in the array.
    \param indices Pointer to index array.
    \param numIndices Number of indices in array. */
    virtual void append(std::vector<S3DVertex>&& vertices, std::uint32_t numPositionBuffer,
      std::vector<uint32_t>&& indices, std::uint32_t numIndices) = 0;

    //! Append the meshbuffer to the current buffer
    /** Only works for compatible vertex types
    \param other Buffer to append to this one. */
    virtual void append(const IMeshBuffer* const other) = 0;

    //! Describe what kind of primitive geometry is used by the meshbuffer
    /** Note: Default is E_PRIMITIVE_TYPE::TRIANGLES. Using other types is fine for rendering.
    But meshbuffer manipulation functions might expect type E_PRIMITIVE_TYPE::TRIANGLES
    to work correctly. Also mesh writers will generally fail (badly!) with other
    types than E_PRIMITIVE_TYPE::TRIANGLES. */
    virtual void setPrimitiveType(scene::E_PRIMITIVE_TYPE type) = 0;

    //! Get the kind of primitive geometry which is used by the meshbuffer
    virtual scene::E_PRIMITIVE_TYPE getPrimitiveType() const = 0;

    //! Calculate how many geometric primitives are used by this meshbuffer
    virtual std::uint32_t getPrimitiveCount() const
    {
      std::uint32_t indexCount = getIndexCount();
      switch (getPrimitiveType())
      {
        case scene::E_PRIMITIVE_TYPE::POINTS:         return indexCount;
        case scene::E_PRIMITIVE_TYPE::LINE_STRIP:     return indexCount-1;
        case scene::E_PRIMITIVE_TYPE::LINE_LOOP:      return indexCount;
        case scene::E_PRIMITIVE_TYPE::LINES:          return indexCount/2;
        case scene::E_PRIMITIVE_TYPE::TRIANGLE_STRIP: return (indexCount-2);
        case scene::E_PRIMITIVE_TYPE::TRIANGLE_FAN:   return (indexCount-2);
        case scene::E_PRIMITIVE_TYPE::TRIANGLES:      return indexCount/3;
        case scene::E_PRIMITIVE_TYPE::QUAD_STRIP:     return (indexCount-2)/2;
        case scene::E_PRIMITIVE_TYPE::QUADS:          return indexCount/4;
        case scene::E_PRIMITIVE_TYPE::POLYGON:        return indexCount; // (not really primitives, that would be 1, works like line_strip)
        case scene::E_PRIMITIVE_TYPE::POINT_SPRITES:  return indexCount;
      }
      return 0;
    }

  };

} // namespace scene
} // namespace saga

#endif
