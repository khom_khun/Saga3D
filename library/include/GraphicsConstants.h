#ifndef __GRAPHICS_CONSTANTS_H_INCLUDED__
#define __GRAPHICS_CONSTANTS_H_INCLUDED__

namespace saga
{
namespace video 
{

  constexpr int CUBE_FACE_COUNT = 6;
  constexpr int MAX_MIPMAPS = 16;
  constexpr int MAX_SHADER_IMAGES = 8;
  constexpr int MAX_COLOR_ATTACHMENTS = 4;
  constexpr int MAX_VERTEX_ATTRIBUTES = 4;

} // namespace video

namespace scene
{

  constexpr int TRANSFORM_FRUSTUM_COUNT = 2;
  constexpr int VIEW_FRUSTUM_PLANE_COUNT = 6;

} // namespace scene

} // namespace saga

#endif

