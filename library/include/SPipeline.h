#ifndef __SPIPELINE_H_INCLUDED__
#define __SPIPELINE_H_INCLUDED__

#include "SShader.h"
#include "EPrimitiveTypes.h"
#include "SBlendState.h"
#include "SDepthStencilState.h"
#include "SRasterizerState.h"
#include "SPipelineLayout.h"

namespace saga
{
namespace video
{
  struct SPipeline : public SGPUResource
  {
    SPipeline()
      :  Rasterizer({ E_CULL_MODE::BACK_FACE }),
      DepthStencil({ E_COMPARE_FUNC::LESS_EQUAL, true }) {}

    ShaderHandle Shaders;
    scene::E_PRIMITIVE_TYPE PrimitiveType;
    SRasterizerState Rasterizer;
    SDepthStencilState DepthStencil;
    SBlendState Blend;
    SPipelineLayout Layout;
  };

  using PipelineHandle = SGPUResource::HandleType;

} // namespace scene
} // namespace saga

#endif // __SPIPELINE_H_INCLUDED__

