// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __I_PARTICLE_EMITTER_H_INCLUDED__
#define __I_PARTICLE_EMITTER_H_INCLUDED__

#include "IAttributeExchangingObject.h"
#include "SParticle.h"

namespace saga
{
namespace scene
{

//! Types of built in particle emitters
enum class E_PARTICLE_EMITTER_TYPE
{
  EPET_POINT = 0,
  EPET_ANIMATED_MESH,
  EPET_BOX,
  EPET_CYLINDER,
  EPET_MESH,
  EPET_RING,
  EPET_SPHERE,
  EPET_COUNT
};

//! Names for built in particle emitters
const char* const ParticleEmitterTypeNames[] =
{
  "Point",
  "AnimatedMesh",
  "Box",
  "Cylinder",
  "Mesh",
  "Ring",
  "Sphere",
  0
};

//! A particle emitter for using with particle systems.
/** A Particle emitter emits new particles into a particle system.
*/
class IParticleEmitter : public virtual io::IAttributeExchangingObject
{
public:

  //! Prepares an array with new particles to emit into the system
  /** \param now Current time.
  \param timeSinceLastCall Time elapsed since last call, in milliseconds.
  \param outArray Pointer which will point to the array with the new
  particles to add into the system.
  \return Amount of new particles in the array. Can be 0. */
  virtual std::int32_t emitt(std::uint32_t now, std::uint32_t timeSinceLastCall, SParticle*& outArray) = 0;

  //! Set direction the emitter emits particles
  virtual void setDirection(const glm::vec3& newDirection) = 0;

  //! Set minimum number of particles the emitter emits per second
  virtual void setMinParticlesPerSecond(std::uint32_t minPPS) = 0;

  //! Set maximum number of particles the emitter emits per second
  virtual void setMaxParticlesPerSecond(std::uint32_t maxPPS) = 0;

  //! Set minimum starting color for particles
  virtual void setMinStartColor(const video::SColor& color) = 0;

  //! Set maximum starting color for particles
  virtual void setMaxStartColor(const video::SColor& color) = 0;

  //! Set the maximum starting size for particles
  virtual void setMaxStartSize(const glm::vec2& size) = 0;

  //! Set the minimum starting size for particles
  virtual void setMinStartSize(const glm::vec2& size) = 0;

  //! Set the minimum particle life-time in milliseconds
  virtual void setMinLifeTime(std::uint32_t lifeTimeMin) = 0;

  //! Set the maximum particle life-time in milliseconds
  virtual void setMaxLifeTime(std::uint32_t lifeTimeMax) = 0;

  //! Set maximal random derivation from the direction
  virtual void setMaxAngleDegrees(std::int32_t maxAngleDegrees) = 0;

  //! Get direction the emitter emits particles
  virtual const glm::vec3& getDirection() const = 0;

  //! Get the minimum number of particles the emitter emits per second
  virtual std::uint32_t getMinParticlesPerSecond() const = 0;

  //! Get the maximum number of particles the emitter emits per second
  virtual std::uint32_t getMaxParticlesPerSecond() const = 0;

  //! Get the minimum starting color for particles
  virtual const video::SColor& getMinStartColor() const = 0;

  //! Get the maximum starting color for particles
  virtual const video::SColor& getMaxStartColor() const = 0;

  //! Get the maximum starting size for particles
  virtual const glm::vec2& getMaxStartSize() const = 0;

  //! Get the minimum starting size for particles
  virtual const glm::vec2& getMinStartSize() const = 0;

  //! Get the minimum particle life-time in milliseconds
  virtual std::uint32_t getMinLifeTime() const = 0;

  //! Get the maximum particle life-time in milliseconds
  virtual std::uint32_t getMaxLifeTime() const = 0;

  //! Get maximal random derivation from the direction
  virtual std::int32_t getMaxAngleDegrees() const = 0;

  //! Get emitter type
  virtual E_PARTICLE_EMITTER_TYPE getType() const { return EPET_POINT; }
};

typedef IParticleEmitter IParticlePointEmitter;

} // namespace scene
} // namespace saga


#endif

