// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __I_EVENT_RECEIVER_H_INCLUDED__
#define __I_EVENT_RECEIVER_H_INCLUDED__

#include <SDL2/SDL_events.h>

namespace saga
{

//! Interface of an object which can receive events (SDL_Event*)
/** If you want to subscribe to events, inherit IEventReceiver
and call SagaDevice::addEventReceiver.
*/
class IEventReceiver
{
public:
  //! Destructor
  virtual ~IEventReceiver() {}

  //! Called when an SDL event is fired
  virtual void OnEvent(const SDL_Event& event) = 0;
};

} // namespace saga

#endif // __I_EVENT_RECEIVER_H_INCLUDED__

