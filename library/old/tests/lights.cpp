// Copyright (C) 2008-2012 Christian Stehno, Colin MacDonald
// No rights reserved: this software is in the public domain.

#include "testUtils.h"

using namespace irr;

static bool testLightTypes(video::E_DRIVER_TYPE driverType)
{
  IrrlichtDevice *device = createDevice (driverType, core::dimension2d<u32>(256,128));
  if (!device)
    return true; // No error if device does not exist

  video::IVideoDriver* driver = device->getVideoDriver();
  scene::ISceneManager* smgr = device->getSceneManager();
  if (!driver->getDriverAttributes().getAttributeAsInt("MaxLights"))
  {
    device->closeDevice();
    device->run();
    device->drop();
    return true;
  }

  stabilizeScreenBackground(driver);

  logTestString("Testing driver %ls\n", driver->getName());

//  smgr->setAmbientLight(video::SColorf(0.3f,0.3f,0.3f));
  scene::ICameraSceneNode* cam = smgr->addCameraSceneNode();
  cam->setPosition(glm::vec3(0,200,0));
  cam->setTarget(glm::vec3());
  smgr->addAnimatedMeshSceneNode(device->getSceneManager()->addHillPlaneMesh("plane", glm::vec2(4,4), glm::uvec2(128,128)));
  scene::ILightSceneNode* light1 = smgr->addLightSceneNode(0, glm::vec3(-100,30,-100));
  light1->setLightType(video::E_LIGHT_TYPE::POINT);
  light1->setRadius(100.f);
  light1->getLightData().DiffuseColor.set(0,1,1);
//  smgr->addCubeSceneNode(10, light1)->setMaterialFlag(video::E_MATERIAL_FLAG::LIGHTING, false);
  scene::ILightSceneNode* light2 = smgr->addLightSceneNode(0, glm::vec3(100,30,100));
  light2->setRotation(glm::vec3(90,0,0));
  light2->setLightType(video::E_LIGHT_TYPE::SPOT);
  light2->setRadius(100.f);
  light2->getLightData().DiffuseColor.set(1,0,0);
  light2->getLightData().InnerCone=10.f;
  light2->getLightData().OuterCone=30.f;
//  smgr->addCubeSceneNode(10, light2)->setMaterialFlag(video::E_MATERIAL_FLAG::LIGHTING, false);
  scene::ILightSceneNode* light3 = smgr->addLightSceneNode();
  light3->setRotation(glm::vec3(15,0,0));
  light3->setLightType(video::E_LIGHT_TYPE::DIRECTIONAL);
  light1->getLightData().DiffuseColor.set(0,1,0);

  driver->beginScene(video::E_CLEAR_BUFFER_FLAG::COLOR | video::E_CLEAR_BUFFER_FLAG::DEPTH, video::SColor(0));
  smgr->drawAll();
  driver->endScene();

  const bool result = takeScreenshotAndCompareAgainstReference(driver, "-lightType.png", 99.91f);

  device->closeDevice();
  device->run();
  device->drop();

  return result;
}

bool lights(void)
{
  bool result = true;
  // no lights in sw renderer
  TestWithAllDrivers(testLightTypes);
  return result;
}
