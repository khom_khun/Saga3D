// Copyright (C) 2006-2012 Luke Hoschke
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

// B3D Mesh loader
// File format designed by Mark Sibly for the Blitz3D engine and has been
// declared public domain



#ifndef SB3DSTRUCTS_H
#define SB3DSTRUCTS_H

#include "SMaterial.h"

namespace saga {
namespace scene {

struct SB3dChunkHeader
{
  char name[4];
  std::int32_t size;
};

struct SB3dChunk
{
  SB3dChunk(const SB3dChunkHeader& header, long sp)
    : length(header.size+8), startposition(sp)
  {
    name[0]=header.name[0];
    name[1]=header.name[1];
    name[2]=header.name[2];
    name[3]=header.name[3];
  }
  char name[4];
  std::int32_t length;
  long startposition;
};

struct SB3dTexture
{
  std::string TextureName;
  std::int32_t Flags;
  std::int32_t Blend;
  float Xpos;
  float Ypos;
  float Xscale;
  float Yscale;
  float Angle;
};

struct SB3dMaterial
{
  SB3dMaterial() : red(1.0f), green(1.0f),
    blue(1.0f), alpha(1.0f), shininess(0.0f), blend(1),
    fx(0)
  {
    for (std::uint32_t i = 0; i < video::MATERIAL_MAX_TEXTURES; ++i)
      Textures[i]= 0;
  }
  //video::SMaterial Material;
  float red, green, blue, alpha;
  float shininess;
  std::int32_t blend,fx;
  SB3dTexture *Textures[video::MATERIAL_MAX_TEXTURES];
};

} // namespace scene
} // namespace saga

#endif
