// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __IRR_C_COLLADA_MESH_WRITER_H_INCLUDED__
#define __IRR_C_COLLADA_MESH_WRITER_H_INCLUDED__

#include "IColladaMeshWriter.h"
#include "S3DVertex.h"
#include "irrMap.h"
#include "IVideoDriver.h"

namespace saga
{
namespace io
{
  class IXMLWriter;
  class IFileSystem;
}

namespace scene
{
  //! Callback interface for properties which can be used to influence collada writing
  // (Implementer note: keep namespace labels here to make it easier for users copying this one)
  class CColladaMeshWriterProperties  : public virtual IColladaMeshWriterProperties
  {
  public:
    //! Which lighting model should be used in the technique (FX) section when exporting effects (materials)
    virtual saga::scene::E_COLLADA_TECHNIQUE_FX getTechniqueFx(const saga::video::SMaterial& material) const override;

    //! Which texture index should be used when writing the texture of the given sampler color.
    virtual std::int32_t getTextureIdx(const saga::video::SMaterial & material, saga::scene::E_COLLADA_COLOR_SAMPLER cs) const override;

    //! Return which color from Irrlicht should be used for the color requested by collada
    virtual saga::scene::E_COLLADA_IRR_COLOR getColorMapping(const saga::video::SMaterial & material, saga::scene::E_COLLADA_COLOR_SAMPLER cs) const override;

    //! Return custom colors for certain color types requested by collada.
    virtual saga::video::SColor getCustomColor(const saga::video::SMaterial & material, saga::scene::E_COLLADA_COLOR_SAMPLER cs) const override;

    //! Return the settings for transparence
    virtual saga::scene::E_COLLADA_TRANSPARENT_FX getTransparentFx(const saga::video::SMaterial& material) const override;

    //! Transparency value for that material.
    virtual float getTransparency(const saga::video::SMaterial& material) const override;

    //! Reflectivity value for that material
    virtual float getReflectivity(const saga::video::SMaterial& material) const override;

    //! Return index of refraction for that material
    virtual float getIndexOfRefraction(const saga::video::SMaterial& material) const override;

    //! Should node be used in scene export? By default all visible nodes are exported.
    virtual bool isExportable(const saga::scene::ISceneNode * node) const override;

    //! Return the mesh for the given nod. If it has no mesh or shouldn't export it's mesh return 0.
    virtual saga::scene::IMesh* getMesh(saga::scene::ISceneNode * node) override;

    //! Return if the node has it's own material overwriting the mesh-materials
    virtual bool useNodeMaterial(const scene::ISceneNode* node) const override;
  };

  class CColladaMeshWriterNames  : public virtual IColladaMeshWriterNames
  {
  public:
    CColladaMeshWriterNames(IColladaMeshWriter * writer);
    virtual saga::std::wstring nameForMesh(const scene::IMesh* mesh, int instance) override;
    virtual saga::std::wstring nameForNode(const scene::ISceneNode* node) override;
    virtual saga::std::wstring nameForMaterial(const video::SMaterial & material, int materialId, const scene::IMesh* mesh, const scene::ISceneNode* node) override;
  protected:
    saga::std::wstring nameForPtr(const void* ptr) const;
  private:
    IColladaMeshWriter * ColladaMeshWriter;
  };



//! class to write meshes, implementing a COLLADA (.dae, .xml) writer
/** This writer implementation has been originally developed for irrEdit and then
merged out to the Irrlicht Engine */
class CColladaMeshWriter : public IColladaMeshWriter
{
public:

  CColladaMeshWriter(ISceneManager * smgr, video::IVideoDriver* driver, io::IFileSystem* fs);
  virtual ~CColladaMeshWriter();

  //! Returns the type of the mesh writer
  virtual EMESH_WRITER_TYPE getType() const override;

  //! writes a scene starting with the given node
  virtual bool writeScene(io::IWriteFile* file, scene::ISceneNode* root) override;

  //! writes a mesh
  virtual bool writeMesh(io::IWriteFile* file, scene::IMesh* mesh, std::int32_t flags=EMWF_NONE) override;

  // Restrict the characters of oldString a set of allowed characters in xs::NCName and add the prefix.
  virtual saga::std::wstring toNCName(const saga::std::wstring& oldString, const saga::std::wstring& prefix=saga::std::wstring(L"_NC_")) const override;

  //! After export you can find out which name had been used for writing the geometry for this node.
  virtual const saga::std::wstring* findGeometryNameForNode(ISceneNode* node) override;

protected:

  void reset();
  bool hasSecondTextureCoordinates(scene::E_VERTEX_TYPE type) const;
  void writeUv(const saga::glm::vec2& vec);
  void writeVector(const saga::glm::vec2& vec);
  void writeVector(const saga::glm::vec3& vec);
  void writeColor(const saga::video::SColorf& colorf, bool writeAlpha=true);
  inline saga::std::wstring toString(const saga::video::E_PIXEL_FORMAT format) const;
  inline saga::std::wstring toString(const saga::video::E_TEXTURE_CLAMP clamp) const;
  inline saga::std::wstring toString(const saga::scene::E_COLLADA_TRANSPARENT_FX opaque) const;
  inline saga::std::wstring toRef(const saga::std::wstring& source) const;
  bool isCamera(const scene::ISceneNode* node) const;
  saga::std::wstring nameForMesh(const scene::IMesh* mesh, int instance) const;
  saga::std::wstring nameForNode(const scene::ISceneNode* node) const;
  saga::std::wstring nameForMaterial(const video::SMaterial & material, int materialId, const scene::IMesh* mesh, const scene::ISceneNode* node);
  saga::std::wstring nameForMaterialSymbol(const scene::IMesh* mesh, int materialId) const;
  saga::std::wstring findCachedMaterialName(const saga::video::SMaterial& material) const;
  saga::std::wstring minTexfilterToString(bool bilinear, bool trilinear) const;
  saga::std::wstring magTexfilterToString(bool bilinear, bool trilinear) const;
  saga::std::wstring pathToURI(const saga::std::string& path) const;
  inline bool isXmlNameStartChar(char c) const;
  inline bool isXmlNameChar(char c) const;
  std::int32_t getCheckedTextureIdx(const video::SMaterial & material, E_COLLADA_COLOR_SAMPLER cs);
  video::SColor getColorMapping(const video::SMaterial & material, E_COLLADA_COLOR_SAMPLER cs, E_COLLADA_IRR_COLOR colType);
  void writeAsset();
  void makeMeshNames(saga::scene::ISceneNode * node);
  void writeNodeMaterials(saga::scene::ISceneNode * node);
  void writeNodeEffects(saga::scene::ISceneNode * node);
  void writeNodeLights(saga::scene::ISceneNode * node);
  void writeNodeCameras(saga::scene::ISceneNode * node);
  void writeAllMeshGeometries();
  void writeSceneNode(saga::scene::ISceneNode * node);
  void writeMeshMaterials(scene::IMesh* mesh, std::vector<saga::std::wstring> * materialNamesOut= 0);
  void writeMeshEffects(scene::IMesh* mesh);
  void writeMaterialEffect(const saga::std::wstring& materialname, const video::SMaterial & material);
  void writeMeshGeometry(const saga::std::wstring& meshname, scene::IMesh* mesh);
  void writeMeshInstanceGeometry(const saga::std::wstring& meshname, scene::IMesh* mesh, scene::ISceneNode* node= 0);
  void writeMaterial(const saga::std::wstring& materialname);
  void writeLightInstance(const saga::std::wstring& lightName);
  void writeCameraInstance(const saga::std::wstring& cameraName);
  void writeLibraryImages();
  void writeColorFx(const video::SMaterial & material, const char * colorname, E_COLLADA_COLOR_SAMPLER cs, const char* attr1Name= 0, const char* attr1Value= 0);
  void writeAmbientLightElement(const video::SColorf & col);
  void writeColorElement(const video::SColor & col, bool writeAlpha=true);
  void writeColorElement(const video::SColorf & col, bool writeAlpha=true);
  void writeTextureSampler(std::int32_t textureIdx);
  void writeFxElement(const video::SMaterial & material, E_COLLADA_TECHNIQUE_FX techFx);
  void writeNode(const char * nodeName, const char * content);
  void writeFloatElement(float value);
  void writeRotateElement(const saga::glm::vec3& axis, float angle);
  void writeScaleElement(const saga::glm::vec3& scale);
  void writeTranslateElement(const saga::glm::vec3& translate);
  void writeLookAtElement(const saga::glm::vec3& eyePos, const saga::glm::vec3& targetPos, const saga::glm::vec3& upVector);
  void writeMatrixElement(const saga::glm::mat4& matrix);

  struct SComponentGlobalStartPos
  {
    SComponentGlobalStartPos() : PosStartIndex(-1), PosLastIndex(-1),
        NormalStartIndex(-1), NormalLastIndex(-1),
        TCoord0StartIndex(-1), TCoord0LastIndex(-1),
        TCoord1StartIndex(-1), TCoord1LastIndex(-1)
    { }

    std::int32_t PosStartIndex;
    std::int32_t PosLastIndex;

    std::int32_t NormalStartIndex;
    std::int32_t NormalLastIndex;

    std::int32_t TCoord0StartIndex;
    std::int32_t TCoord0LastIndex;

    std::int32_t TCoord1StartIndex;
    std::int32_t TCoord1LastIndex;
  };

  io::IFileSystem* FileSystem;
  video::IVideoDriver* VideoDriver;
  io::IXMLWriter* Writer;
  std::vector<video::ITexture*> LibraryImages;
  std::string Directory;

  // Helper struct for creating geometry copies for the ECGI_PER_MESH_AND_MATERIAL settings.
  struct SGeometryMeshMaterials
  {
    bool equals(const std::vector<saga::std::wstring>& names) const
    {
      if (names.size() != MaterialNames.size())
        return false;
      for (std::uint32_t i = 0; i < MaterialNames.size(); ++i)
        if (names[i] != MaterialNames[i])
          return false;
      return true;
    }

    saga::std::wstring GeometryName;        // replacing the usual ColladaMesh::Name
    std::vector<saga::std::wstring> MaterialNames;  // Material names exported for this instance
    std::vector<const ISceneNode*> MaterialOwners;  // Nodes using this specific mesh-material combination
  };

  // Check per mesh-ptr if stuff has been written for this mesh already
  struct SColladaMesh
  {
    SColladaMesh() : MaterialsWritten(false), EffectsWritten(false)
    {
    }

    SGeometryMeshMaterials * findGeometryMeshMaterials(const std::vector<saga::std::wstring> materialNames)
    {
      for (std::uint32_t i = 0; i < GeometryMeshMaterials.size(); ++i)
      {
        if (GeometryMeshMaterials[i].equals(materialNames))
          return &(GeometryMeshMaterials[i]);
      }
      return NULL;
    }

    const saga::std::wstring& findGeometryNameForNode(const ISceneNode* node) const
    {
      if (GeometryMeshMaterials.size() < 2)
        return Name;
      for (std::uint32_t i = 0; i < GeometryMeshMaterials.size(); ++i)
      {
        if (GeometryMeshMaterials[i].MaterialOwners.linear_search(node)  >= 0)
          return GeometryMeshMaterials[i].GeometryName;
      }
      return Name; // (shouldn't get here usually)
    }

    saga::std::wstring Name;
    bool MaterialsWritten;  // just an optimization doing that here in addition to the MaterialsWritten map
    bool EffectsWritten;  // just an optimization doing that here in addition to the EffectsWritten map

    std::vector<SGeometryMeshMaterials> GeometryMeshMaterials;
  };
  typedef core::map<IMesh*, SColladaMesh>::Node MeshNode;
  core::map<IMesh*, SColladaMesh> Meshes;

  // structure for the lights library
  struct SColladaLight
  {
    SColladaLight()  {}
    saga::std::wstring Name;
  };
  typedef core::map<ISceneNode*, SColladaLight>::Node LightNode;
  core::map<ISceneNode*, SColladaLight> LightNodes;

  // structure for the camera library
  typedef core::map<ISceneNode*, saga::std::wstring>::Node CameraNode;
  core::map<ISceneNode*, saga::std::wstring> CameraNodes;

  // Check per name if stuff has been written already
  // TODO: second parameter not needed, we just don't have a core::set class yet in Irrlicht
  core::map<saga::std::wstring, bool> MaterialsWritten;
  core::map<saga::std::wstring, bool> EffectsWritten;

  // Cache material names
  struct MaterialName
  {
    MaterialName(const saga::video::SMaterial & material, const saga::std::wstring& name)
      : Material(material), Name(name)
    {}
    saga:://video::SMaterial Material;
    saga::std::wstring Name;
  };
  std::vector< MaterialName > MaterialNameCache;
};


} // namespace
} // namespace

#endif
