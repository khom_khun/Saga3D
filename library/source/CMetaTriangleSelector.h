// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_META_TRIANGLE_SELECTOR_H_INCLUDED__
#define __C_META_TRIANGLE_SELECTOR_H_INCLUDED__

#include "IMetaTriangleSelector.h"


namespace saga
{
namespace scene
{

//! Interface for making multiple triangle selectors work as one big selector.
class CMetaTriangleSelector : public IMetaTriangleSelector
{
public:

  //! constructor
  CMetaTriangleSelector();

  //! destructor
  virtual ~CMetaTriangleSelector();

  //! Get amount of all available triangles in this selector
  virtual std::int32_t getTriangleCount() const override;

  //! Gets all triangles.
  virtual void getTriangles(core::triangle3df* triangles, std::int32_t arraySize,
    std::int32_t& outTriangleCount, const glm::mat4* transform, bool useNodeTransform,
    std::vector<SCollisionTriangleRange>* outTriangleInfo) const override;

  //! Gets all triangles which lie within a specific bounding box.
  virtual void getTriangles(core::triangle3df* triangles, std::int32_t arraySize,
    std::int32_t& outTriangleCount, const core::aabbox3d<float>& box,
    const glm::mat4* transform,  bool useNodeTransform,
    std::vector<SCollisionTriangleRange>* outTriangleInfo) const override;

  //! Gets all triangles which have or may have contact with a 3d line.
  virtual void getTriangles(core::triangle3df* triangles, std::int32_t arraySize,
    std::int32_t& outTriangleCount, const core::line3d<float>& line,
    const glm::mat4* transform,  bool useNodeTransform,
    std::vector<SCollisionTriangleRange>* outTriangleInfo) const override;

  //! Adds a triangle selector to the collection of triangle selectors
  //! in this metaTriangleSelector.
  virtual void addTriangleSelector(ITriangleSelector* toAdd) override;

  //! Removes a specific triangle selector which was added before  from the collection.
  virtual bool removeTriangleSelector(ITriangleSelector* toRemove) override;

  //! Removes all triangle selectors from the collection.
  virtual void removeAllTriangleSelectors() override;

  //! Get the scene node associated with a given triangle.
  virtual ISceneNode* getSceneNodeForTriangle(std::uint32_t triangleIndex) const override;

  // Get the number of TriangleSelectors that are part of this one
  virtual std::uint32_t getSelectorCount() const override;

  // Get the TriangleSelector based on index based on getSelectorCount
  virtual ITriangleSelector* getSelector(std::uint32_t index) override;

  // Get the TriangleSelector based on index based on getSelectorCount
  virtual const ITriangleSelector* getSelector(std::uint32_t index) const override;

private:

  std::vector<ITriangleSelector*> TriangleSelectors;
};

} // namespace scene
} // namespace saga


#endif

