// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#include "CMeshCache.h"
#include "IAnimatedMesh.h"
#include "IMesh.h"
#include <algorithm>

namespace saga
{
namespace scene
{

static const std::string EmptyString;

CMeshCache::~CMeshCache()
{
  clear();
}

//! adds a mesh to the list
void CMeshCache::addMesh(const std::string& filename, const std::shared_ptr<IAnimatedMesh>& mesh)
{
  MeshEntry e (filename);
  e.Mesh = mesh;

  Meshes.push_back(e);
}

//! Removes a mesh from the cache.
void CMeshCache::removeMesh(const std::shared_ptr<IMesh>& mesh)
{
  if (!mesh)
    return;

  for (std::uint32_t i = 0; i < Meshes.size(); ++i)
  {
    if (Meshes[i].Mesh == mesh || (Meshes[i].Mesh && Meshes[i].Mesh->getMesh(0) == mesh))
    {
      Meshes.erase(Meshes.begin() + i);
      return;
    }
  }
}

//! Returns amount of loaded meshes
std::uint32_t CMeshCache::getMeshCount() const
{
  return Meshes.size();
}

//! Returns current number of the mesh
std::int32_t CMeshCache::getMeshIndex(const std::shared_ptr<IMesh>&mesh) const
{
  for (std::uint32_t i = 0; i < Meshes.size(); ++i)
  {
    if (Meshes[i].Mesh == mesh || (Meshes[i].Mesh && Meshes[i].Mesh->getMesh(0) == mesh))
      return (std::int32_t)i;
  }

  return -1;
}

//! Returns a mesh based on its index number
const std::shared_ptr<IAnimatedMesh>& CMeshCache::getMeshByIndex(std::uint32_t number) const
{
  return Meshes.at(number).Mesh;
}

//! Returns a mesh based on its name.
const std::shared_ptr<IAnimatedMesh>& CMeshCache::getMeshByName(const std::string& name) const
{
  if (Meshes.empty()) return NullMesh;
  // TODO: faster search on sorted range
  auto it = std::find_if(Meshes.begin(), Meshes.end(), [name] (const auto& mesh)
  {
    return mesh.NamedPath == name;
  });

  if (it != Meshes.end())
    return it->Mesh;
  else
    return NullMesh;
}

//! Get the name of a loaded mesh, based on its index.
const std::string& CMeshCache::getMeshName(std::uint32_t index) const
{
  if (index >= Meshes.size())
    return EmptyString;

  return Meshes[index].NamedPath;
}

//! Get the name of a loaded mesh, if there is any.
const std::string& CMeshCache::getMeshName(const std::shared_ptr<IMesh>& mesh) const
{
  if (!mesh)
    return EmptyString;

  for (const auto& m: Meshes)
  {
    if (m.Mesh == mesh || (m.Mesh && m.Mesh->getMesh(0) == mesh))
      return m.NamedPath;
  }

  return EmptyString;
}

//! Renames a loaded mesh.
bool CMeshCache::renameMesh(std::uint32_t index, const std::string& name)
{
  if (index >= Meshes.size())
    return false;

  Meshes[index].NamedPath = name;
  std::sort(Meshes.begin(), Meshes.end());
  return true;
}

//! Renames a loaded mesh.
bool CMeshCache::renameMesh(const std::shared_ptr<IMesh>& mesh, const std::string& name)
{
  for (std::uint32_t i = 0; i < Meshes.size(); ++i)
  {
    if (Meshes[i].Mesh == mesh || (Meshes[i].Mesh && Meshes[i].Mesh->getMesh(0) == mesh))
    {
      Meshes[i].NamedPath = name;
      std::sort(Meshes.begin(), Meshes.end());
      return true;
    }
  }

  return false;
}

//! returns if a mesh already was loaded
bool CMeshCache::isMeshLoaded(const std::string& name)
{
  return getMeshByName(name) != nullptr;
}

//! Clears the whole mesh cache, removing all meshes.
void CMeshCache::clear()
{
  Meshes.clear();
}

//! Clears all meshes that are held in the mesh cache but not used anywhere else.
void CMeshCache::clearUnusedMeshes()
{
  for (std::uint32_t i = 0; i < Meshes.size(); ++i)
  {
    if (Meshes[i].Mesh.use_count() == 1)
    {
      Meshes.erase(Meshes.begin() + i);
      --i;
    }
  }
}


} // namespace scene
} // namespace saga

