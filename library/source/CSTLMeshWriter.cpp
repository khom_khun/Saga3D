// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h



#ifdef _IRR_COMPILE_WITH_STL_WRITER_

#include "CSTLMeshWriter.h"
#include "os.h"
#include "IMesh.h"
#include "IMeshBuffer.h"

#include "ISceneManager.h"
#include "IMeshCache.h"
#include "IWriteFile.h"

namespace saga
{
namespace scene
{

CSTLMeshWriter::CSTLMeshWriter(scene::ISceneManager* smgr)
  : SceneManager(smgr)
{
  #ifdef _DEBUG
  setDebugName("CSTLMeshWriter");
  #endif

  if (SceneManager)
    SceneManager->grab();
}


CSTLMeshWriter::~CSTLMeshWriter()
{
  if (SceneManager)
    SceneManager->drop();
}


//! Returns the type of the mesh writer
EMESH_WRITER_TYPE CSTLMeshWriter::getType() const
{
  return EMWT_STL;
}


//! writes a mesh
bool CSTLMeshWriter::writeMesh(io::IWriteFile* file, scene::IMesh* mesh, std::int32_t flags)
{
  if (!file)
    return false;

  os::Printer::log("Writing mesh", file->getFileName());

  if (flags & (scene::EMWF_WRITE_BINARY|scene::EMWF_WRITE_COMPRESSED))
    return writeMeshBinary(file, mesh, flags);
  else
    return writeMeshASCII(file, mesh, flags);
}


bool CSTLMeshWriter::writeMeshBinary(io::IWriteFile* file, scene::IMesh* mesh, std::int32_t flags)
{
  // write STL MESH header

  file->write("binary ",7);
  const std::string name(SceneManager->getMeshCache()->getMeshName(mesh));
  const std::int32_t sizeleft = 73-name.size(); // 80 byte header
  if (sizeleft<0)
    file->write(name.c_str(),73);
  else
  {
    char* buf = new char[80];
    memset(buf, 0, 80);
    file->write(name.c_str(),name.size());
    file->write(buf,sizeleft);
    delete [] buf;
  }
  std::uint32_t facenum = 0;
  for (std::uint32_t j= 0; j<mesh->getMeshBufferCount(); ++j)
    facenum += mesh->getMeshBuffer(j)->getIndexCount()/3;
  file->write(&facenum,4);

  // write mesh buffers

  for (std::uint32_t i = 0; i < mesh->getMeshBufferCount(); ++i)
  {
    IMeshBuffer* buffer = mesh->getMeshBuffer(i);
    if (buffer)
    {
      const std::uint32_t indexCount = buffer->getIndexCount();
      const std::uint16_t attributes = 0;
      for (std::uint32_t j= 0; j<indexCount; j+=3)
      {
        const glm::vec3& v1 = buffer->getPosition(buffer->getIndices()[j]);
        const glm::vec3& v2 = buffer->getPosition(buffer->getIndices()[j+1]);
        const glm::vec3& v3 = buffer->getPosition(buffer->getIndices()[j+2]);
        const core::plane3df tmpplane(v1,v2,v3);
        file->write(&tmpplane.Normal, 12);
        file->write(&v1, 12);
        file->write(&v2, 12);
        file->write(&v3, 12);
        file->write(&attributes, 2);
      }
    }
  }
  return true;
}


bool CSTLMeshWriter::writeMeshASCII(io::IWriteFile* file, scene::IMesh* mesh, std::int32_t flags)
{
  // write STL MESH header

  file->write("solid ",6);
  const std::string name(SceneManager->getMeshCache()->getMeshName(mesh));
  file->write(name.c_str(),name.size());
  file->write("\n\n",2);

  // write mesh buffers

  for (std::uint32_t i = 0; i < mesh->getMeshBufferCount(); ++i)
  {
    IMeshBuffer* buffer = mesh->getMeshBuffer(i);
    if (buffer)
    {
      const std::uint32_t indexCount = buffer->getIndexCount();
      for (std::uint32_t j= 0; j<indexCount; j+=3)
      {
        writeFace(file,
          buffer->getPosition(buffer->getIndices()[j]),
          buffer->getPosition(buffer->getIndices()[j+1]),
          buffer->getPosition(buffer->getIndices()[j+2]));
      }
      file->write("\n",1);
    }
  }

  file->write("endsolid ",9);
  file->write(name.c_str(),name.size());

  return true;
}


void CSTLMeshWriter::getVectorAsStringLine(const glm::vec3& v, std::string& s) const
{
  s = std::string(v.X);
  s += " ";
  s += std::string(v.Y);
  s += " ";
  s += std::string(v.Z);
  s += "\n";
}


void CSTLMeshWriter::writeFace(io::IWriteFile* file,
    const glm::vec3& v1,
    const glm::vec3& v2,
    const glm::vec3& v3)
{
  std::string tmp;
  file->write("facet normal ",13);
  getVectorAsStringLine(core::plane3df(v1,v2,v3).Normal, tmp);
  file->write(tmp.c_str(),tmp.size());
  file->write("  outer loop\n",13);
  file->write("    vertex ",11);
  getVectorAsStringLine(v1, tmp);
  file->write(tmp.c_str(),tmp.size());
  file->write("    vertex ",11);
  getVectorAsStringLine(v2, tmp);
  file->write(tmp.c_str(),tmp.size());
  file->write("    vertex ",11);
  getVectorAsStringLine(v3, tmp);
  file->write(tmp.c_str(),tmp.size());
  file->write("  endloop\n",10);
  file->write("endfacet\n",9);
}


} // namespace
} // namespace

#endif

