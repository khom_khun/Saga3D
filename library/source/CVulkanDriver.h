#ifndef __C_VIDEO_VULKAN_H_INCLUDED__
#define __C_VIDEO_VULKAN_H_INCLUDED__

#include "enumType.hpp"
#include "CVideoDriver.h"
#include "SIrrCreationParameters.h"

#ifdef _WIN32
#define VK_USE_PLATFORM_WIN32_KHR
#elif defined __linux__ 
#ifdef __ANDROID__
  #define VK_USE_PLATFORM_ANDROID_KHR
#else
  #define VK_USE_PLATFORM_XLIB_KHR
#endif
#endif

#include <VEZ.h>
#include <glm/vec3.hpp>
#include <vector>

namespace saga
{
class CIrrDeviceSDL;

namespace video
{
  class CVulkanDriver : public CVideoDriver
  {
  public:
    //! Constructor
    CVulkanDriver(const SIrrlichtCreationParameters& params, CIrrDeviceSDL& device);

    //! Destructor
    virtual ~CVulkanDriver();

    virtual const std::string& getVendorName() const override { return VendorName; }

    bool initDriver();

    virtual void beginPass(RenderPassHandle pass) override;
    virtual void render() override;
    virtual void endPass(bool render = true) override;
    virtual void present(TextureHandle texture = NULL_GPU_RESOURCE_HANDLE) override;
    virtual void submit() override;

    virtual TextureHandle createTexture(STexture&& texture) override;
    virtual TextureHandle createTexture(const std::string& path) override;
    virtual TextureHandle createTexture(unsigned char* data, std::size_t size) override;
    virtual void bindTexture(TextureHandle texture, int binding) override;

    virtual SGPUResource::HandleType createResource(SShader&& shader) override;
    virtual SGPUResource::HandleType createResource(SShaderUniform&& uniform) override;
    virtual SGPUResource::HandleType createResource(SPipeline&& pipeline) override;
    virtual SGPUResource::HandleType createResource(SRenderPass&& pass) override;

    virtual void updateShaderUniform(ShaderUniformHandle uniform, void* data) override;
    virtual void bindShaderUniform(ShaderUniformHandle uniform, int binding) override;

    virtual void createGPUMeshBuffer(scene::IMeshBuffer& buffer, const RenderPassHandle pass) override;

    virtual std::uint32_t getWidth() const override;
    virtual std::uint32_t getHeight() const override;

    virtual void copyTexture(TextureHandle srcTex, TextureHandle dstTex,
      const glm::ivec2& srcOffset, const glm::ivec2& dstOffset, const glm::ivec2& size) override;

    virtual void blitTexture(TextureHandle srcTex, TextureHandle dstTex,
      const glm::ivec2& srcOffset = {}, const glm::ivec2& dstOffset = {}) override;

  private:
    VkShaderModule createShader(ShaderHandle shader, VkShaderStageFlagBits stage);
    void createFramebuffer();
    void createCommandBuffer();
    void createTexture(TextureHandle texture);

  private:
    VkQueue GraphicsQueue = VK_NULL_HANDLE;
    VkCommandBuffer CommandBuffer = VK_NULL_HANDLE;
    VkSemaphore Semaphore = VK_NULL_HANDLE;

  private:
    struct SceneNodeData {
      VkBuffer VertexBuffer; 
      VkBuffer IndexBuffer; 
    };

    struct PassMeshBuffer
    {
      RenderPassHandle Pass;
      std::unordered_map<std::uint64_t, SceneNodeData> MeshBuffers;
    };

    std::vector<PassMeshBuffer> PassMeshBuffers;

   private:
    std::string Name;
    std::string VendorName;

    SIrrlichtCreationParameters Params;
    CIrrDeviceSDL& Device;
    E_DEVICE_TYPE DeviceType;

    int PhysicalDeviceIndex = 0;
    #ifndef NDEBUG
    bool EnableValidationLayers = true;
    #else
    bool EnableValidationLayers = false;
    #endif
    bool ManageFramebuffer = true;
    VkSampleCountFlagBits SampleCountFlag = VK_SAMPLE_COUNT_1_BIT;
    std::vector<std::string> DeviceExtensions;
    VkPhysicalDeviceFeatures DeviceFeatures;
    VkInstance Instance = VK_NULL_HANDLE;
    VkPhysicalDevice PhysicalDevice = VK_NULL_HANDLE;
    VkSurfaceKHR Surface = VK_NULL_HANDLE;
    VkDevice VKDevice = VK_NULL_HANDLE;
    VezSwapchain Swapchain = VK_NULL_HANDLE;
    std::vector<VkImage> SwapchainImages;
    VkPhysicalDeviceMemoryProperties MemoryProperties;

    struct VulkanTexture {
      VkImage Image;
      VkImageView ImageView;
      VkSampler Sampler;
    };

    std::unordered_map<TextureHandle, VulkanTexture> VkTextures;
    std::unordered_map<ShaderHandle, std::vector<VkShaderModule>> VkShaderModules;
    std::unordered_map<ShaderUniformHandle, VkBuffer> VkShaderUniforms;

    struct VulkanPipeline {
      VezPipeline Handle;
      VkVertexInputBindingDescription VertexInputBinding;
      VezVertexInputFormat VertexInputFormat;
    };
    std::unordered_map<PipelineHandle, VulkanPipeline> VkPipelines;

    struct VulkanFramebuffer {
      VkImage colorImage = VK_NULL_HANDLE;
      VkImageView colorImageView = VK_NULL_HANDLE;
      VkImage depthStencilImage = VK_NULL_HANDLE;
      VkImageView depthStencilImageView = VK_NULL_HANDLE;
      VezFramebuffer handle = VK_NULL_HANDLE;
    };

    VulkanFramebuffer DefaultFramebuffer;
    std::unordered_map<RenderPassHandle, VezFramebuffer> VkFramebuffers;
  };

// FIXME(manh): move this function when have more video driver, i.e: WebGL
VkFormat toVkFormat(const E_ATTRIBUTE_FORMAT format);
VkFormat toVkFormat(const E_PIXEL_FORMAT format);

} // namespace video
} // namespace saga

#endif // __C_VIDEO_VULKAN_H_INCLUDED__
