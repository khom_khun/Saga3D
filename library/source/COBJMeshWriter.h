// Copyright (C) 2008-2012 Christian Stehno
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __IRR_OBJ_MESH_WRITER_H_INCLUDED__
#define __IRR_OBJ_MESH_WRITER_H_INCLUDED__

#include "IMeshWriter.h"
#include "S3DVertex.h"


namespace saga
{
namespace io
{
  class IFileSystem;
} // namespace io
namespace scene
{
  class IMeshBuffer;
  class ISceneManager;

  //! class to write meshes, implementing a OBJ writer
  class COBJMeshWriter : public IMeshWriter
  {
  public:

    COBJMeshWriter(scene::ISceneManager* smgr, io::IFileSystem* fs);
    virtual ~COBJMeshWriter();

    //! Returns the type of the mesh writer
    virtual EMESH_WRITER_TYPE getType() const override;

    //! writes a mesh
    virtual bool writeMesh(io::IWriteFile* file, scene::IMesh* mesh, std::int32_t flags=EMWF_NONE) override;

  protected:
    // create vector output with line end into string
    void getVectorAsStringLine(const glm::vec3& v,
        std::string& s) const;

    // create vector output with line end into string
    void getVectorAsStringLine(const glm::vec2& v,
        std::string& s) const;

    // create color output with line end into string
    void getColorAsStringLine(const video::SColor& color,
        const char* const prefix, std::string& s) const;

    scene::ISceneManager* SceneManager;
    io::IFileSystem* FileSystem;
  };

} // namespace
} // namespace

#endif

