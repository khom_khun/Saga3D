// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_PARTICLE_RING_EMITTER_H_INCLUDED__
#define __C_PARTICLE_RING_EMITTER_H_INCLUDED__


#ifdef _IRR_COMPILE_WITH_PARTICLES_

#include "IParticleRingEmitter.h"


namespace saga
{
namespace scene
{

//! A ring emitter
class CParticleRingEmitter : public IParticleRingEmitter
{
public:

  //! constructor
  CParticleRingEmitter(
    const glm::vec3& center, float radius, float ringThickness,
    const glm::vec3& direction = glm::vec3(0.0f,0.03f,0.0f),
    std::uint32_t minParticlesPerSecond = 20,
    std::uint32_t maxParticlesPerSecond = 40,
    const video::SColor& minStartColor = video::SColor(255,0,0,0),
    const video::SColor& maxStartColor = video::SColor(255,255,255,255),
    std::uint32_t lifeTimeMin=2000,
    std::uint32_t lifeTimeMax=4000,
    std::int32_t maxAngleDegrees= 0,
    const glm::vec2& minStartSize = glm::vec2(5.0f,5.0f),
    const glm::vec2& maxStartSize = glm::vec2(5.0f,5.0f)
   );

  //! Prepares an array with new particles to emitt into the system
  //! and returns how much new particles there are.
  virtual std::int32_t emitt(std::uint32_t now, std::uint32_t timeSinceLastCall, SParticle*& outArray) override;

  //! Set direction the emitter emits particles
  virtual void setDirection(const glm::vec3& newDirection) override { Direction = newDirection; }

  //! Set minimum number of particles the emitter emits per second
  virtual void setMinParticlesPerSecond(std::uint32_t minPPS) override { MinParticlesPerSecond = minPPS; }

  //! Set maximum number of particles the emitter emits per second
  virtual void setMaxParticlesPerSecond(std::uint32_t maxPPS) override { MaxParticlesPerSecond = maxPPS; }

  //! Set minimum starting color for particles
  virtual void setMinStartColor(const video::SColor& color) override { MinStartColor = color; }

  //! Set maximum starting color for particles
  virtual void setMaxStartColor(const video::SColor& color) override { MaxStartColor = color; }

  //! Set the maximum starting size for particles
  virtual void setMaxStartSize(const glm::vec2& size) override { MaxStartSize = size; }

  //! Set the minimum starting size for particles
  virtual void setMinStartSize(const glm::vec2& size) override { MinStartSize = size; }

  //! Set the minimum particle life-time in milliseconds
  virtual void setMinLifeTime(std::uint32_t lifeTimeMin) override { MinLifeTime = lifeTimeMin; }

  //! Set the maximum particle life-time in milliseconds
  virtual void setMaxLifeTime(std::uint32_t lifeTimeMax) override { MaxLifeTime = lifeTimeMax; }

  //!  Set maximal random derivation from the direction
  virtual void setMaxAngleDegrees(std::int32_t maxAngleDegrees) override { MaxAngleDegrees = maxAngleDegrees; }

  //! Set the center of the ring
  virtual void setCenter(const glm::vec3& center) override { Center = center; }

  //! Set the radius of the ring
  virtual void setRadius(float radius) override { Radius = radius; }

  //! Set the thickness of the ring
  virtual void setRingThickness(float ringThickness) override { RingThickness = ringThickness; }

  //! Gets direction the emitter emits particles
  virtual const glm::vec3& getDirection() const override { return Direction; }

  //! Gets the minimum number of particles the emitter emits per second
  virtual std::uint32_t getMinParticlesPerSecond() const override { return MinParticlesPerSecond; }

  //! Gets the maximum number of particles the emitter emits per second
  virtual std::uint32_t getMaxParticlesPerSecond() const override { return MaxParticlesPerSecond; }

  //! Gets the minimum starting color for particles
  virtual const video::SColor& getMinStartColor() const override { return MinStartColor; }

  //! Gets the maximum starting color for particles
  virtual const video::SColor& getMaxStartColor() const override { return MaxStartColor; }

  //! Gets the maximum starting size for particles
  virtual const glm::vec2& getMaxStartSize() const override { return MaxStartSize; }

  //! Gets the minimum starting size for particles
  virtual const glm::vec2& getMinStartSize() const override { return MinStartSize; }

  //! Get the minimum particle life-time in milliseconds
  virtual std::uint32_t getMinLifeTime() const override { return MinLifeTime; }

  //! Get the maximum particle life-time in milliseconds
  virtual std::uint32_t getMaxLifeTime() const override { return MaxLifeTime; }

  //!  Get maximal random derivation from the direction
  virtual std::int32_t getMaxAngleDegrees() const override { return MaxAngleDegrees; }

  //! Get the center of the ring
  virtual const glm::vec3& getCenter() const override { return Center; }

  //! Get the radius of the ring
  virtual float getRadius() const override { return Radius; }

  //! Get the thickness of the ring
  virtual float getRingThickness() const override { return RingThickness; }

  //! Writes attributes of the object.
  virtual void serializeAttributes(io::IAttributes* out, io::SAttributeReadWriteOptions* options) const override;

  //! Reads attributes of the object.
  virtual void deserializeAttributes(io::IAttributes* in, io::SAttributeReadWriteOptions* options) override;

private:

  std::vector<SParticle> Particles;

  glm::vec3  Center;
  float Radius;
  float RingThickness;

  glm::vec3 Direction;
  glm::vec2 MaxStartSize, MinStartSize;
  std::uint32_t MinParticlesPerSecond, MaxParticlesPerSecond;
  video::SColor MinStartColor, MaxStartColor;
  std::uint32_t MinLifeTime, MaxLifeTime;

  std::uint32_t Time;
  std::int32_t MaxAngleDegrees;
};

} // namespace scene
} // namespace saga

#endif // _IRR_COMPILE_WITH_PARTICLES_

#endif

