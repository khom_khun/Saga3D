// Copyright (C) 2014 Lauri Kasanen
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

// Modified version with rigging/skinning support

#ifndef __IRR_B3D_MESH_WRITER_H_INCLUDED__
#define __IRR_B3D_MESH_WRITER_H_INCLUDED__

#include "IMeshWriter.h"
#include "IWriteFile.h"
#include "SB3DStructs.h"
#include "ISkinnedMesh.h"



namespace saga
{
namespace scene
{

//! class to write B3D mesh files
class CB3DMeshWriter : public IMeshWriter
{
public:

  CB3DMeshWriter();

  //! Returns the type of the mesh writer
    virtual EMESH_WRITER_TYPE getType() const;

  //! writes a mesh
    virtual bool writeMesh(io::IWriteFile* file, scene::IMesh* mesh, std::int32_t flags=EMWF_NONE);

private:
  std::uint32_t Size;

    void writeJointChunk(io::IWriteFile* file, ISkinnedMesh* mesh , ISkinnedMesh::SJoint* joint);
    std::uint32_t getJointChunkSize(const ISkinnedMesh* mesh, ISkinnedMesh::SJoint* joint);
    std::vector<ISkinnedMesh::SJoint*> getRootJoints(const ISkinnedMesh* mesh);

    std::uint32_t getUVlayerCount(IMesh *mesh);
    ISkinnedMesh* getSkinned (IMesh *mesh);

  void write(io::IWriteFile* file, const void *ptr, const std::uint32_t bytes);

};

} // namespace
} // namespace

#endif
