// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#include "CSceneNodeAnimatorCameraFPS.h"
#include "IVideoDriver.h"
#include "ISceneManager.h"
#include "ICameraSceneNode.h"

namespace saga
{
namespace scene
{

//! constructor
CSceneNodeAnimatorCameraFPS::CSceneNodeAnimatorCameraFPS(
  float moveSpeed, float rotateSpeed)
: MoveSpeed(moveSpeed), RotateSpeed(rotateSpeed)
{

}

//! destructor
CSceneNodeAnimatorCameraFPS::~CSceneNodeAnimatorCameraFPS()
{

}

//! It is possible to send mouse and key events to the camera. Most cameras
//! may ignore this input, but camera scene nodes which are created for
//! example with scene::ISceneManager::addMayaCameraSceneNode or
//! scene::ISceneManager::addFPSCameraSceneNode, may want to get this input
//! for changing their position, look at target or whatever.
void CSceneNodeAnimatorCameraFPS::OnEvent(const SDL_Event& event)
{
  switch(event.type)
  {
    case SDL_KEYDOWN:
    switch (event.key.keysym.scancode)
    {
      case SDL_SCANCODE_W:
      {
        Position += Target * MoveSpeed;
      } break;
      case SDL_SCANCODE_A:
      {

      }
      case SDL_SCANCODE_S:
      {

      } break;
      case SDL_SCANCODE_D:
      {

      } break;
    } break;

    case SDL_MOUSEMOTION:
    {
      glm::ivec2 mouse(event.motion.x, event.motion.y);
      glm::vec3 rot(
        (MousePosition.y - mouse.y) * RotateSpeed * 0.2f,
        (MousePosition.x - mouse.x) * RotateSpeed * 0.2f,
        0.f
      );
      Rotation += rot;
      MousePosition = mouse;
    } break;

    default:
      break;
  }
}

void CSceneNodeAnimatorCameraFPS::animateNode(ISceneNode& node, std::uint32_t timeMs)
{
  if (node.getType() != E_SCENE_NODE_TYPE::CAMERA)
    return;

  ICameraSceneNode& camera = *static_cast<ICameraSceneNode*>(&node);
  auto& smgr = camera.getSceneManager();

  if(smgr.get() == nullptr)
    return;

  // get time
  float timeDiff = (float) (timeMs - LastAnimationTime);
  LastAnimationTime = timeMs;

  // auto mat = glm::rotate(glm::mat4(1.f), glm::radians(Rotation.x), {1.f, 0.f, 0.f});
  // mat = glm::rotate(mat, glm::radians(Rotation.y), {0.f, 1.f, 0.f});
  // mat = glm::rotate(mat, glm::radians(Rotation.z), {0.f, 0.f, 1.f});
  // auto target = camera.getTarget();
  // target = mat * glm::vec4(target, 1.0f);
  camera.setRotation(Rotation);
  // camera.setTarget(target);
}

//! Sets the rotation speed
void CSceneNodeAnimatorCameraFPS::setRotateSpeed(float speed)
{
  RotateSpeed = speed;
}

//! Sets the movement speed
void CSceneNodeAnimatorCameraFPS::setMoveSpeed(float speed)
{
  MoveSpeed = speed;
}

//! Gets the rotation speed
float CSceneNodeAnimatorCameraFPS::getRotateSpeed() const
{
  return RotateSpeed;
}

// Gets the movement speed
float CSceneNodeAnimatorCameraFPS::getMoveSpeed() const
{
  return MoveSpeed;
}

//! Sets whether the Y axis of the mouse should be inverted.
void CSceneNodeAnimatorCameraFPS::setInvertMouse(bool invert)
{

}

} // namespace scene
} // namespace saga
