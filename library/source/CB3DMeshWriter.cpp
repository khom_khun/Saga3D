// Copyright (C) 2014 Lauri Kasanen
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

// TODO: replace printf's by logging messages



#define _IRR_COMPILE_WITH_B3D_WRITER_
#ifdef _IRR_COMPILE_WITH_B3D_WRITER_

#include "CB3DMeshWriter.h"
#include "os.h"
#include "ISkinnedMesh.h"
#include "IMeshBuffer.h"
#include "IWriteFile.h"
#include "ITexture.h"
#include "irrMap.h"


namespace saga
{
namespace scene
{

using namespace core;
using namespace video;

CB3DMeshWriter::CB3DMeshWriter()
{
  #ifdef _DEBUG
  setDebugName("CB3DMeshWriter");
  #endif
}


//! Returns the type of the mesh writer
EMESH_WRITER_TYPE CB3DMeshWriter::getType() const
{
    return EMWT_B3D;
}


//! writes a mesh
bool CB3DMeshWriter::writeMesh(io::IWriteFile* file, IMesh* const mesh, std::int32_t flags)
{
    if (!file || !mesh)
        return false;
#ifdef __BIG_ENDIAN__
    os::Printer::log("B3D export does not support big-endian systems.", ELL_ERROR);
    return false;
#endif

    Size = 0;
    file->write("BB3D", 4);
    file->write(&Size, sizeof(std::uint32_t)); // Updated later once known.

    int version = 1;
    write(file, &version, sizeof(int));

    //

    const std::uint32_t numBeshBuffers = mesh->getMeshBufferCount();
    array<SB3dTexture> texs;
    map<ITexture *, std::uint32_t> tex2id;  // TODO: texture pointer as key not sufficient as same texture can have several id's
    std::uint32_t texsizes = 0;
    for (std::uint32_t i = 0; i < numBeshBuffers; i++)
  {
        const IMeshBuffer * const mb = mesh->getMeshBuffer(i);
        const SMaterial &mat = mb->getMaterial();

        for (std::uint32_t j = 0; j < MATERIAL_MAX_TEXTURES; j++)
    {
            if (mat.getTexture(j))
      {
                SB3dTexture t;
        t.TextureName = std::string(mat.getTexture(j)->getName().getPath());

        // TODO: need some description of Blitz3D texture-flags to figure this out. But Blend should likely depend on material-type.
        t.Flags = j == 2 ? 65536 : 1;
        t.Blend = 2;

        // TODO: evaluate texture matrix
        t.Xpos = 0;
        t.Ypos = 0;
        t.Xscale = 1;
        t.Yscale = 1;
        t.Angle = 0;

                texs.push_back(t);
                texsizes += 7*4 + t.TextureName.size() + 1;
                tex2id[mat.getTexture(j)] = texs.size() - 1;
            }
        }
    }

    write(file, "TEXS", 4);
    write(file, &texsizes, 4);

    std::uint32_t numTexture = texs.size();
    for (std::uint32_t i = 0; i < numTexture; i++)
  {
        write(file, texs[i].TextureName.c_str(), texs[i].TextureName.size() + 1);
        write(file, &texs[i].Flags, 7*4);
    }

    //

    const std::uint32_t brushsize = (7 * 4 + 1) * numBeshBuffers + numBeshBuffers * 4 * MATERIAL_MAX_TEXTURES + 4;
    write(file, "BRUS", 4);
    write(file, &brushsize, 4);
    std::uint32_t brushcheck = Size;
    const std::uint32_t usedtex = MATERIAL_MAX_TEXTURES;
    write(file, &usedtex, 4);

    for (std::uint32_t i = 0; i < numBeshBuffers; i++)
  {
        const IMeshBuffer * const mb = mesh->getMeshBuffer(i);
        const SMaterial &mat = mb->getMaterial();

        write(file, "", 1);

        float f = 1;
        write(file, &f, 4);
        write(file, &f, 4);
        write(file, &f, 4);
        write(file, &f, 4);

        f = 0;
        write(file, &f, 4);

        std::uint32_t tmp = 1;
        write(file, &tmp, 4);
        tmp = 0;
        write(file, &tmp, 4);

        for (std::uint32_t j = 0; j < MATERIAL_MAX_TEXTURES; j++)
    {
            if (mat.getTexture(j))
      {
                const std::uint32_t id = tex2id[mat.getTexture(j)];
                write(file, &id, 4);
            }
      else
      {
                const int id = -1;
                write(file, &id, 4);
            }
        }
    }

    // Check brushsize
    brushcheck = Size - brushcheck;
    if (brushcheck != brushsize)
  {
        printf("Failed in brush size calculation, size %u advanced %u\n",
            brushsize, brushcheck);
  }

    write(file, "NODE", 4);

    // Calculate node size
    std::uint32_t nodesize = 41 + 8 + 4 + 8;
    std::uint32_t bonesSize = 0;

    if(ISkinnedMesh *skinnedMesh = getSkinned(mesh))
    {
        if (!skinnedMesh->isStatic())
        {
            bonesSize += 20;
        }

        const std::vector<ISkinnedMesh::SJoint*> rootJoints = getRootJoints(skinnedMesh);
        for (std::uint32_t i = 0; i < rootJoints.size(); i++)
        {
            bonesSize += getJointChunkSize(skinnedMesh, rootJoints[i]);
        }
        nodesize += bonesSize;

        // -------------------

    }

    // VERT data
    nodesize += 12;

    const std::uint32_t texcoords = getUVlayerCount(mesh);
    for (std::uint32_t i = 0; i < numBeshBuffers; i++)
    {
        nodesize += 8 + 4;
        const IMeshBuffer * const mb = mesh->getMeshBuffer(i);

        nodesize += mb->getVertexCount() * 10 * 4;

        nodesize += mb->getVertexCount() * texcoords * 2 * 4;
        nodesize += mb->getIndexCount() * 4;
    }
    write(file, &nodesize, 4);
    std::uint32_t nodecheck = Size;

    // Node
    write(file, "", 1);
    float f = 0;
    write(file, &f, 4);
    write(file, &f, 4);
    write(file, &f, 4);

    f = 1;
    write(file, &f, 4);
    write(file, &f, 4);
    write(file, &f, 4);

    write(file, &f, 4);
    f = 0;
    write(file, &f, 4);
    write(file, &f, 4);
    write(file, &f, 4);

    // Mesh
    write(file, "MESH", 4);
    const std::uint32_t meshsize = nodesize - 41 - 8 - bonesSize;
    write(file, &meshsize, 4);
  std::int32_t brushID = -1;
    write(file, &brushID, 4);



    // Verts
    write(file, "VRTS", 4);
    std::uint32_t vertsize = 12;

    for (std::uint32_t i = 0; i < numBeshBuffers; i++)
    {
        const IMeshBuffer * const mb = mesh->getMeshBuffer(i);

        vertsize += mb->getVertexCount() * 10 * 4 +
                    mb->getVertexCount() * texcoords * 2 * 4;
    }
    write(file, &vertsize, 4);
    std::uint32_t vertcheck = Size;

    int flagsB3D = 3;
    write(file, &flagsB3D, 4);

    write(file, &texcoords, 4);
    flagsB3D = 2;
    write(file, &flagsB3D, 4);

    for (std::uint32_t i = 0; i < numBeshBuffers; i++)
    {
        const IMeshBuffer * const mb = mesh->getMeshBuffer(i);
        std::uint32_t numVertices = mb->getVertexCount();
        for (std::uint32_t j = 0; j < numVertices; j++)
    {
            const vector3df &pos = mb->getPosition(j);
            write(file, &pos.X, 4);
            write(file, &pos.Y, 4);
            write(file, &pos.Z, 4);

            const vector3df &n = mb->getNormal(j);
            write(file, &n.X, 4);
            write(file, &n.Y, 4);
            write(file, &n.Z, 4);

            const std::uint32_t zero = 0;
            switch (mb->getVertexType())
      {
                case E_VERTEX_TYPE::STANDARD:
                {
                    S3DVertex *v = (S3DVertex *) mb->getVertices();
                    const SColorf col(v[j].Color);
                    write(file, &col.r, 4);
                    write(file, &col.g, 4);
                    write(file, &col.b, 4);
                    write(file, &col.a, 4);

                    write(file, &v[j].TCoords.X, 4);
                    write(file, &v[j].TCoords.Y, 4);
                    if (texcoords == 2)
                    {
                        write(file, &zero, 4);
                        write(file, &zero, 4);
                    }
                }
                break;
                case E_VERTEX_TYPE::TWO_TCOORDS:
                {
                    S3DVertex2TCoords *v = (S3DVertex2TCoords *) mb->getVertices();
                    const SColorf col(v[j].Color);
                    write(file, &col.r, 4);
                    write(file, &col.g, 4);
                    write(file, &col.b, 4);
                    write(file, &col.a, 4);

                    write(file, &v[j].TCoords.X, 4);
                    write(file, &v[j].TCoords.Y, 4);
                    write(file, &v[j].TCoords2.X, 4);
                    write(file, &v[j].TCoords2.Y, 4);
                }
                break;
                case E_VERTEX_TYPE::TANGENTS:
                {
                    S3DVertexTangents *v = (S3DVertexTangents *) mb->getVertices();
                    const SColorf col(v[j].Color);
                    write(file, &col.r, 4);
                    write(file, &col.g, 4);
                    write(file, &col.b, 4);
                    write(file, &col.a, 4);

                    write(file, &v[j].TCoords.X, 4);
                    write(file, &v[j].TCoords.Y, 4);
                    if (texcoords == 2)
                    {
                        write(file, &zero, 4);
                        write(file, &zero, 4);
                    }
                }
                break;
            }
        }
    }
    // Check vertsize
    vertcheck = Size - vertcheck;
    if (vertcheck != vertsize)
  {
        printf("Failed in vertex size calculation, size %u advanced %u\n",
            vertsize, vertcheck);
  }

    std::uint32_t currentMeshBufferIndex = 0;
    // Tris
    for (std::uint32_t i = 0; i < numBeshBuffers; i++)
    {
        const IMeshBuffer * const mb = mesh->getMeshBuffer(i);
        write(file, "TRIS", 4);
        const std::uint32_t trisize = 4 + mb->getIndexCount() * 4;
        write(file, &trisize, 4);

        std::uint32_t tricheck = Size;

        write(file, &i, 4);

        std::uint32_t numIndices = mb->getIndexCount();
        const std::uint16_t * const idx = (std::uint16_t *) mb->getIndices();
        for (std::uint32_t j = 0; j < numIndices; j += 3)
    {
            std::uint32_t tmp = idx[j] + currentMeshBufferIndex;
            write(file, &tmp, sizeof(std::uint32_t));

            tmp = idx[j + 1] + currentMeshBufferIndex;
            write(file, &tmp, sizeof(std::uint32_t));

            tmp = idx[j + 2] + currentMeshBufferIndex;
            write(file, &tmp, sizeof(std::uint32_t));
        }

        // Check that tris calculation was ok
        tricheck = Size - tricheck;
        if (tricheck != trisize)
    {
            printf("Failed in tris size calculation, size %u advanced %u\n",
                trisize, tricheck);
    }

        currentMeshBufferIndex += mb->getVertexCount();
    }

    if(ISkinnedMesh *skinnedMesh = getSkinned(mesh))
    {
        // Write animation data
        if (!skinnedMesh->isStatic())
        {
            write(file, "ANIM", 4);

            const std::uint32_t animsize = 12;
            write(file, &animsize, 4);

            const std::uint32_t flags = 0;
            const std::uint32_t frames = skinnedMesh->getFrameCount();
            const float fps = skinnedMesh->getAnimationSpeed();

            write(file, &flags, 4);
            write(file, &frames, 4);
            write(file, &fps, 4);
        }

        // Write joints
        std::vector<ISkinnedMesh::SJoint*> rootJoints = getRootJoints(skinnedMesh);

        for (std::uint32_t i = 0; i < rootJoints.size(); i++)
        {
            writeJointChunk(file, skinnedMesh, rootJoints[i]);
        }
    }

    // Check that node calculation was ok
    nodecheck = Size - nodecheck;
    if (nodecheck != nodesize)
  {
        printf("Failed in node size calculation, size %u advanced %u\n",
            nodesize, nodecheck);
  }
    file->seek(4);
    file->write(&Size, 4);

    return true;
}



void CB3DMeshWriter::writeJointChunk(io::IWriteFile* file, ISkinnedMesh* mesh , ISkinnedMesh::SJoint* joint)
{
    // Node
    write(file, "NODE", 4);

    // Calculate node size
    std::uint32_t nodesize = getJointChunkSize(mesh, joint);
    nodesize -= 8; // The declaration + size of THIS chunk shouldn't be added to the size

    write(file, &nodesize, 4);


    std::string name = joint->Name;
    write(file, name.c_str(), name.size());
    write(file, "", 1);

    glm::vec3 pos = joint->Animatedposition;
    // Position
    write(file, &pos.X, 4);
    write(file, &pos.Y, 4);
    write(file, &pos.Z, 4);

    // Scale
    glm::vec3 scale = joint->Animatedscale;
    if (scale == glm::vec3(0, 0, 0))
        scale = glm::vec3(1, 1, 1);

    write(file, &scale.X, 4);
    write(file, &scale.Y, 4);
    write(file, &scale.Z, 4);

    // Rotation
    core::quaternion quat = joint->Animatedrotation;
    write(file, &quat.W, 4);
    write(file, &quat.X, 4);
    write(file, &quat.Y, 4);
    write(file, &quat.Z, 4);

    // Bone
    write(file, "BONE", 4);
    std::uint32_t bonesize = 8 * joint->Weights.size();
    write(file, &bonesize, 4);

    // Skinning ------------------
    for (std::uint32_t i = 0; i < joint->Weights.size(); i++)
    {
        const std::uint32_t vertexID = joint->Weights[i].vertex_id;
        const std::uint32_t bufferID = joint->Weights[i].buffer_id;
        const float weight = joint->Weights[i].strength;

        std::uint32_t b3dVertexID = vertexID;
        for (std::uint32_t j = 0; j < bufferID; j++)
        {
            b3dVertexID += mesh->getMeshBuffer(j)->getVertexCount();
        }

        write(file, &b3dVertexID, 4);
        write(file, &weight, 4);
    }
    // ---------------------------

    // Animation keys
    if (joint->PositionKeys.size())
    {
        write(file, "KEYS", 4);
        std::uint32_t keysSize = 4 * joint->PositionKeys.size() * 4; // X, Y and Z pos + frame
        keysSize += 4;  // Flag to define the type of the key
        write(file, &keysSize, 4);

        std::uint32_t flag = 1; // 1 = flag for position keys
        write(file, &flag, 4);

        for (std::uint32_t i = 0; i < joint->PositionKeys.size(); i++)
        {
            const std::int32_t frame = static_cast<std::int32_t>(joint->PositionKeys[i].frame);
            const glm::vec3 pos = joint->PositionKeys[i].position;

            write (file, &frame, 4);

            write (file, &pos.X, 4);
            write (file, &pos.Y, 4);
            write (file, &pos.Z, 4);

        }
    }
    if (joint->RotationKeys.size())
    {
        write(file, "KEYS", 4);
        std::uint32_t keysSize = 4 * joint->RotationKeys.size() * 5; // W, X, Y and Z rot + frame
        keysSize += 4; // Flag
        write(file, &keysSize, 4);

        std::uint32_t flag = 4;
        write(file, &flag, 4);

        for (std::uint32_t i = 0; i < joint->RotationKeys.size(); i++)
        {
            const std::int32_t frame = static_cast<std::int32_t>(joint->RotationKeys[i].frame);
            const core::quaternion rot = joint->RotationKeys[i].rotation;

            write (file, &frame, 4);

            write (file, &rot.W, 4);
            write (file, &rot.X, 4);
            write (file, &rot.Y, 4);
            write (file, &rot.Z, 4);
        }
    }
    if (joint->ScaleKeys.size())
    {
        write(file, "KEYS", 4);
        std::uint32_t keysSize = 4 * joint->ScaleKeys.size() * 4; // X, Y and Z scale + frame
        keysSize += 4; // Flag
        write(file, &keysSize, 4);

        std::uint32_t flag = 2;
        write(file, &flag, 4);

        for (std::uint32_t i = 0; i < joint->ScaleKeys.size(); i++)
        {
            const std::int32_t frame = static_cast<std::int32_t>(joint->ScaleKeys[i].frame);
            const glm::vec3 scale = joint->ScaleKeys[i].scale;

            write (file, &frame, 4);

            write (file, &scale.X, 4);
            write (file, &scale.Y, 4);
            write (file, &scale.Z, 4);
        }
    }

    for (std::uint32_t i = 0; i < joint->Children.size(); i++)
    {
        writeJointChunk(file, mesh, joint->Children[i]);
    }
}


ISkinnedMesh* CB3DMeshWriter::getSkinned (IMesh *mesh)
{
  if (mesh->getMeshType() == E_ANIMATED_MESH_TYPE::SKINNED)
    {
    return static_cast<ISkinnedMesh*>(mesh);
    }
    return 0;
}

std::uint32_t CB3DMeshWriter::getJointChunkSize(const ISkinnedMesh* mesh, ISkinnedMesh::SJoint* joint)
{
    std::uint32_t chunkSize = 8 + 40; // Chunk declaration + chunk data
    chunkSize += joint->Name.size() + 1; // the NULL character at the end of the string

    std::uint32_t boneSize = joint->Weights.size() * 8; // vertex_id + weight = 8 bits per weight block
    boneSize += 8; // declaration + size of he BONE chunk

    std::uint32_t keysSize = 0;
    if (joint->PositionKeys.size() != 0)
    {
        keysSize += 8; // KEYS + chunk size
        keysSize += 4; // flags

        keysSize += (joint->PositionKeys.size() * 16);
    }
    if (joint->RotationKeys.size() != 0)
    {
        keysSize += 8; // KEYS + chunk size
        keysSize += 4; // flags

        keysSize += (joint->RotationKeys.size() * 20);
    }
    if (joint->ScaleKeys.size() != 0)
    {
        keysSize += 8; // KEYS + chunk size
        keysSize += 4; // flags

        keysSize += (joint->ScaleKeys.size() * 16);
    }

    chunkSize += boneSize;
    chunkSize += keysSize;

    for (std::uint32_t i = 0; i < joint->Children.size(); ++i)
    {
        chunkSize += (getJointChunkSize(mesh, joint->Children[i]));
    }
    return chunkSize;
}

std::vector<ISkinnedMesh::SJoint*> CB3DMeshWriter::getRootJoints(const ISkinnedMesh* mesh)
{
    std::vector<ISkinnedMesh::SJoint*> roots;

    std::vector<ISkinnedMesh::SJoint*> allJoints = mesh->getAllJoints();
    for (std::uint32_t i = 0; i < allJoints.size(); i++)
    {
        bool isRoot = true;
        ISkinnedMesh::SJoint* testedJoint = allJoints[i];
        for (std::uint32_t j = 0; j < allJoints.size(); j++)
        {
           ISkinnedMesh::SJoint* testedJoint2 = allJoints[j];
           for (std::uint32_t k = 0; k < testedJoint2->Children.size(); k++)
           {
               if (testedJoint == testedJoint2->Children[k])
                    isRoot = false;
           }
        }
        if (isRoot)
            roots.push_back(testedJoint);
    }

    return roots;
}

std::uint32_t CB3DMeshWriter::getUVlayerCount(IMesh* mesh)
{
    const std::uint32_t numBeshBuffers = mesh->getMeshBufferCount();
    for (std::uint32_t i = 0; i < numBeshBuffers; i++)
    {
        const IMeshBuffer * const mb = mesh->getMeshBuffer(i);

        if (mb->getVertexType() == E_VERTEX_TYPE::TWO_TCOORDS)
        {
            return 2;
        }
    }
    return 1;
}

void CB3DMeshWriter::write(io::IWriteFile* file, const void *ptr, const std::uint32_t bytes)
{
  file->write(ptr, bytes);
  Size += bytes;
}

} // namespace
} // namespace

#endif // _IRR_COMPILE_WITH_B3D_WRITER_

