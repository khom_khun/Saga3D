// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#include "CEmptySceneNode.h"
#include "ISceneManager.h"

namespace saga
{
namespace scene
{

//! constructor
CEmptySceneNode::CEmptySceneNode(ISceneNode* parent, ISceneManager* mgr, std::int32_t id)
: ISceneNode(parent, mgr, id)
{
  #ifdef _DEBUG
  setDebugName("CEmptySceneNode");
  #endif

  setAutomaticCulling(scene::E_AUTOMATIC_CULLING_TYPE::OFF);
}


//! pre render event
void CEmptySceneNode::OnRegisterSceneNode(video::RenderPassHandle pass)
{
  if (IsVisible)
    SceneManager->registerNodeForRendering(this);

  ISceneNode::OnRegisterSceneNode(video::RenderPassHandle pass);
}


//! render
void CEmptySceneNode::render()
{
  // do nothing
}


//! returns the axis aligned bounding box of this node
const core::aabbox3d<float>& CEmptySceneNode::getBoundingBox() const
{
  return Box;
}


//! Creates a clone of this scene node and its children.
ISceneNode* CEmptySceneNode::clone(ISceneNode* newParent, ISceneManager* newManager)
{
  if (!newParent)
    newParent = Parent;
  if (!newManager)
    newManager = SceneManager;

  CEmptySceneNode* nb = new CEmptySceneNode(newParent,
    newManager, ID);

  nb->cloneMembers(this, newManager);
  nb->Box = Box;

  if (newParent)
    nb->drop();
  return nb;
}


} // namespace scene
} // namespace saga
