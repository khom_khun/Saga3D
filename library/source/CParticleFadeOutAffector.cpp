// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#include "CParticleFadeOutAffector.h"

#ifdef _IRR_COMPILE_WITH_PARTICLES_


#include "os.h"

namespace saga
{
namespace scene
{

//! constructor
CParticleFadeOutAffector::CParticleFadeOutAffector(
  const video::SColor& targetColor, std::uint32_t fadeOutTime)
  : IParticleFadeOutAffector(), TargetColor(targetColor)
{

  #ifdef _DEBUG
  setDebugName("CParticleFadeOutAffector");
  #endif

  FadeOutTime = fadeOutTime ? static_cast<float>(fadeOutTime) : 1.0f;
}


//! Affects an array of particles.
void CParticleFadeOutAffector::affect(std::uint32_t now, SParticle* particlearray, std::uint32_t count)
{
  if (!Enabled)
    return;
  float d;

  for (std::uint32_t i = 0; i < count; ++i)
  {
    if (particlearray[i].endTime - now < FadeOutTime)
    {
      d = (particlearray[i].endTime - now) / FadeOutTime;  // FadeOutTime probably float to save casts here (just guessing)
      particlearray[i].color = particlearray[i].startColor.getInterpolated(
        TargetColor, d);
    }
  }
}


//! Writes attributes of the object.
//! Implement this to expose the attributes of your scene node animator for
//! scripting languages, editors, debuggers or xml serialization purposes.
void CParticleFadeOutAffector::serializeAttributes(io::IAttributes* out, io::SAttributeReadWriteOptions* options) const
{
  out->addColor("TargetColor", TargetColor);
  out->addFloat("FadeOutTime", FadeOutTime);
}

//! Reads attributes of the object.
//! Implement this to set the attributes of your scene node animator for
//! scripting languages, editors, debuggers or xml deserialization purposes.
//! \param startIndex: start index where to start reading attributes.
//! \return: returns last index of an attribute read by this affector
void CParticleFadeOutAffector::deserializeAttributes(io::IAttributes* in, io::SAttributeReadWriteOptions* options)
{
  TargetColor = in->getAttributeAsColor("TargetColor");
  FadeOutTime = in->getAttributeAsFloat("FadeOutTime");
}


} // namespace scene
} // namespace saga

#endif // _IRR_COMPILE_WITH_PARTICLES_
