#include "CAssimpMeshLoader.h"
#include "CMeshBuffer.h"
#include "SMesh.h"
#include "SAnimatedMesh.h"
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

namespace saga
{
namespace scene
{

CAssimpMeshLoader::CAssimpMeshLoader()
{

}

CAssimpMeshLoader::~CAssimpMeshLoader()
{

}

std::shared_ptr<IAnimatedMesh> CAssimpMeshLoader::createMesh(void* data, const std::size_t size)
{
  Assimp::Importer Importer;
  const aiScene* pScene;
  constexpr int flags =
    aiProcess_Triangulate |
    aiProcess_PreTransformVertices |
    aiProcess_CalcTangentSpace |
    aiProcess_FlipUVs |
    aiProcess_GenSmoothNormals;

  pScene = Importer.ReadFileFromMemory(data, size, flags);
  if (pScene == nullptr) return nullptr;

  auto mesh = std::make_shared<SMesh>();

	for (unsigned int i = 0; i < pScene->mNumMeshes; i++)
  {
	  const aiMesh* paiMesh = pScene->mMeshes[i];
    auto meshBuffer = std::make_unique<CMeshBuffer>();

    const auto vertexCount = pScene->mMeshes[i]->mNumVertices;

		aiColor3D pColor(0.f, 0.f, 0.f);
		pScene->mMaterials[paiMesh->mMaterialIndex]->Get(AI_MATKEY_COLOR_DIFFUSE, pColor);
    static const aiVector3D Zero3D(0.0f, 0.0f, 0.0f);

    std::vector<S3DVertex> vertices;
    std::vector<uint32_t> indices;
    vertices.reserve(vertexCount);
    for (unsigned int j = 0; j < paiMesh->mNumVertices; j++)
    {
      const aiVector3D* pPos = &(paiMesh->mVertices[j]);
      const aiVector3D* pNormal = &(paiMesh->mNormals[j]);
      const aiVector3D* pTexCoord = (paiMesh->HasTextureCoords(0)) ? &(paiMesh->mTextureCoords[0][j]) : &Zero3D;
      const aiVector3D* pTangent = (paiMesh->HasTangentsAndBitangents()) ? &(paiMesh->mTangents[j]) : &Zero3D;
      const aiVector3D* pBiTangent = (paiMesh->HasTangentsAndBitangents()) ? &(paiMesh->mBitangents[j]) : &Zero3D;

      vertices.push_back({
        { pPos->x, pPos->y, pPos->z },
        { pNormal->x, pNormal->y, pNormal->z },
        { pColor.r, pColor.g, pColor.b, 1.0 },
        { pTexCoord->x, pTexCoord->y },
        { pTangent->x, pTangent->y, pTangent->z },
        { pBiTangent->x, pBiTangent->y, pBiTangent->z },
      });
    }

    for (unsigned int j = 0; j < paiMesh->mNumFaces; j++)
    {
      const aiFace& Face = paiMesh->mFaces[j];
      if (Face.mNumIndices != 3)
        continue;

      indices.push_back(Face.mIndices[0]);
      indices.push_back(Face.mIndices[1]);
      indices.push_back(Face.mIndices[2]);
    }
    const auto indexCount = indices.size();
    meshBuffer->append(std::move(vertices), vertexCount, std::move(indices), indexCount);
    mesh->addMeshBuffer(std::move(meshBuffer));
  }
  auto animatedMesh = std::make_shared<SAnimatedMesh>(mesh);
  return animatedMesh;
}

} // namespace scene
} // namespace saga
