// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#include "CMeshManipulator.h"
#include "SMesh.h"
#include "CMeshBuffer.h"
#include "SAnimatedMesh.h"
#include "os.h"
#include "irrMap.h"
#include "triangle3d.h"

namespace saga
{
namespace scene
{

static inline glm::vec3 getAngleWeight(const glm::vec3& v1,
    const glm::vec3& v2,
    const glm::vec3& v3)
{
  // Calculate this triangle's weight for each of its three vertices
  // start by calculating the lengths of its sides
  const float a = v2.getDistanceFromSQ(v3);
  const float asqrt = sqrtf(a);
  const float b = v1.getDistanceFromSQ(v3);
  const float bsqrt = sqrtf(b);
  const float c = v1.getDistanceFromSQ(v2);
  const float csqrt = sqrtf(c);

  // use them to find the angle at each vertex
  return glm::vec3(
    acosf((b + c - a) / (2.f * bsqrt * csqrt)),
    acosf((-b + c + a) / (2.f * asqrt * csqrt)),
    acosf((b - c + a) / (2.f * bsqrt * asqrt)));
}


//! Flips the direction of surfaces. Changes backfacing triangles to frontfacing
//! triangles and vice versa.
//! \param mesh: Mesh on which the operation is performed.
void CMeshManipulator::flipSurfaces(scene::IMesh* mesh) const
{
  if (!mesh)
    return;

  const std::uint32_t bcount = mesh->getMeshBufferCount();
  for (std::uint32_t b= 0; b<bcount; ++b)
  {
    IMeshBuffer* buffer = mesh->getMeshBuffer(b);
    const std::uint32_t idxcnt = buffer->getIndexCount();
    if (buffer->getIndexType() == scene::E_INDEX_TYPE::BITS_16)
    {
      std::uint16_t* idx = buffer->getIndices();
      for (std::uint32_t i = 0; i < idxcnt; i+=3)
      {
        const std::uint16_t tmp = idx[i+1];
        idx[i+1] = idx[i+2];
        idx[i+2] = tmp;
      }
    }
    else
    {
      std::uint32_t* idx = reinterpret_cast<std::uint32_t*>(buffer->getIndices());
      for (std::uint32_t i = 0; i < idxcnt; i+=3)
      {
        const std::uint32_t tmp = idx[i+1];
        idx[i+1] = idx[i+2];
        idx[i+2] = tmp;
      }
    }
  }
}


namespace
{
template <typename T>
void recalculateNormalsT(IMeshBuffer* buffer, bool smooth, bool angleWeighted)
{
  const std::uint32_t vtxcnt = buffer->getVertexCount();
  const std::uint32_t idxcnt = buffer->getIndexCount();
  const T* idx = reinterpret_cast<T*>(buffer->getIndices());

  if (!smooth)
  {
    for (std::uint32_t i = 0; i < idxcnt; i+=3)
    {
      const glm::vec3& v1 = buffer->getPosition(idx[i+0]);
      const glm::vec3& v2 = buffer->getPosition(idx[i+1]);
      const glm::vec3& v3 = buffer->getPosition(idx[i+2]);
      const glm::vec3 normal = core::plane3d<float>(v1, v2, v3).Normal;
      buffer->getNormal(idx[i+0]) = normal;
      buffer->getNormal(idx[i+1]) = normal;
      buffer->getNormal(idx[i+2]) = normal;
    }
  }
  else
  {
    std::uint32_t i;

    for (i = 0; i!= vtxcnt; ++i)
      buffer->getNormal(i).set(0.f, 0.f, 0.f);

    for (i = 0; i < idxcnt; i+=3)
    {
      const glm::vec3& v1 = buffer->getPosition(idx[i+0]);
      const glm::vec3& v2 = buffer->getPosition(idx[i+1]);
      const glm::vec3& v3 = buffer->getPosition(idx[i+2]);
      const glm::vec3 normal = core::plane3d<float>(v1, v2, v3).Normal;

      glm::vec3 weight(1.f,1.f,1.f);
      if (angleWeighted)
        weight = saga::scene::getAngleWeight(v1,v2,v3); // writing saga::scene:: necessary for borland

      buffer->getNormal(idx[i+0]) += weight.X*normal;
      buffer->getNormal(idx[i+1]) += weight.Y*normal;
      buffer->getNormal(idx[i+2]) += weight.Z*normal;
    }

    for (i = 0; i!= vtxcnt; ++i)
      buffer->getNormal(i).normalize();
  }
}
}


//! Recalculates all normals of the mesh buffer.
/** \param buffer: Mesh buffer on which the operation is performed. */
void CMeshManipulator::recalculateNormals(IMeshBuffer* buffer, bool smooth, bool angleWeighted) const
{
  if (!buffer)
    return;

  if (buffer->getIndexType()==scene::E_INDEX_TYPE::BITS_16)
    recalculateNormalsT<std::uint16_t>(buffer, smooth, angleWeighted);
  else
    recalculateNormalsT<std::uint32_t>(buffer, smooth, angleWeighted);
}


//! Recalculates all normals of the mesh.
//! \param mesh: Mesh on which the operation is performed.
void CMeshManipulator::recalculateNormals(scene::IMesh* mesh, bool smooth, bool angleWeighted) const
{
  if (!mesh)
    return;

  const std::uint32_t bcount = mesh->getMeshBufferCount();
  for (std::uint32_t b= 0; b<bcount; ++b)
    recalculateNormals(mesh->getMeshBuffer(b), smooth, angleWeighted);
}


namespace
{
void calculateTangents(
  glm::vec3& normal,
  glm::vec3& tangent,
  glm::vec3& binormal,
  const glm::vec3& vt1, const glm::vec3& vt2, const glm::vec3& vt3, // vertices
  const glm::vec2& tc1, const glm::vec2& tc2, const glm::vec2& tc3) // texture coords
{
  // choose one of them:
  //#define USE_NVIDIA_GLH_VERSION // use version used by nvidia in glh headers
  #define USE_IRR_VERSION

#ifdef USE_IRR_VERSION

  glm::vec3 v1 = vt1 - vt2;
  glm::vec3 v2 = vt3 - vt1;
  normal = v2.crossProduct(v1);
  normal.normalize();

  // binormal

  float deltaX1 = tc1.X - tc2.X;
  float deltaX2 = tc3.X - tc1.X;
  binormal = (v1 * deltaX2) - (v2 * deltaX1);
  binormal.normalize();

  // tangent

  float deltaY1 = tc1.Y - tc2.Y;
  float deltaY2 = tc3.Y - tc1.Y;
  tangent = (v1 * deltaY2) - (v2 * deltaY1);
  tangent.normalize();

  // adjust

  glm::vec3 txb = tangent.crossProduct(binormal);
  if (txb.dotProduct(normal) < 0.0f)
  {
    tangent *= -1.0f;
    binormal *= -1.0f;
  }

#endif // USE_IRR_VERSION

#ifdef USE_NVIDIA_GLH_VERSION

  tangent.set(0,0,0);
  binormal.set(0,0,0);

  glm::vec3 v1(vt2.X - vt1.X, tc2.X - tc1.X, tc2.Y - tc1.Y);
  glm::vec3 v2(vt3.X - vt1.X, tc3.X - tc1.X, tc3.Y - tc1.Y);

  glm::vec3 txb = v1.crossProduct(v2);
  if (!core::iszero (txb.X))
  {
    tangent.X  = -txb.Y / txb.X;
    binormal.X = -txb.Z / txb.X;
  }

  v1.X = vt2.Y - vt1.Y;
  v2.X = vt3.Y - vt1.Y;
  txb = v1.crossProduct(v2);

  if (!core::iszero (txb.X))
  {
    tangent.Y  = -txb.Y / txb.X;
    binormal.Y = -txb.Z / txb.X;
  }

  v1.X = vt2.Z - vt1.Z;
  v2.X = vt3.Z - vt1.Z;
  txb = v1.crossProduct(v2);

  if (!core::iszero (txb.X))
  {
    tangent.Z  = -txb.Y / txb.X;
    binormal.Z = -txb.Z / txb.X;
  }

  tangent.normalize();
  binormal.normalize();

  normal = tangent.crossProduct(binormal);
  normal.normalize();

  binormal = tangent.crossProduct(normal);
  binormal.normalize();

  core::plane3d<float> pl(vt1, vt2, vt3);

  if(normal.dotProduct(pl.Normal) < 0.0f)
    normal *= -1.0f;

#endif // USE_NVIDIA_GLH_VERSION
}


//! Recalculates tangents for a tangent mesh buffer
template <typename T>
void recalculateTangentsT(IMeshBuffer* buffer, bool recalculateNormals, bool smooth, bool angleWeighted)
{
  if (!buffer || (buffer->getVertexType()!= scene::E_VERTEX_TYPE::TANGENTS))
    return;

  const std::uint32_t vtxCnt = buffer->getVertexCount();
  const std::uint32_t idxCnt = buffer->getIndexCount();

  T* idx = reinterpret_cast<T*>(buffer->getIndices());
  video::S3DVertexTangents* v =
    (video::S3DVertexTangents*)buffer->getVertices();

  if (smooth)
  {
    std::uint32_t i;

    for (i = 0; i!= vtxCnt; ++i)
    {
      if (recalculateNormals)
        v[i].Normal.set(0.f, 0.f, 0.f);
      v[i].Tangent.set(0.f, 0.f, 0.f);
      v[i].Binormal.set(0.f, 0.f, 0.f);
    }

    //Each vertex gets the sum of the tangents and binormals from the faces around it
    for (i = 0; i < idxCnt; i+=3)
    {
      // if this triangle is degenerate, skip it!
      if (v[idx[i+0]].Pos == v[idx[i+1]].Pos ||
        v[idx[i+0]].Pos == v[idx[i+2]].Pos ||
        v[idx[i+1]].Pos == v[idx[i+2]].Pos
        /*||
        v[idx[i+0]].TCoords == v[idx[i+1]].TCoords ||
        v[idx[i+0]].TCoords == v[idx[i+2]].TCoords ||
        v[idx[i+1]].TCoords == v[idx[i+2]].TCoords */
       )
        continue;

      //Angle-weighted normals look better, but are slightly more CPU intensive to calculate
      glm::vec3 weight(1.f,1.f,1.f);
      if (angleWeighted)
        weight = saga::scene::getAngleWeight(v[i+0].Pos,v[i+1].Pos,v[i+2].Pos);  // writing saga::scene:: necessary for borland
      glm::vec3 localNormal;
      glm::vec3 localTangent;
      glm::vec3 localBinormal;

      calculateTangents(
        localNormal,
        localTangent,
        localBinormal,
        v[idx[i+0]].Pos,
        v[idx[i+1]].Pos,
        v[idx[i+2]].Pos,
        v[idx[i+0]].TCoords,
        v[idx[i+1]].TCoords,
        v[idx[i+2]].TCoords);

      if (recalculateNormals)
        v[idx[i+0]].Normal += localNormal * weight.X;
      v[idx[i+0]].Tangent += localTangent * weight.X;
      v[idx[i+0]].Binormal += localBinormal * weight.X;

      calculateTangents(
        localNormal,
        localTangent,
        localBinormal,
        v[idx[i+1]].Pos,
        v[idx[i+2]].Pos,
        v[idx[i+0]].Pos,
        v[idx[i+1]].TCoords,
        v[idx[i+2]].TCoords,
        v[idx[i+0]].TCoords);

      if (recalculateNormals)
        v[idx[i+1]].Normal += localNormal * weight.Y;
      v[idx[i+1]].Tangent += localTangent * weight.Y;
      v[idx[i+1]].Binormal += localBinormal * weight.Y;

      calculateTangents(
        localNormal,
        localTangent,
        localBinormal,
        v[idx[i+2]].Pos,
        v[idx[i+0]].Pos,
        v[idx[i+1]].Pos,
        v[idx[i+2]].TCoords,
        v[idx[i+0]].TCoords,
        v[idx[i+1]].TCoords);

      if (recalculateNormals)
        v[idx[i+2]].Normal += localNormal * weight.Z;
      v[idx[i+2]].Tangent += localTangent * weight.Z;
      v[idx[i+2]].Binormal += localBinormal * weight.Z;
    }

    // Normalize the tangents and binormals
    if (recalculateNormals)
    {
      for (i = 0; i!= vtxCnt; ++i)
        v[i].Normal.normalize();
    }
    for (i = 0; i!= vtxCnt; ++i)
    {
      v[i].Tangent.normalize();
      v[i].Binormal.normalize();
    }
  }
  else
  {
    glm::vec3 localNormal;
    for (std::uint32_t i = 0; i < idxCnt; i+=3)
    {
      calculateTangents(
        localNormal,
        v[idx[i+0]].Tangent,
        v[idx[i+0]].Binormal,
        v[idx[i+0]].Pos,
        v[idx[i+1]].Pos,
        v[idx[i+2]].Pos,
        v[idx[i+0]].TCoords,
        v[idx[i+1]].TCoords,
        v[idx[i+2]].TCoords);
      if (recalculateNormals)
        v[idx[i+0]].Normal=localNormal;

      calculateTangents(
        localNormal,
        v[idx[i+1]].Tangent,
        v[idx[i+1]].Binormal,
        v[idx[i+1]].Pos,
        v[idx[i+2]].Pos,
        v[idx[i+0]].Pos,
        v[idx[i+1]].TCoords,
        v[idx[i+2]].TCoords,
        v[idx[i+0]].TCoords);
      if (recalculateNormals)
        v[idx[i+1]].Normal=localNormal;

      calculateTangents(
        localNormal,
        v[idx[i+2]].Tangent,
        v[idx[i+2]].Binormal,
        v[idx[i+2]].Pos,
        v[idx[i+0]].Pos,
        v[idx[i+1]].Pos,
        v[idx[i+2]].TCoords,
        v[idx[i+0]].TCoords,
        v[idx[i+1]].TCoords);
      if (recalculateNormals)
        v[idx[i+2]].Normal=localNormal;
    }
  }
}
}


//! Recalculates tangents for a tangent mesh buffer
void CMeshManipulator::recalculateTangents(IMeshBuffer* buffer, bool recalculateNormals, bool smooth, bool angleWeighted) const
{
  if (buffer && (buffer->getVertexType() == scene::E_VERTEX_TYPE::TANGENTS))
  {
    if (buffer->getIndexType() == scene::E_INDEX_TYPE::BITS_16)
      recalculateTangentsT<std::uint16_t>(buffer, recalculateNormals, smooth, angleWeighted);
    else
      recalculateTangentsT<std::uint32_t>(buffer, recalculateNormals, smooth, angleWeighted);
  }
}


//! Recalculates tangents for all tangent mesh buffers
void CMeshManipulator::recalculateTangents(IMesh* mesh, bool recalculateNormals, bool smooth, bool angleWeighted) const
{
  if (!mesh)
    return;

  const std::uint32_t meshBufferCount = mesh->getMeshBufferCount();
  for (std::uint32_t b= 0; b<meshBufferCount; ++b)
  {
    recalculateTangents(mesh->getMeshBuffer(b), recalculateNormals, smooth, angleWeighted);
  }
}


namespace
{
//! Creates a planar texture mapping on the meshbuffer
template<typename T>
void makePlanarTextureMappingT(scene::IMeshBuffer* buffer, float resolution)
{
  std::uint32_t idxcnt = buffer->getIndexCount();
  T* idx = reinterpret_cast<T*>(buffer->getIndices());

  for (std::uint32_t i = 0; i < idxcnt; i+=3)
  {
    core::plane3df p(buffer->getPosition(idx[i+0]), buffer->getPosition(idx[i+1]), buffer->getPosition(idx[i+2]));
    p.Normal.X = fabsf(p.Normal.X);
    p.Normal.Y = fabsf(p.Normal.Y);
    p.Normal.Z = fabsf(p.Normal.Z);
    // calculate planar mapping worldspace coordinates

    if (p.Normal.X > p.Normal.Y && p.Normal.X > p.Normal.Z)
    {
      for (std::uint32_t o= 0; o!=3; ++o)
      {
        buffer->getTCoords(idx[i+o]).X = buffer->getPosition(idx[i+o]).Y * resolution;
        buffer->getTCoords(idx[i+o]).Y = buffer->getPosition(idx[i+o]).Z * resolution;
      }
    }
    else
    if (p.Normal.Y > p.Normal.X && p.Normal.Y > p.Normal.Z)
    {
      for (std::uint32_t o= 0; o!=3; ++o)
      {
        buffer->getTCoords(idx[i+o]).X = buffer->getPosition(idx[i+o]).X * resolution;
        buffer->getTCoords(idx[i+o]).Y = buffer->getPosition(idx[i+o]).Z * resolution;
      }
    }
    else
    {
      for (std::uint32_t o= 0; o!=3; ++o)
      {
        buffer->getTCoords(idx[i+o]).X = buffer->getPosition(idx[i+o]).X * resolution;
        buffer->getTCoords(idx[i+o]).Y = buffer->getPosition(idx[i+o]).Y * resolution;
      }
    }
  }
}
}


//! Creates a planar texture mapping on the meshbuffer
void CMeshManipulator::makePlanarTextureMapping(scene::IMeshBuffer* buffer, float resolution) const
{
  if (!buffer)
    return;

  if (buffer->getIndexType()==scene::E_INDEX_TYPE::BITS_16)
    makePlanarTextureMappingT<std::uint16_t>(buffer, resolution);
  else
    makePlanarTextureMappingT<std::uint32_t>(buffer, resolution);
}


//! Creates a planar texture mapping on the mesh
void CMeshManipulator::makePlanarTextureMapping(scene::IMesh* mesh, float resolution) const
{
  if (!mesh)
    return;

  const std::uint32_t bcount = mesh->getMeshBufferCount();
  for (std::uint32_t b= 0; b<bcount; ++b)
  {
    makePlanarTextureMapping(mesh->getMeshBuffer(b), resolution);
  }
}


namespace
{
//! Creates a planar texture mapping on the meshbuffer
template <typename T>
void makePlanarTextureMappingT(scene::IMeshBuffer* buffer, float resolutionS, float resolutionT, u8 axis, const glm::vec3& offset)
{
  std::uint32_t idxcnt = buffer->getIndexCount();
  T* idx = reinterpret_cast<T*>(buffer->getIndices());

  for (std::uint32_t i = 0; i < idxcnt; i+=3)
  {
    // calculate planar mapping worldspace coordinates
    if (axis== 0)
    {
      for (std::uint32_t o= 0; o!=3; ++o)
      {
        buffer->getTCoords(idx[i+o]).X = 0.5f+(buffer->getPosition(idx[i+o]).Z + offset.Z) * resolutionS;
        buffer->getTCoords(idx[i+o]).Y = 0.5f-(buffer->getPosition(idx[i+o]).Y + offset.Y) * resolutionT;
      }
    }
    else if (axis==1)
    {
      for (std::uint32_t o= 0; o!=3; ++o)
      {
        buffer->getTCoords(idx[i+o]).X = 0.5f+(buffer->getPosition(idx[i+o]).X + offset.X) * resolutionS;
        buffer->getTCoords(idx[i+o]).Y = 1.f-(buffer->getPosition(idx[i+o]).Z + offset.Z) * resolutionT;
      }
    }
    else if (axis==2)
    {
      for (std::uint32_t o= 0; o!=3; ++o)
      {
        buffer->getTCoords(idx[i+o]).X = 0.5f+(buffer->getPosition(idx[i+o]).X + offset.X) * resolutionS;
        buffer->getTCoords(idx[i+o]).Y = 0.5f-(buffer->getPosition(idx[i+o]).Y + offset.Y) * resolutionT;
      }
    }
  }
}
}


//! Creates a planar texture mapping on the meshbuffer
void CMeshManipulator::makePlanarTextureMapping(scene::IMeshBuffer* buffer, float resolutionS, float resolutionT, u8 axis, const glm::vec3& offset) const
{
  if (!buffer)
    return;

  if (buffer->getIndexType()==scene::E_INDEX_TYPE::BITS_16)
    makePlanarTextureMappingT<std::uint16_t>(buffer, resolutionS, resolutionT, axis, offset);
  else
    makePlanarTextureMappingT<std::uint32_t>(buffer, resolutionS, resolutionT, axis, offset);
}


//! Creates a planar texture mapping on the mesh
void CMeshManipulator::makePlanarTextureMapping(scene::IMesh* mesh, float resolutionS, float resolutionT, u8 axis, const glm::vec3& offset) const
{
  if (!mesh)
    return;

  const std::uint32_t bcount = mesh->getMeshBufferCount();
  for (std::uint32_t b= 0; b<bcount; ++b)
  {
    makePlanarTextureMapping(mesh->getMeshBuffer(b), resolutionS, resolutionT, axis, offset);
  }
}


//! Clones a static IMesh into a modifyable SMesh.
// not yet 32bit
SMesh* CMeshManipulator::createMeshCopy(scene::IMesh* mesh) const
{
  if (!mesh)
    return 0;

  SMesh* clone = new SMesh();

  const std::uint32_t meshBufferCount = mesh->getMeshBufferCount();

  for (std::uint32_t b= 0; b<meshBufferCount; ++b)
  {
    const IMeshBuffer* const mb = mesh->getMeshBuffer(b);
    switch(mb->getVertexType())
    {
    case scene::E_VERTEX_TYPE::STANDARD:
      {
        SMeshBuffer* buffer = new SMeshBuffer();
        buffer->Material = mb->getMaterial();
        const std::uint32_t vcount = mb->getVertexCount();
        buffer->Vertices.reallocate(vcount);
        video::S3DVertex* vertices = (video::S3DVertex*)mb->getVertices();
        for (std::uint32_t i = 0; i < vcount; ++i)
          buffer->Vertices.push_back(vertices[i]);
        const std::uint32_t icount = mb->getIndexCount();
        buffer->Indices.reallocate(icount);
        const std::uint16_t* indices = mb->getIndices();
        for (std::uint32_t i = 0; i < icount; ++i)
          buffer->Indices.push_back(indices[i]);
        clone->addMeshBuffer(buffer);
        buffer->drop();
      }
      break;
    case scene::E_VERTEX_TYPE::TWO_TCOORDS:
      {
        SMeshBufferLightMap* buffer = new SMeshBufferLightMap();
        buffer->Material = mb->getMaterial();
        const std::uint32_t vcount = mb->getVertexCount();
        buffer->Vertices.reallocate(vcount);
        video::S3DVertex2TCoords* vertices = (video::S3DVertex2TCoords*)mb->getVertices();
        for (std::uint32_t i = 0; i < vcount; ++i)
          buffer->Vertices.push_back(vertices[i]);
        const std::uint32_t icount = mb->getIndexCount();
        buffer->Indices.reallocate(icount);
        const std::uint16_t* indices = mb->getIndices();
        for (std::uint32_t i = 0; i < icount; ++i)
          buffer->Indices.push_back(indices[i]);
        clone->addMeshBuffer(buffer);
        buffer->drop();
      }
      break;
    case scene::E_VERTEX_TYPE::TANGENTS:
      {
        SMeshBufferTangents* buffer = new SMeshBufferTangents();
        buffer->Material = mb->getMaterial();
        const std::uint32_t vcount = mb->getVertexCount();
        buffer->Vertices.reallocate(vcount);
        video::S3DVertexTangents* vertices = (video::S3DVertexTangents*)mb->getVertices();
        for (std::uint32_t i = 0; i < vcount; ++i)
          buffer->Vertices.push_back(vertices[i]);
        const std::uint32_t icount = mb->getIndexCount();
        buffer->Indices.reallocate(icount);
        const std::uint16_t* indices = mb->getIndices();
        for (std::uint32_t i = 0; i < icount; ++i)
          buffer->Indices.push_back(indices[i]);
        clone->addMeshBuffer(buffer);
        buffer->drop();
      }
      break;
    }// end switch

  }// end for all mesh buffers

  clone->BoundingBox = mesh->getBoundingBox();
  return clone;
}


//! Creates a copy of the mesh, which will only consist of unique primitives
// not yet 32bit
IMesh* CMeshManipulator::createMeshUniquePrimitives(IMesh* mesh) const
{
  if (!mesh)
    return 0;

  SMesh* clone = new SMesh();

  const std::uint32_t meshBufferCount = mesh->getMeshBufferCount();

  for (std::uint32_t b= 0; b<meshBufferCount; ++b)
  {
    const IMeshBuffer* const mb = mesh->getMeshBuffer(b);
    const std::int32_t idxCnt = mb->getIndexCount();
    const std::uint16_t* idx = mb->getIndices();

    switch(mb->getVertexType())
    {
    case scene::E_VERTEX_TYPE::STANDARD:
      {
        SMeshBuffer* buffer = new SMeshBuffer();
        buffer->Material = mb->getMaterial();

        video::S3DVertex* v =
          (video::S3DVertex*)mb->getVertices();

        buffer->Vertices.reallocate(idxCnt);
        buffer->Indices.reallocate(idxCnt);
        for (std::int32_t i = 0; i < idxCnt; i += 3)
        {
          buffer->Vertices.push_back(v[idx[i + 0 ]]);
          buffer->Vertices.push_back(v[idx[i + 1 ]]);
          buffer->Vertices.push_back(v[idx[i + 2 ]]);

          buffer->Indices.push_back(i + 0);
          buffer->Indices.push_back(i + 1);
          buffer->Indices.push_back(i + 2);
        }

        buffer->setBoundingBox(mb->getBoundingBox());
        clone->addMeshBuffer(buffer);
        buffer->drop();
      }
      break;
    case scene::E_VERTEX_TYPE::TWO_TCOORDS:
      {
        SMeshBufferLightMap* buffer = new SMeshBufferLightMap();
        buffer->Material = mb->getMaterial();

        video::S3DVertex2TCoords* v =
          (video::S3DVertex2TCoords*)mb->getVertices();

        buffer->Vertices.reallocate(idxCnt);
        buffer->Indices.reallocate(idxCnt);
        for (std::int32_t i = 0; i < idxCnt; i += 3)
        {
          buffer->Vertices.push_back(v[idx[i + 0 ]]);
          buffer->Vertices.push_back(v[idx[i + 1 ]]);
          buffer->Vertices.push_back(v[idx[i + 2 ]]);

          buffer->Indices.push_back(i + 0);
          buffer->Indices.push_back(i + 1);
          buffer->Indices.push_back(i + 2);
        }
        buffer->setBoundingBox(mb->getBoundingBox());
        clone->addMeshBuffer(buffer);
        buffer->drop();
      }
      break;
    case scene::E_VERTEX_TYPE::TANGENTS:
      {
        SMeshBufferTangents* buffer = new SMeshBufferTangents();
        buffer->Material = mb->getMaterial();

        video::S3DVertexTangents* v =
          (video::S3DVertexTangents*)mb->getVertices();

        buffer->Vertices.reallocate(idxCnt);
        buffer->Indices.reallocate(idxCnt);
        for (std::int32_t i = 0; i < idxCnt; i += 3)
        {
          buffer->Vertices.push_back(v[idx[i + 0 ]]);
          buffer->Vertices.push_back(v[idx[i + 1 ]]);
          buffer->Vertices.push_back(v[idx[i + 2 ]]);

          buffer->Indices.push_back(i + 0);
          buffer->Indices.push_back(i + 1);
          buffer->Indices.push_back(i + 2);
        }

        buffer->setBoundingBox(mb->getBoundingBox());
        clone->addMeshBuffer(buffer);
        buffer->drop();
      }
      break;
    }// end switch

  }// end for all mesh buffers

  clone->BoundingBox = mesh->getBoundingBox();
  return clone;
}


//! Creates a copy of a mesh, which will have identical vertices welded together
// not yet 32bit
IMesh* CMeshManipulator::createMeshWelded(IMesh *mesh, float tolerance) const
{
  SMesh* clone = new SMesh();
  clone->BoundingBox = mesh->getBoundingBox();

  std::vector<std::uint16_t> redirects;

  for (std::uint32_t b= 0; b<mesh->getMeshBufferCount(); ++b)
  {
    const IMeshBuffer* const mb = mesh->getMeshBuffer(b);
    // reset redirect list
    redirects.set_used(mb->getVertexCount());

    const std::uint16_t* indices = 0;
    std::uint32_t indexCount = 0;
    std::vector<std::uint16_t>* outIdx = 0;

    switch(mb->getVertexType())
    {
    case scene::E_VERTEX_TYPE::STANDARD:
    {
      SMeshBuffer* buffer = new SMeshBuffer();
      buffer->BoundingBox = mb->getBoundingBox();
      buffer->Material = mb->getMaterial();
      clone->addMeshBuffer(buffer);
      buffer->drop();

      video::S3DVertex* v =
          (video::S3DVertex*)mb->getVertices();

      std::uint32_t vertexCount = mb->getVertexCount();

      indices = mb->getIndices();
      indexCount = mb->getIndexCount();
      outIdx = &buffer->Indices;

      buffer->Vertices.reallocate(vertexCount);

      for (std::uint32_t i = 0; i < vertexCount; ++i)
      {
        bool found = false;
        for (std::uint32_t j= 0; j < i; ++j)
        {
          if (v[i].Pos.equals(v[j].Pos, tolerance) &&
             v[i].Normal.equals(v[j].Normal, tolerance) &&
             v[i].TCoords.equals(v[j].TCoords) &&
            (v[i].Color == v[j].Color))
          {
            redirects[i] = redirects[j];
            found = true;
            break;
          }
        }
        if (!found)
        {
          redirects[i] = buffer->Vertices.size();
          buffer->Vertices.push_back(v[i]);
        }
      }

      break;
    }
    case scene::E_VERTEX_TYPE::TWO_TCOORDS:
    {
      SMeshBufferLightMap* buffer = new SMeshBufferLightMap();
      buffer->BoundingBox = mb->getBoundingBox();
      buffer->Material = mb->getMaterial();
      clone->addMeshBuffer(buffer);
      buffer->drop();

      video::S3DVertex2TCoords* v =
          (video::S3DVertex2TCoords*)mb->getVertices();

      std::uint32_t vertexCount = mb->getVertexCount();

      indices = mb->getIndices();
      indexCount = mb->getIndexCount();
      outIdx = &buffer->Indices;

      buffer->Vertices.reallocate(vertexCount);

      for (std::uint32_t i = 0; i < vertexCount; ++i)
      {
        bool found = false;
        for (std::uint32_t j= 0; j < i; ++j)
        {
          if (v[i].Pos.equals(v[j].Pos, tolerance) &&
             v[i].Normal.equals(v[j].Normal, tolerance) &&
             v[i].TCoords.equals(v[j].TCoords) &&
             v[i].TCoords2.equals(v[j].TCoords2) &&
            (v[i].Color == v[j].Color))
          {
            redirects[i] = redirects[j];
            found = true;
            break;
          }
        }
        if (!found)
        {
          redirects[i] = buffer->Vertices.size();
          buffer->Vertices.push_back(v[i]);
        }
      }
      break;
    }
    case scene::E_VERTEX_TYPE::TANGENTS:
    {
      SMeshBufferTangents* buffer = new SMeshBufferTangents();
      buffer->BoundingBox = mb->getBoundingBox();
      buffer->Material = mb->getMaterial();
      clone->addMeshBuffer(buffer);
      buffer->drop();

      video::S3DVertexTangents* v =
          (video::S3DVertexTangents*)mb->getVertices();

      std::uint32_t vertexCount = mb->getVertexCount();

      indices = mb->getIndices();
      indexCount = mb->getIndexCount();
      outIdx = &buffer->Indices;

      buffer->Vertices.reallocate(vertexCount);

      for (std::uint32_t i = 0; i < vertexCount; ++i)
      {
        bool found = false;
        for (std::uint32_t j= 0; j < i; ++j)
        {
          if (v[i].Pos.equals(v[j].Pos, tolerance) &&
             v[i].Normal.equals(v[j].Normal, tolerance) &&
             v[i].TCoords.equals(v[j].TCoords) &&
             v[i].Tangent.equals(v[j].Tangent, tolerance) &&
             v[i].Binormal.equals(v[j].Binormal, tolerance) &&
            (v[i].Color == v[j].Color))
          {
            redirects[i] = redirects[j];
            found = true;
            break;
          }
        }
        if (!found)
        {
          redirects[i] = buffer->Vertices.size();
          buffer->Vertices.push_back(v[i]);
        }
      }
      break;
    }
    default:
      os::Printer::log("Cannot create welded mesh, vertex type unsupported", ELL_ERROR);
      break;
    }

    // Clean up any degenerate tris
     std::vector<std::uint16_t> &Indices = *outIdx;
    Indices.clear();
    Indices.reallocate(indexCount);
    for (std::uint32_t i = 0; i < indexCount; i+=3)
     {
      std::uint16_t a, b, c;
      a = redirects[indices[i]];
      b = redirects[indices[i+1]];
      c = redirects[indices[i+2]];

      bool drop = false;

      if (a == b || b == c || a == c)
        drop = true;

      // Open for other checks

      if (!drop)
      {
        Indices.push_back(a);
        Indices.push_back(b);
        Indices.push_back(c);
      }
    }
  }
  return clone;
}


//! Creates a copy of the mesh, which will only consist of S3DVertexTangents vertices.
// not yet 32bit
IMesh* CMeshManipulator::createMeshWithTangents(IMesh* mesh, bool recalculateNormals, bool smooth, bool angleWeighted, bool calculateTangents) const
{
  using namespace video;

  if (!mesh)
    return 0;

  // copy mesh and fill data into SMeshBufferTangents
  SMesh* clone = new SMesh();
  const std::uint32_t meshBufferCount = mesh->getMeshBufferCount();

  for (std::uint32_t b= 0; b<meshBufferCount; ++b)
  {
    const IMeshBuffer* const original = mesh->getMeshBuffer(b);
    SMeshBufferTangents* buffer = new SMeshBufferTangents();

    // copy material
    buffer->Material = original->getMaterial();

    // copy indices
    const std::uint32_t idxCnt = original->getIndexCount();
    const std::uint16_t* indices = original->getIndices();
    buffer->Indices.reallocate(idxCnt);
    for (std::uint32_t i = 0; i < idxCnt; ++i)
      buffer->Indices.push_back(indices[i]);

    // copy vertices
    const std::uint32_t vtxCnt = original->getVertexCount();
    buffer->Vertices.reallocate(vtxCnt);

    const E_VERTEX_TYPE vType = original->getVertexType();
    switch(vType)
    {
    case scene::E_VERTEX_TYPE::STANDARD:
      {
        const S3DVertex* v = (const S3DVertex*)original->getVertices();

        for (std::uint32_t i = 0; i < vtxCnt; ++i)
          buffer->Vertices.push_back(S3DVertexTangents(
            v[i].Pos, v[i].Normal, v[i].Color, v[i].TCoords));
      }
      break;
    case scene::E_VERTEX_TYPE::TWO_TCOORDS:
      {
        const S3DVertex2TCoords* v =(const S3DVertex2TCoords*)original->getVertices();

        for (std::uint32_t i = 0; i < vtxCnt; ++i)
          buffer->Vertices.push_back(S3DVertexTangents(
            v[i].Pos, v[i].Normal, v[i].Color, v[i].TCoords));
      }
      break;
    case scene::E_VERTEX_TYPE::TANGENTS:
      {
        const S3DVertexTangents* v =(const S3DVertexTangents*)original->getVertices();

        for (std::uint32_t i = 0; i < vtxCnt; ++i)
          buffer->Vertices.push_back(v[i]);
      }
      break;
    }
    buffer->recalculateBoundingBox();

    // add new buffer
    clone->addMeshBuffer(buffer);
    buffer->drop();
  }

  clone->recalculateBoundingBox();
  if (calculateTangents)
    recalculateTangents(clone, recalculateNormals, smooth, angleWeighted);

  return clone;
}

namespace
{

struct height_edge
{
  std::uint32_t far;

  std::uint32_t polycount;
  std::uint32_t polys[2];
  glm::vec3 normal[2];
};

enum
{
  HEIGHT_TRIACCEL_MAX = 1024
};

}

//! Optimizes the mesh using an algorithm tuned for heightmaps.
void CMeshManipulator::heightmapOptimizeMesh(IMesh * const m, const float tolerance) const
{
  const std::uint32_t max = m->getMeshBufferCount();

  for (std::uint32_t i = 0; i < max; i++)
  {
    IMeshBuffer * const mb = m->getMeshBuffer(i);

    heightmapOptimizeMesh(mb, tolerance);
  }
}

//! Optimizes the mesh using an algorithm tuned for heightmaps.
void CMeshManipulator::heightmapOptimizeMesh(IMeshBuffer * const mb, const float tolerance) const
{
  using namespace core;
  using namespace video;

  array<height_edge> edges;

  const std::uint32_t idxs = mb->getIndexCount();
  const std::uint32_t verts = mb->getVertexCount();

  std::uint16_t *ind = mb->getIndices();
  S3DVertex *vert = (S3DVertex *) mb->getVertices();

  // First an acceleration structure: given this vert, which triangles touch it?
  // Using this drops two exponents off the algorightm complexity, O(n^4) > O(n^2)
  // Other optimizations brought it down to O(n).
  std::uint32_t **accel = (std::uint32_t **) malloc(verts * sizeof(std::uint32_t *));
  for (std::uint32_t i = 0; i < verts; i++)
  {
    accel[i] = (std::uint32_t *) calloc(HEIGHT_TRIACCEL_MAX, sizeof(std::uint32_t));
    for (std::uint32_t j = 0; j < HEIGHT_TRIACCEL_MAX; j++)
    {
      accel[i][j] = USHRT_MAX;
    }
  }

  std::uint16_t *cur = (std::uint16_t *) calloc(verts, sizeof(std::uint16_t));
  for (std::uint32_t j = 0; j < idxs; j+=3)
  {
    std::uint32_t v = ind[j];

    if (cur[v] >= HEIGHT_TRIACCEL_MAX)
    {
      os::Printer::log("Too complex mesh to optimize, aborting.");
      goto donehere;
    }

    accel[v][cur[v]] = j;
    cur[v]++;

    // Unrolled tri loop, parts 2 and 3
    v = ind[j+1];

    if (cur[v] >= HEIGHT_TRIACCEL_MAX)
    {
      os::Printer::log("Too complex mesh to optimize, aborting.");
      goto donehere;
    }

    accel[v][cur[v]] = j;
    cur[v]++;

    v = ind[j+2];

    if (cur[v] >= HEIGHT_TRIACCEL_MAX)
    {
      os::Printer::log("Too complex mesh to optimize, aborting.");
      goto donehere;
    }

    accel[v][cur[v]] = j;
    cur[v]++;
  }
  free(cur);

  // Built, go
  for (std::uint32_t i = 0; i < verts; i++)
  {
    const vector3df &mypos = vert[i].Pos;

    // find all edges of this vert
    edges.clear();

    bool gotonext = false;
    std::uint32_t j;
    std::uint16_t cur;
    for (cur = 0; accel[i][cur] != USHRT_MAX && cur < HEIGHT_TRIACCEL_MAX; cur++)
    {
      j = accel[i][cur];

      std::uint32_t far1 = -1, far2 = -1;
      if (ind[j] == i)
      {
        far1 = ind[j+1];
        far2 = ind[j+2];
      }
      else if (ind[j+1] == i)
      {
        far1 = ind[j];
        far2 = ind[j+2];
      }
      else if (ind[j+2] == i)
      {
        far1 = ind[j];
        far2 = ind[j+1];
      }

      // Skip degenerate tris
      if (vert[i].Pos == vert[far1].Pos ||
        vert[far1].Pos == vert[far2].Pos)
      {
//        puts("skipping degenerate tri");
        continue;
      }

      // Edges found, check if we already added them
      const std::uint32_t ecount = edges.size();
      bool far1new = true, far2new = true;

      for (std::uint32_t e = 0; e < ecount; e++)
      {
        if (edges[e].far == far1 ||
          edges[e].far == far2)
        {

          // Skip if over 2 polys
          if (edges[e].polycount > 2)
          {
            gotonext = true;
            goto almostnext;
          }
          edges[e].polys[edges[e].polycount] = j;
          edges[e].normal[edges[e].polycount] =
            vert[i].Normal;
          edges[e].polycount++;

          if (edges[e].far == far1)
            far1new = false;
          else
            far2new = false;
        }
      }

      if (far1new)
      {
        // New edge
        height_edge ed;

        ed.far = far1;
        ed.polycount = 1;
        ed.polys[0] = j;
        ed.normal[0] = vert[i].Normal;

        edges.push_back(ed);
      }
      if (far2new)
      {
        // New edge
        height_edge ed;

        ed.far = far2;
        ed.polycount = 1;
        ed.polys[0] = j;
        ed.normal[0] = vert[i].Normal;

        edges.push_back(ed);
      }
    }

    almostnext:
    if (gotonext)
      continue;

    // Edges found. Possible to simplify?

    const std::uint32_t ecount = edges.size();
//    printf("Vert %u has %u edges\n", i, ecount);
    for (std::uint32_t e = 0; e < ecount; e++)
    {
      for (std::uint32_t f = 0; f < ecount; f++)
      {
        if (f == e) continue;

        vector3df one = mypos - vert[edges[e].far].Pos;
        vector3df two = vert[edges[f].far].Pos - mypos;

        one.normalize();
        two.normalize();

        // Straight line ?
        if (!one.equals(two, tolerance) || one.getLengthSQ() < 0.5f)
          continue;

        // All other edges must have two polys
        for (std::uint32_t g = 0; g < ecount; g++)
        {
          if (g == e || g == f)
            continue;

          if (edges[g].polycount != 2)
          {
//            printf("%u: polycount not 2 (%u)\n",
//              g, edges[g].polycount);
            goto testnext;
          }

          // Normals must match
          if (!edges[g].normal[0].equals(edges[g].normal[1],
            tolerance))
          {
//            puts("Normals don't match");
            goto testnext;
          }

          // Normals must not flip
          for (std::uint32_t z = 0; z < edges[g].polycount; z++)
          {
            bool flat = false;
            vector3df pos[3];
            pos[0] =
              vert[ind[edges[g].polys[z]]].Pos;
            pos[1] =
              vert[ind[edges[g].polys[z] + 1]].Pos;
            pos[2] =
              vert[ind[edges[g].polys[z] + 2]].Pos;

            for (std::uint32_t y = 0; y < 3; y++)
            {
              if (edges[g].polys[z] + y == i)
              {
                pos[y] = vert[edges[e].far].Pos;
              }
              else if (edges[g].polys[z] + y
                == edges[e].far)
              {
                flat = true;
                break;
              }
            }
            if (!flat)
            {
              triangle3df temp(pos[0],
                pos[1], pos[2]);
              vector3df N = temp.getNormal();
              N.normalize();
//              if (N.getLengthSQ() < 0.5f)
//                puts("empty");

              if (!N.equals(edges[g].normal[z], tolerance))
              {
//                puts("wouldflip");
                goto testnext;
              }
            }
          }

          // Must not be on model edge
          if (edges[g].polycount == 1)
          {
            goto testnext;
          }

        }

        // Must not be on model edge
        if (edges[e].polycount == 1)
        {
          goto testnext;
        }

        // OK, moving to welding position
        vert[i] = vert[edges[e].far];
//        printf("Contracted vert %u to %u\n",
//          i, edges[e].far);
      }
    }


    testnext:;
  }

donehere:
  for (std::uint32_t i = 0; i < verts; i++)
  {
    free(accel[i]);
  }
  free(accel);
}

//! Creates a copy of the mesh, which will only consist of S3DVertex2TCoords vertices.
// not yet 32bit
IMesh* CMeshManipulator::createMeshWith2TCoords(IMesh* mesh) const
{
  using namespace video;

  if (!mesh)
    return 0;

  // copy mesh and fill data into SMeshBufferLightMap

  SMesh* clone = new SMesh();
  const std::uint32_t meshBufferCount = mesh->getMeshBufferCount();

  for (std::uint32_t b= 0; b<meshBufferCount; ++b)
  {
    const IMeshBuffer* const original = mesh->getMeshBuffer(b);
    SMeshBufferLightMap* buffer = new SMeshBufferLightMap();

    // copy material
    buffer->Material = original->getMaterial();

    // copy indices
    const std::uint32_t idxCnt = original->getIndexCount();
    const std::uint16_t* indices = original->getIndices();
    buffer->Indices.reallocate(idxCnt);
    for (std::uint32_t i = 0; i < idxCnt; ++i)
      buffer->Indices.push_back(indices[i]);

    // copy vertices
    const std::uint32_t vtxCnt = original->getVertexCount();
    buffer->Vertices.reallocate(vtxCnt);

    const scene::E_VERTEX_TYPE vType = original->getVertexType();
    switch(vType)
    {
    case scene::E_VERTEX_TYPE::STANDARD:
      {
        const S3DVertex* v = (const S3DVertex*)original->getVertices();

        for (std::uint32_t i = 0; i < vtxCnt; ++i)
          buffer->Vertices.push_back(video::S3DVertex2TCoords(
            v[i].Pos, v[i].Normal, v[i].Color, v[i].TCoords, v[i].TCoords));
      }
      break;
    case scene::E_VERTEX_TYPE::TWO_TCOORDS:
      {
        const S3DVertex2TCoords* v =(const S3DVertex2TCoords*)original->getVertices();
        for (std::uint32_t i = 0; i < vtxCnt; ++i)
          buffer->Vertices.push_back(v[i]);
      }
      break;
    case scene::E_VERTEX_TYPE::TANGENTS:
      {
        const S3DVertexTangents* v =(const S3DVertexTangents*)original->getVertices();

        for (std::uint32_t i = 0; i < vtxCnt; ++i)
          buffer->Vertices.push_back(S3DVertex2TCoords(
            v[i].Pos, v[i].Normal, v[i].Color, v[i].TCoords, v[i].TCoords));
      }
      break;
    }
    buffer->recalculateBoundingBox();

    // add new buffer
    clone->addMeshBuffer(buffer);
    buffer->drop();
  }

  clone->recalculateBoundingBox();
  return clone;
}


//! Creates a copy of the mesh, which will only consist of S3DVertex vertices.
// not yet 32bit
IMesh* CMeshManipulator::createMeshWith1TCoords(IMesh* mesh) const
{
  using namespace video;

  if (!mesh)
    return 0;

  // copy mesh and fill data into SMeshBuffer
  SMesh* clone = new SMesh();
  const std::uint32_t meshBufferCount = mesh->getMeshBufferCount();

  for (std::uint32_t b= 0; b<meshBufferCount; ++b)
  {
    const IMeshBuffer* const original = mesh->getMeshBuffer(b);
    SMeshBuffer* buffer = new SMeshBuffer();

    // copy material
    buffer->Material = original->getMaterial();

    // copy indices
    const std::uint32_t idxCnt = original->getIndexCount();
    const std::uint16_t* indices = original->getIndices();
    buffer->Indices.reallocate(idxCnt);
    for (std::uint32_t i = 0; i < idxCnt; ++i)
      buffer->Indices.push_back(indices[i]);

    // copy vertices
    const std::uint32_t vtxCnt = original->getVertexCount();
    buffer->Vertices.reallocate(vtxCnt);

    const scene::E_VERTEX_TYPE vType = original->getVertexType();
    switch(vType)
    {
    case scene::E_VERTEX_TYPE::STANDARD:
      {
        const S3DVertex* v = (const S3DVertex*)original->getVertices();

        for (std::uint32_t i = 0; i < vtxCnt; ++i)
          buffer->Vertices.push_back(v[i]);
      }
      break;
    case scene::E_VERTEX_TYPE::TWO_TCOORDS:
      {
        const S3DVertex2TCoords* v =(const S3DVertex2TCoords*)original->getVertices();

        for (std::uint32_t i = 0; i < vtxCnt; ++i)
          buffer->Vertices.push_back(S3DVertex(
            v[i].Pos, v[i].Normal, v[i].Color, v[i].TCoords));
      }
      break;
    case scene::E_VERTEX_TYPE::TANGENTS:
      {
        const S3DVertexTangents* v =(const S3DVertexTangents*)original->getVertices();

        for (std::uint32_t i = 0; i < vtxCnt; ++i)
          buffer->Vertices.push_back(S3DVertex(
            v[i].Pos, v[i].Normal, v[i].Color, v[i].TCoords));
      }
      break;
    }

    buffer->recalculateBoundingBox();
    // add new buffer
    clone->addMeshBuffer(buffer);
    buffer->drop();
  }

  clone->recalculateBoundingBox();
  return clone;
}


//! Returns amount of polygons in mesh.
std::int32_t CMeshManipulator::getPolyCount(scene::IMesh* mesh) const
{
  if (!mesh)
    return 0;

  std::int32_t trianglecount = 0;

  for (std::uint32_t g= 0; g<mesh->getMeshBufferCount(); ++g)
    trianglecount += mesh->getMeshBuffer(g)->getIndexCount() / 3;

  return trianglecount;
}


//! Returns amount of polygons in mesh.
std::int32_t CMeshManipulator::getPolyCount(scene::IAnimatedMesh* mesh) const
{
  if (mesh && mesh->getFrameCount() != 0)
    return getPolyCount(mesh->getMesh(0));

  return 0;
}


//! create a new AnimatedMesh and adds the mesh to it
IAnimatedMesh * CMeshManipulator::createAnimatedMesh(scene::IMesh* mesh, scene::E_ANIMATED_MESH_TYPE type) const
{
  return new SAnimatedMesh(mesh, type);
}

namespace
{

struct vcache
{
  std::vector<std::uint32_t> tris;
  float score;
  std::int16_t cachepos;
  std::uint16_t NumActiveTris;
};

struct tcache
{
  std::uint16_t ind[3];
  float score;
  bool drawn;
};

const std::uint16_t cachesize = 32;

float FindVertexScore(vcache *v)
{
  const float CacheDecayPower = 1.5f;
  const float LastTriScore = 0.75f;
  const float ValenceBoostScale = 2.0f;
  const float ValenceBoostPower = 0.5f;
  const float MaxSizeVertexCache = 32.0f;

  if (v->NumActiveTris == 0)
  {
    // No tri needs this vertex!
    return -1.0f;
  }

  float Score = 0.0f;
  int CachePosition = v->cachepos;
  if (CachePosition < 0)
  {
    // Vertex is not in FIFO cache - no score.
  }
  else
  {
    if (CachePosition < 3)
    {
      // This vertex was used in the last triangle,
      // so it has a fixed score.
      Score = LastTriScore;
    }
    else
    {
      // Points for being high in the cache.
      const float Scaler = 1.0f / (MaxSizeVertexCache - 3);
      Score = 1.0f - (CachePosition - 3) * Scaler;
      Score = powf(Score, CacheDecayPower);
    }
  }

  // Bonus points for having a low number of tris still to
  // use the vert, so we get rid of lone verts quickly.
  float ValenceBoost = powf(v->NumActiveTris,
        -ValenceBoostPower);
  Score += ValenceBoostScale * ValenceBoost;

  return Score;
}

/*
  A specialized LRU cache for the Forsyth algorithm.
*/

class f_lru
{

public:
  f_lru(vcache *v, tcache *t): vc(v), tc(t)
  {
    for (std::uint16_t i = 0; i < cachesize; i++)
    {
      cache[i] = -1;
    }
  }

  // Adds this vertex index and returns the highest-scoring triangle index
  std::uint32_t add(std::uint16_t vert, bool updatetris = false)
  {
    bool found = false;

    // Mark existing pos as empty
    for (std::uint16_t i = 0; i < cachesize; i++)
    {
      if (cache[i] == vert)
      {
        // Move everything down
        for (std::uint16_t j = i; j; j--)
        {
          cache[j] = cache[j - 1];
        }

        found = true;
        break;
      }
    }

    if (!found)
    {
      if (cache[cachesize-1] != -1)
        vc[cache[cachesize-1]].cachepos = -1;

      // Move everything down
      for (std::uint16_t i = cachesize - 1; i; i--)
      {
        cache[i] = cache[i - 1];
      }
    }

    cache[0] = vert;

    std::uint32_t highest = 0;
    float hiscore = 0;

    if (updatetris)
    {
      // Update cache positions
      for (std::uint16_t i = 0; i < cachesize; i++)
      {
        if (cache[i] == -1)
          break;

        vc[cache[i]].cachepos = i;
        vc[cache[i]].score = FindVertexScore(&vc[cache[i]]);
      }

      // Update triangle scores
      for (std::uint16_t i = 0; i < cachesize; i++)
      {
        if (cache[i] == -1)
          break;

        const std::uint16_t trisize = vc[cache[i]].tris.size();
        for (std::uint16_t t = 0; t < trisize; t++)
        {
          tcache *tri = &tc[vc[cache[i]].tris[t]];

          tri->score =
            vc[tri->ind[0]].score +
            vc[tri->ind[1]].score +
            vc[tri->ind[2]].score;

          if (tri->score > hiscore)
          {
            hiscore = tri->score;
            highest = vc[cache[i]].tris[t];
          }
        }
      }
    }

    return highest;
  }

private:
  std::int32_t cache[cachesize];
  vcache *vc;
  tcache *tc;
};

} // end anonymous namespace

/**
Vertex cache optimization according to the Forsyth paper:
http://home.comcast.net/~tom_forsyth/papers/fast_vE_RENDER_TARGET_TYPE::cache_opt.html

The function is thread-safe (read: you can optimize several meshes in different threads)

\param mesh Source mesh for the operation.  */
IMesh* CMeshManipulator::createForsythOptimizedMesh(const IMesh *mesh) const
{
  if (!mesh)
    return 0;

  SMesh *newmesh = new SMesh();
  newmesh->BoundingBox = mesh->getBoundingBox();

  const std::uint32_t mbcount = mesh->getMeshBufferCount();

  for (std::uint32_t b = 0; b < mbcount; ++b)
  {
    const IMeshBuffer *mb = mesh->getMeshBuffer(b);

    if (mb->getIndexType() != scene::E_INDEX_TYPE::BITS_16)
    {
      os::Printer::log("Cannot optimize a mesh with 32bit indices", ELL_ERROR);
      newmesh->drop();
      return 0;
    }

    const std::uint32_t icount = mb->getIndexCount();
    const std::uint32_t tcount = icount / 3;
    const std::uint32_t vcount = mb->getVertexCount();
    const std::uint16_t *ind = mb->getIndices();

    vcache *vc = new vcache[vcount];
    tcache *tc = new tcache[tcount];

    f_lru lru(vc, tc);

    // init
    for (std::uint16_t i = 0; i < vcount; i++)
    {
      vc[i].score = 0;
      vc[i].cachepos = -1;
      vc[i].NumActiveTris = 0;
    }

    // First pass: count how many times a vert is used
    for (std::uint32_t i = 0; i < icount; i += 3)
    {
      vc[ind[i]].NumActiveTris++;
      vc[ind[i + 1]].NumActiveTris++;
      vc[ind[i + 2]].NumActiveTris++;

      const std::uint32_t tri_ind = i/3;
      tc[tri_ind].ind[0] = ind[i];
      tc[tri_ind].ind[1] = ind[i + 1];
      tc[tri_ind].ind[2] = ind[i + 2];
    }

    // Second pass: list of each triangle
    for (std::uint32_t i = 0; i < tcount; i++)
    {
      vc[tc[i].ind[0]].tris.push_back(i);
      vc[tc[i].ind[1]].tris.push_back(i);
      vc[tc[i].ind[2]].tris.push_back(i);

      tc[i].drawn = false;
    }

    // Give initial scores
    for (std::uint16_t i = 0; i < vcount; i++)
    {
      vc[i].score = FindVertexScore(&vc[i]);
    }
    for (std::uint32_t i = 0; i < tcount; i++)
    {
      tc[i].score =
          vc[tc[i].ind[0]].score +
          vc[tc[i].ind[1]].score +
          vc[tc[i].ind[2]].score;
    }

    switch(mb->getVertexType())
    {
      case scene::E_VERTEX_TYPE::STANDARD:
      {
        video::S3DVertex *v = (video::S3DVertex *) mb->getVertices();

        SMeshBuffer *buf = new SMeshBuffer();
        buf->Material = mb->getMaterial();

        buf->Vertices.reallocate(vcount);
        buf->Indices.reallocate(icount);

        core::map<const video::S3DVertex, const std::uint16_t> sind; // search index for fast operation
        typedef core::map<const video::S3DVertex, const std::uint16_t>::Node snode;

        // Main algorithm
        std::uint32_t highest = 0;
        std::uint32_t drawcalls = 0;
        for (;;)
        {
          if (tc[highest].drawn)
          {
            bool found = false;
            float hiscore = 0;
            for (std::uint32_t t = 0; t < tcount; t++)
            {
              if (!tc[t].drawn)
              {
                if (tc[t].score > hiscore)
                {
                  highest = t;
                  hiscore = tc[t].score;
                  found = true;
                }
              }
            }
            if (!found)
              break;
          }

          // Output the best triangle
          std::uint16_t newind = buf->Vertices.size();

          snode *s = sind.find(v[tc[highest].ind[0]]);

          if (!s)
          {
            buf->Vertices.push_back(v[tc[highest].ind[0]]);
            buf->Indices.push_back(newind);
            sind.insert(v[tc[highest].ind[0]], newind);
            newind++;
          }
          else
          {
            buf->Indices.push_back(s->getValue());
          }

          s = sind.find(v[tc[highest].ind[1]]);

          if (!s)
          {
            buf->Vertices.push_back(v[tc[highest].ind[1]]);
            buf->Indices.push_back(newind);
            sind.insert(v[tc[highest].ind[1]], newind);
            newind++;
          }
          else
          {
            buf->Indices.push_back(s->getValue());
          }

          s = sind.find(v[tc[highest].ind[2]]);

          if (!s)
          {
            buf->Vertices.push_back(v[tc[highest].ind[2]]);
            buf->Indices.push_back(newind);
            sind.insert(v[tc[highest].ind[2]], newind);
          }
          else
          {
            buf->Indices.push_back(s->getValue());
          }

          vc[tc[highest].ind[0]].NumActiveTris--;
          vc[tc[highest].ind[1]].NumActiveTris--;
          vc[tc[highest].ind[2]].NumActiveTris--;

          tc[highest].drawn = true;

          for (std::uint16_t j = 0; j < 3; j++)
          {
            vcache *vert = &vc[tc[highest].ind[j]];
            for (std::uint16_t t = 0; t < vert->tris.size(); t++)
            {
              if (highest == vert->tris[t])
              {
                vert->tris.erase(t);
                break;
              }
            }
          }

          lru.add(tc[highest].ind[0]);
          lru.add(tc[highest].ind[1]);
          highest = lru.add(tc[highest].ind[2], true);
          drawcalls++;
        }

        buf->setBoundingBox(mb->getBoundingBox());
        newmesh->addMeshBuffer(buf);
        buf->drop();
      }
      break;
      case scene::E_VERTEX_TYPE::TWO_TCOORDS:
      {
        video::S3DVertex2TCoords *v = (video::S3DVertex2TCoords *) mb->getVertices();

        SMeshBufferLightMap *buf = new SMeshBufferLightMap();
        buf->Material = mb->getMaterial();

        buf->Vertices.reallocate(vcount);
        buf->Indices.reallocate(icount);

        core::map<const video::S3DVertex2TCoords, const std::uint16_t> sind; // search index for fast operation
        typedef core::map<const video::S3DVertex2TCoords, const std::uint16_t>::Node snode;

        // Main algorithm
        std::uint32_t highest = 0;
        std::uint32_t drawcalls = 0;
        for (;;)
        {
          if (tc[highest].drawn)
          {
            bool found = false;
            float hiscore = 0;
            for (std::uint32_t t = 0; t < tcount; t++)
            {
              if (!tc[t].drawn)
              {
                if (tc[t].score > hiscore)
                {
                  highest = t;
                  hiscore = tc[t].score;
                  found = true;
                }
              }
            }
            if (!found)
              break;
          }

          // Output the best triangle
          std::uint16_t newind = buf->Vertices.size();

          snode *s = sind.find(v[tc[highest].ind[0]]);

          if (!s)
          {
            buf->Vertices.push_back(v[tc[highest].ind[0]]);
            buf->Indices.push_back(newind);
            sind.insert(v[tc[highest].ind[0]], newind);
            newind++;
          }
          else
          {
            buf->Indices.push_back(s->getValue());
          }

          s = sind.find(v[tc[highest].ind[1]]);

          if (!s)
          {
            buf->Vertices.push_back(v[tc[highest].ind[1]]);
            buf->Indices.push_back(newind);
            sind.insert(v[tc[highest].ind[1]], newind);
            newind++;
          }
          else
          {
            buf->Indices.push_back(s->getValue());
          }

          s = sind.find(v[tc[highest].ind[2]]);

          if (!s)
          {
            buf->Vertices.push_back(v[tc[highest].ind[2]]);
            buf->Indices.push_back(newind);
            sind.insert(v[tc[highest].ind[2]], newind);
          }
          else
          {
            buf->Indices.push_back(s->getValue());
          }

          vc[tc[highest].ind[0]].NumActiveTris--;
          vc[tc[highest].ind[1]].NumActiveTris--;
          vc[tc[highest].ind[2]].NumActiveTris--;

          tc[highest].drawn = true;

          for (std::uint16_t j = 0; j < 3; j++)
          {
            vcache *vert = &vc[tc[highest].ind[j]];
            for (std::uint16_t t = 0; t < vert->tris.size(); t++)
            {
              if (highest == vert->tris[t])
              {
                vert->tris.erase(t);
                break;
              }
            }
          }

          lru.add(tc[highest].ind[0]);
          lru.add(tc[highest].ind[1]);
          highest = lru.add(tc[highest].ind[2]);
          drawcalls++;
        }

        buf->setBoundingBox(mb->getBoundingBox());
        newmesh->addMeshBuffer(buf);
        buf->drop();

      }
      break;
      case scene::E_VERTEX_TYPE::TANGENTS:
      {
        video::S3DVertexTangents *v = (video::S3DVertexTangents *) mb->getVertices();

        SMeshBufferTangents *buf = new SMeshBufferTangents();
        buf->Material = mb->getMaterial();

        buf->Vertices.reallocate(vcount);
        buf->Indices.reallocate(icount);

        core::map<const video::S3DVertexTangents, const std::uint16_t> sind; // search index for fast operation
        typedef core::map<const video::S3DVertexTangents, const std::uint16_t>::Node snode;

        // Main algorithm
        std::uint32_t highest = 0;
        std::uint32_t drawcalls = 0;
        for (;;)
        {
          if (tc[highest].drawn)
          {
            bool found = false;
            float hiscore = 0;
            for (std::uint32_t t = 0; t < tcount; t++)
            {
              if (!tc[t].drawn)
              {
                if (tc[t].score > hiscore)
                {
                  highest = t;
                  hiscore = tc[t].score;
                  found = true;
                }
              }
            }
            if (!found)
              break;
          }

          // Output the best triangle
          std::uint16_t newind = buf->Vertices.size();

          snode *s = sind.find(v[tc[highest].ind[0]]);

          if (!s)
          {
            buf->Vertices.push_back(v[tc[highest].ind[0]]);
            buf->Indices.push_back(newind);
            sind.insert(v[tc[highest].ind[0]], newind);
            newind++;
          }
          else
          {
            buf->Indices.push_back(s->getValue());
          }

          s = sind.find(v[tc[highest].ind[1]]);

          if (!s)
          {
            buf->Vertices.push_back(v[tc[highest].ind[1]]);
            buf->Indices.push_back(newind);
            sind.insert(v[tc[highest].ind[1]], newind);
            newind++;
          }
          else
          {
            buf->Indices.push_back(s->getValue());
          }

          s = sind.find(v[tc[highest].ind[2]]);

          if (!s)
          {
            buf->Vertices.push_back(v[tc[highest].ind[2]]);
            buf->Indices.push_back(newind);
            sind.insert(v[tc[highest].ind[2]], newind);
          }
          else
          {
            buf->Indices.push_back(s->getValue());
          }

          vc[tc[highest].ind[0]].NumActiveTris--;
          vc[tc[highest].ind[1]].NumActiveTris--;
          vc[tc[highest].ind[2]].NumActiveTris--;

          tc[highest].drawn = true;

          for (std::uint16_t j = 0; j < 3; j++)
          {
            vcache *vert = &vc[tc[highest].ind[j]];
            for (std::uint16_t t = 0; t < vert->tris.size(); t++)
            {
              if (highest == vert->tris[t])
              {
                vert->tris.erase(t);
                break;
              }
            }
          }

          lru.add(tc[highest].ind[0]);
          lru.add(tc[highest].ind[1]);
          highest = lru.add(tc[highest].ind[2]);
          drawcalls++;
        }

        buf->setBoundingBox(mb->getBoundingBox());
        newmesh->addMeshBuffer(buf);
        buf->drop();
      }
      break;
    }

    delete [] vc;
    delete [] tc;

  } // for each meshbuffer

  return newmesh;
}

} // namespace scene
} // namespace saga

