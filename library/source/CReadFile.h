// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_READ_FILE_H_INCLUDED__
#define __C_READ_FILE_H_INCLUDED__

#include <stdio.h>
#include "IReadFile.h"


namespace saga
{

namespace io
{

  /*!
    Class for reading a real file from disk.
  */
  class CReadFile : public IReadFile
  {
  public:

    CReadFile(const std::string& fileName);

    virtual ~CReadFile();

    //! returns how much was read
    virtual size_t read(void* buffer, size_t sizeToRead) override;

    //! changes position in file, returns true if successful
    virtual bool seek(long finalPos, bool relativeMovement = false) override;

    //! returns size of file
    virtual long getSize() const override;

    //! returns if file is open
    bool isOpen() const
    {
      return File != 0;
    }

    //! returns where in the file we are.
    virtual long getPos() const override;

    //! returns name of file
    virtual const std::string& getFileName() const override;

    //! create read file on disk.
    static IReadFile* createReadFile(const std::string& fileName);

  private:

    //! opens the file
    void openFile();

    FILE* File;
    long FileSize;
    std::string Filename;
  };

} // namespace io
} // namespace saga

#endif

