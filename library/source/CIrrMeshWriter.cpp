// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h



#ifdef _IRR_COMPILE_WITH_IRR_WRITER_

#include "CIrrMeshWriter.h"
#include "os.h"
#include "IWriteFile.h"
#include "IXMLWriter.h"
#include "IMesh.h"


namespace saga
{
namespace scene
{


CIrrMeshWriter::CIrrMeshWriter(video::IVideoDriver* driver,
        io::IFileSystem* fs)
  : FileSystem(fs), VideoDriver(driver), Writer(0)
{
  #ifdef _DEBUG
  setDebugName("CIrrMeshWriter");
  #endif

  if (VideoDriver)
    VideoDriver->grab();

  if (FileSystem)
    FileSystem->grab();
}


CIrrMeshWriter::~CIrrMeshWriter()
{
  if (VideoDriver)
    VideoDriver->drop();

  if (FileSystem)
    FileSystem->drop();
}


//! Returns the type of the mesh writer
EMESH_WRITER_TYPE CIrrMeshWriter::getType() const
{
  return EMWT_IRR_MESH;
}


//! writes a mesh
bool CIrrMeshWriter::writeMesh(io::IWriteFile* file, scene::IMesh* mesh, std::int32_t flags)
{
  if (!file)
    return false;

  Writer = FileSystem->createXMLWriter(file);

  if (!Writer)
  {
    os::Printer::log("Could not write file", file->getFileName());
    return false;
  }

  os::Printer::log("Writing mesh", file->getFileName());

  // write IRR MESH header

  Writer->writeXMLHeader();

  Writer->writeElement(L"mesh", false,
    L"xmlns", L"http://irrlicht.sourceforge.net/IRRMESH_09_2007",
    L"version", L"1.0");
  Writer->writeLineBreak();

  // add some informational comment. Add a space after and before the comment
  // tags so that some braindead xml parsers (AS anyone?) are able to parse this too.

  std::wstring infoComment = L" This file contains a static mesh in the Irrlicht Engine format with ";
  infoComment += std::wstring(mesh->getMeshBufferCount());
  infoComment += L" materials.";

  Writer->writeComment(infoComment.c_str());
  Writer->writeLineBreak();

  // write mesh bounding box

  writeBoundingBox(mesh->getBoundingBox());
  Writer->writeLineBreak();

  // write mesh buffers

  for (int i = 0; i < (int)mesh->getMeshBufferCount(); ++i)
  {
    scene::IMeshBuffer* buffer = mesh->getMeshBuffer(i);
    if (buffer)
    {
      writeMeshBuffer(buffer);
      Writer->writeLineBreak();
    }
  }

  Writer->writeClosingTag(L"mesh");

  Writer->drop();
  return true;
}


void CIrrMeshWriter::writeBoundingBox(const core::aabbox3df& box)
{
  Writer->writeElement(L"boundingBox", true,
    L"minEdge", getVectorAsStringLine(box.MinEdge).c_str(),
    L"maxEdge", getVectorAsStringLine(box.MaxEdge).c_str());
}


std::wstring CIrrMeshWriter::getVectorAsStringLine(const glm::vec3& v) const
{
  std::wstring str;

  str = std::wstring(v.X);
  str += L" ";
  str += std::wstring(v.Y);
  str += L" ";
  str += std::wstring(v.Z);

  return str;
}


std::wstring CIrrMeshWriter::getVectorAsStringLine(const glm::vec2& v) const
{
  std::wstring str;

  str = std::wstring(v.X);
  str += L" ";
  str += std::wstring(v.Y);

  return str;
}


void CIrrMeshWriter::writeMeshBuffer(const scene::IMeshBuffer* buffer)
{
  Writer->writeElement(L"buffer", false);
  Writer->writeLineBreak();

  // write bounding box

  writeBoundingBox(buffer->getBoundingBox());
  Writer->writeLineBreak();

  // write material

  writeMaterial(buffer->getMaterial());

  // write vertices

  const std::wstring vertexTypeStr = video::sBuiltInVertexTypeNames[buffer->getVertexType()];

  Writer->writeElement(L"vertices", false,
    L"type", vertexTypeStr.c_str(),
    L"vertexCount", std::wstring(buffer->getVertexCount()).c_str());

  Writer->writeLineBreak();

  std::uint32_t vertexCount = buffer->getVertexCount();

  switch(buffer->getVertexType())
  {
  case scene::E_VERTEX_TYPE::STANDARD:
    {
      video::S3DVertex* vtx = (video::S3DVertex*)buffer->getVertices();
      for (std::uint32_t j= 0; j<vertexCount; ++j)
      {
        std::wstring str = getVectorAsStringLine(vtx[j].Pos);
        str += L" ";
        str += getVectorAsStringLine(vtx[j].Normal);

        char tmp[12];
        sprintf(tmp, " %02x%02x%02x%02x ", vtx[j].Color.getAlpha(), vtx[j].Color.getRed(), vtx[j].Color.getGreen(), vtx[j].Color.getBlue());
        str += tmp;

        str += getVectorAsStringLine(vtx[j].TCoords);

        Writer->writeText(str.c_str());
        Writer->writeLineBreak();
      }
    }
    break;
  case scene::E_VERTEX_TYPE::TWO_TCOORDS:
    {
      video::S3DVertex2TCoords* vtx = (video::S3DVertex2TCoords*)buffer->getVertices();
      for (std::uint32_t j= 0; j<vertexCount; ++j)
      {
        std::wstring str = getVectorAsStringLine(vtx[j].Pos);
        str += L" ";
        str += getVectorAsStringLine(vtx[j].Normal);

        char tmp[12];
        sprintf(tmp, " %02x%02x%02x%02x ", vtx[j].Color.getAlpha(), vtx[j].Color.getRed(), vtx[j].Color.getGreen(), vtx[j].Color.getBlue());
        str += tmp;

        str += getVectorAsStringLine(vtx[j].TCoords);
        str += L" ";
        str += getVectorAsStringLine(vtx[j].TCoords2);

        Writer->writeText(str.c_str());
        Writer->writeLineBreak();
      }
    }
    break;
  case scene::E_VERTEX_TYPE::TANGENTS:
    {
      video::S3DVertexTangents* vtx = (video::S3DVertexTangents*)buffer->getVertices();
      for (std::uint32_t j= 0; j<vertexCount; ++j)
      {
        std::wstring str = getVectorAsStringLine(vtx[j].Pos);
        str += L" ";
        str += getVectorAsStringLine(vtx[j].Normal);

        char tmp[12];
        sprintf(tmp, " %02x%02x%02x%02x ", vtx[j].Color.getAlpha(), vtx[j].Color.getRed(), vtx[j].Color.getGreen(), vtx[j].Color.getBlue());
        str += tmp;

        str += getVectorAsStringLine(vtx[j].TCoords);
        str += L" ";
        str += getVectorAsStringLine(vtx[j].Tangent);
        str += L" ";
        str += getVectorAsStringLine(vtx[j].Binormal);

        Writer->writeText(str.c_str());
        Writer->writeLineBreak();
      }
    }
    break;
  }

  Writer->writeClosingTag(L"vertices");
  Writer->writeLineBreak();

  // write indices

  Writer->writeElement(L"indices", false,
    L"indexCount", std::wstring(buffer->getIndexCount()).c_str());

  Writer->writeLineBreak();

  int indexCount = (int)buffer->getIndexCount();

  scene::E_INDEX_TYPE iType = buffer->getIndexType();

  const std::uint16_t* idx16 = buffer->getIndices();
  const std::uint32_t* idx32 = (std::uint32_t*) buffer->getIndices();
  const int maxIndicesPerLine = 25;

  for (int i = 0; i < indexCount; ++i)
  {
    if(iType == scene::E_INDEX_TYPE::BITS_16)
    {
      std::wstring str((int)idx16[i]);
      Writer->writeText(str.c_str());
    }
    else
    {
      std::wstring str((int)idx32[i]);
      Writer->writeText(str.c_str());
    }

    if (i % maxIndicesPerLine == maxIndicesPerLine-1)
      Writer->writeLineBreak();
    else
      Writer->writeText(L" ");
  }

  if ((indexCount-1) % maxIndicesPerLine != maxIndicesPerLine-1)
    Writer->writeLineBreak();

  Writer->writeClosingTag(L"indices");
  Writer->writeLineBreak();

  // close buffer tag

  Writer->writeClosingTag(L"buffer");
}


void CIrrMeshWriter::writeMaterial(const video::SMaterial& material)
{
  // simply use irrlichts built-in attribute serialization capabilities here:

  io::IAttributes* attributes =
    VideoDriver->createAttributesFromMaterial(material);

  if (attributes)
  {
    attributes->write(Writer, false, L"material");
    attributes->drop();
  }
}


} // namespace
} // namespace

#endif

