// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h
// Code for this scene node has been contributed by Anders la Cour-Harbo (alc)

#ifndef __C_SKY_DOME_SCENE_NODE_H_INCLUDED__
#define __C_SKY_DOME_SCENE_NODE_H_INCLUDED__

#include "ISceneNode.h"
#include "SMeshBuffer.h"

namespace saga
{
namespace scene
{

class CSkyDomeSceneNode : public ISceneNode
{
  public:
    CSkyDomeSceneNode(video::ITexture* texture, std::uint32_t horiRes, std::uint32_t vertRes,
      float texturePercentage, float spherePercentage, float radius,
      ISceneNode* parent, ISceneManager* smgr, std::int32_t id);
    virtual ~CSkyDomeSceneNode();
    virtual void OnRegisterSceneNode(video::RenderPassHandle pass) override;
    virtual void render() override;
    virtual const core::aabbox3d<float>& getBoundingBox() const override;
    // virtual video::SMaterial& getMaterial(std::uint32_t i) override;
    virtual std::uint32_t getMaterialCount() const override;
    virtual E_SCENE_NODE_TYPE getType() const override { return E_SCENE_NODE_TYPE::SKY_DOME; }

    virtual void serializeAttributes(io::IAttributes* out, io::SAttributeReadWriteOptions* options) const override;
    virtual void deserializeAttributes(io::IAttributes* in, io::SAttributeReadWriteOptions* options) override;
    virtual ISceneNode* clone(ISceneNode* newParent= 0, ISceneManager* newManager= 0) override;

  private:

    void generateMesh();

    SMeshBuffer* Buffer;

    std::uint32_t HorizontalResolution, VerticalResolution;
    float TexturePercentage, SpherePercentage, Radius;
};


}
}

#endif

