// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_EMPTY_SCENE_NODE_H_INCLUDED__
#define __C_EMPTY_SCENE_NODE_H_INCLUDED__

#include "ISceneNode.h"

namespace saga
{
namespace scene
{

  class CEmptySceneNode : public ISceneNode
  {
  public:

    //! constructor
    CEmptySceneNode(ISceneNode* parent, ISceneManager* mgr, std::int32_t id);

    //! returns the axis aligned bounding box of this node
    virtual const core::aabbox3d<float>& getBoundingBox() const override;

    //! This method is called just before the rendering process of the whole scene.
    virtual void OnRegisterSceneNode(video::RenderPassHandle pass) override;

    //! does nothing.
    virtual void render() override;

    //! Returns type of the scene node
    virtual E_SCENE_NODE_TYPE getType() const override { return E_SCENE_NODE_TYPE::EMPTY; }

    //! Creates a clone of this scene node and its children.
    virtual ISceneNode* clone(ISceneNode* newParent= 0, ISceneManager* newManager= 0) override;

  private:

    core::aabbox3d<float> Box;
  };

} // namespace scene
} // namespace saga

#endif

