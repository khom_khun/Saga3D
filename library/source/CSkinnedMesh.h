// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

//New skinned mesh

#ifndef __C_SKINNED_MESH_H_INCLUDED__
#define __C_SKINNED_MESH_H_INCLUDED__

#include "ISkinnedMesh.h"
#include "SMeshBuffer.h"
#include "S3DVertex.h"

namespace saga
{
namespace scene
{

  class IAnimatedMeshSceneNode;
  class IBoneSceneNode;

  class CSkinnedMesh: public ISkinnedMesh
  {
  public:

    //! constructor
    CSkinnedMesh();

    //! destructor
    virtual ~CSkinnedMesh();

    //! returns the amount of frames. If the amount is 1, it is a static (=non animated) mesh.
    virtual std::uint32_t getFrameCount() const override;

    //! Gets the default animation speed of the animated mesh.
    /** \return Amount of frames per second. If the amount is 0, it is a static, non animated mesh. */
    virtual float getAnimationSpeed() const override;

    //! Gets the frame count of the animated mesh.
    /** \param fps Frames per second to play the animation with. If the amount is 0, it is not animated.
    The actual speed is set in the scene node the mesh is instantiated in.*/
    virtual void setAnimationSpeed(float fps) override;

    //! returns the animated mesh based on a detail level (which is ignored)
    virtual IMesh* getMesh(std::int32_t frame, std::int32_t detailLevel=255, std::int32_t startFrameLoop=-1, std::int32_t endFrameLoop=-1) override;

    //! Animates this mesh's joints based on frame input
    //! blend: {0-old position, 1-New position}
    virtual void animateMesh(float frame, float blend) override;

    //! Preforms a software skin on this mesh based of joint positions
    virtual void skinMesh() override;

    //! returns amount of mesh buffers.
    virtual std::uint32_t getMeshBufferCount() const override;

    //! returns pointer to a mesh buffer
    virtual IMeshBuffer* getMeshBuffer(std::uint32_t nr) const override;

    //! Returns pointer to a mesh buffer which fits a material
    /** \param material: material to search for
    \return Returns the pointer to the mesh buffer or
    NULL if there is no such mesh buffer. */
    virtual IMeshBuffer* getMeshBuffer(const video::SMaterial &material) const override;

    //! returns an axis aligned bounding box
    virtual const core::aabbox3d<float>& getBoundingBox() const override;

    //! set user axis aligned bounding box
    virtual void setBoundingBox(const core::aabbox3df& box) override;

    //! sets a flag of all contained materials to a new value
    virtual void setMaterialFlag(video::E_MATERIAL_FLAG flag, bool newvalue) override;

    //! set the hardware mapping hint, for driver
    virtual void setHardwareMappingHint(E_HARDWARE_MAPPING newMappingHint, E_BUFFER_TYPE buffer=E_BUFFER_TYPE::VERTEX_AND_INDEX) override;

    //! flags the meshbuffer as changed, reloads hardware buffers
    virtual void setDirty(E_BUFFER_TYPE buffer=E_BUFFER_TYPE::VERTEX_AND_INDEX) override;

    //! Gets joint count.
    virtual std::uint32_t getJointCount() const override;

    //! Gets the name of a joint.
    virtual const char* getJointName(std::uint32_t number) const override;

    //! Gets a joint number from its name
    virtual std::int32_t getJointNumber(const char* name) const override;

    //! uses animation from another mesh
    virtual bool useAnimationFrom(const ISkinnedMesh *mesh) override;

    //! Update Normals when Animating
    //! False= Don't (default)
    //! True = Update normals, slower
    virtual void updateNormalsWhenAnimating(bool on) override;

    //! Sets Interpolation Mode
    virtual void setInterpolationMode(E_INTERPOLATION_MODE mode) override;

    //! Convertes the mesh to contain tangent information
    virtual void convertMeshToTangents() override;

    //! Does the mesh have no animation
    virtual bool isStatic() override;

    //! (This feature is not implemented in irrlicht yet)
    virtual bool setHardwareSkinning(bool on) override;

    //Interface for the mesh loaders (finalize should lock these functions, and they should have some prefix like loader_
    //these functions will use the needed arrays, set values, etc to help the loaders

    //! exposed for loaders to add mesh buffers
    virtual std::vector<SSkinMeshBuffer*> &getMeshBuffers() override;

    //! alternative method for adding joints
    virtual std::vector<SJoint*> &getAllJoints() override;

    //! alternative method for adding joints
    virtual const std::vector<SJoint*> &getAllJoints() const override;

    //! loaders should call this after populating the mesh
    virtual void finalize() override;

    //! Adds a new meshbuffer to the mesh, access it as last one
    virtual SSkinMeshBuffer *addMeshBuffer() override;

    //! Adds a new joint to the mesh, access it as last one
    virtual SJoint *addJoint(SJoint *parent= 0) override;

    //! Adds a new position key to the mesh, access it as last one
    virtual SPositionKey *addPositionKey(SJoint *joint) override;
    //! Adds a new rotation key to the mesh, access it as last one
    virtual SRotationKey *addRotationKey(SJoint *joint) override;
    //! Adds a new scale key to the mesh, access it as last one
    virtual SScaleKey *addScaleKey(SJoint *joint) override;

    //! Adds a new weight to the mesh, access it as last one
    virtual SWeight *addWeight(SJoint *joint) override;

    virtual void updateBoundingBox(void);

    //! Recovers the joints from the mesh
    void recoverJointsFromMesh(std::vector<IBoneSceneNode*> &jointChildSceneNodes);

    //! Tranfers the joint data to the mesh
    void transferJointsToMesh(const std::vector<IBoneSceneNode*> &jointChildSceneNodes);

    //! Tranfers the joint hints to the mesh
    void transferOnlyJointsHintsToMesh(const std::vector<IBoneSceneNode*> &jointChildSceneNodes);

    //! Creates an array of joints from this mesh as children of node
    void addJoints(std::vector<IBoneSceneNode*> &jointChildSceneNodes,
        IAnimatedMeshSceneNode* node,
        ISceneManager* smgr);

private:
    void checkForAnimation();

    void normalizeWeights();

    void buildAllLocalAnimatedMatrices();

    void buildAllGlobalAnimatedMatrices(SJoint *Joint= 0, SJoint *ParentJoint= 0);

    void getFrameData(float frame, SJoint *Node,
        glm::vec3 &position, std::int32_t &positionHint,
        glm::vec3 &scale, std::int32_t &scaleHint,
        core::quaternion &rotation, std::int32_t &rotationHint);

    void calculateGlobalMatrices(SJoint *Joint,SJoint *ParentJoint);

    void skinJoint(SJoint *Joint, SJoint *ParentJoint);

    void calculateTangents(glm::vec3& normal,
      glm::vec3& tangent, glm::vec3& binormal,
      glm::vec3& vt1, glm::vec3& vt2, glm::vec3& vt3,
      glm::vec2& tc1, glm::vec2& tc2, glm::vec2& tc3);

    std::vector<SSkinMeshBuffer*> *SkinningBuffers; //Meshbuffer to skin, default is to skin localBuffers

    std::vector<SSkinMeshBuffer*> LocalBuffers;

    std::vector<SJoint*> AllJoints;
    std::vector<SJoint*> RootJoints;

    std::vector< std::vector<bool> > Vertices_Moved;

    core::aabbox3d<float> BoundingBox;

    float EndFrame;
    float FramesPerSecond;

    float LastAnimatedFrame;
    bool SkinnedLastFrame;

    E_INTERPOLATION_MODE InterpolationMode:8;

    bool HasAnimation;
    bool PreparedForSkinning;
    bool AnimateNormals;
    bool HardwareSkinning;
  };

} // namespace scene
} // namespace saga

#endif

