// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_PARTICLE_ROTATION_AFFECTOR_H_INCLUDED__
#define __C_PARTICLE_ROTATION_AFFECTOR_H_INCLUDED__


#ifdef _IRR_COMPILE_WITH_PARTICLES_

#include "IParticleRotationAffector.h"

namespace saga
{
namespace scene
{

//! Particle Affector for rotating particles about a point
class CParticleRotationAffector : public IParticleRotationAffector
{
public:

  CParticleRotationAffector(const glm::vec3& speed = glm::vec3(5.0f, 5.0f, 5.0f),
    const glm::vec3& point = glm::vec3());

  //! Affects a particle.
  virtual void affect(std::uint32_t now, SParticle* particlearray, std::uint32_t count) override;

  //! Set the point that particles will attract to
  virtual void setPivotPoint(const glm::vec3& point) override { PivotPoint = point; }

  //! Set the speed in degrees per second
  virtual void setSpeed(const glm::vec3& speed) override { Speed = speed; }

  //! Get the point that particles are attracted to
  virtual const glm::vec3& getPivotPoint() const override { return PivotPoint; }

  //! Get the speed in degrees per second
  virtual const glm::vec3& getSpeed() const override { return Speed; }

  //! Writes attributes of the object.
  virtual void serializeAttributes(io::IAttributes* out, io::SAttributeReadWriteOptions* options) const override;

  //! Reads attributes of the object.
  virtual void deserializeAttributes(io::IAttributes* in, io::SAttributeReadWriteOptions* options) override;

private:

  glm::vec3 PivotPoint;
  glm::vec3 Speed;
  std::uint32_t LastTime;
};

} // namespace scene
} // namespace saga

#endif // _IRR_COMPILE_WITH_PARTICLES_

#endif // __C_PARTICLE_ROTATION_AFFECTOR_H_INCLUDED__
