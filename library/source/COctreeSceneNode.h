// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_OCTREE_SCENE_NODE_H_INCLUDED__
#define __C_OCTREE_SCENE_NODE_H_INCLUDED__

#include "IOctreeSceneNode.h"
#include "Octree.h"

namespace saga
{
namespace scene
{
  class COctreeSceneNode;

  //! implementation of the IOctreeSceneNode
  class COctreeSceneNode : public IOctreeSceneNode
  {
  public:

    //! constructor
    COctreeSceneNode(ISceneNode* parent, ISceneManager* mgr, std::int32_t id,
      std::int32_t minimalPolysPerNode=512);

    //! destructor
    virtual ~COctreeSceneNode();

    virtual void OnRegisterSceneNode(video::RenderPassHandle pass) override;

    //! renders the node.
    virtual void render() override;

    //! returns the axis aligned bounding box of this node
    virtual const core::aabbox3d<float>& getBoundingBox() const override;

    //! creates the tree
    bool createTree(IMesh* mesh);

    //! returns the material based on the zero based index i. To get the amount
    //! of materials used by this scene node, use getMaterialCount().
    //! This function is needed for inserting the node into the scene hierarchy on a
    //! optimal position for minimizing renderstate changes, but can also be used
    //! to directly modify the material of a scene node.
    // virtual video::SMaterial& getMaterial(std::uint32_t i) override;

    //! returns amount of materials used by this scene node.
    virtual std::uint32_t getMaterialCount() const override;

    //! Writes attributes of the scene node.
    virtual void serializeAttributes(io::IAttributes* out, io::SAttributeReadWriteOptions* options= 0) const override;

    //! Reads attributes of the scene node.
    virtual void deserializeAttributes(io::IAttributes* in, io::SAttributeReadWriteOptions* options= 0) override;

    //! Returns type of the scene node
    virtual E_SCENE_NODE_TYPE getType() const override { return E_SCENE_NODE_TYPE::OCTREE; }

    //! Sets a new mesh to display
    virtual void setMesh(IMesh* mesh) override;

    //! Get the currently defined mesh for display.
    virtual IMesh* getMesh(void) override;

    //! Sets if the scene node should not copy the materials of the mesh but use them in a read only style.
    virtual void setReadOnlyMaterials(bool readonly) override;

    //! Check if the scene node should not copy the materials of the mesh but use them in a read only style
    virtual bool isReadOnlyMaterials() const override;

    //! Creates shadow volume scene node as child of this node
    //! and returns a pointer to it.
    virtual IShadowVolumeSceneNode* addShadowVolumeSceneNode(const IMesh* shadowMesh,
      std::int32_t id, bool zfailmethod=true, float infinity=10000.0f) override;

    //! Removes a child from this scene node.
    //! Implemented here, to be able to remove the shadow properly, if there is one,
    //! or to remove attached children.
    virtual bool removeChild(ISceneNode* child) override;

    //! Set if/how vertex buffer object are used for the meshbuffers
    /** NOTE: When there is already a mesh in the node this will rebuild
    the octree. */
    virtual void setUseVBO(EOCTREENODE_VBO useVBO) override;

    //! Get if/how vertex buffer object are used for the meshbuffers
    virtual EOCTREENODE_VBO getUseVBO() const override;

    //! Set the kind of tests polygons do for visibility against the camera
    virtual void setPolygonChecks(EOCTREE_POLYGON_CHECKS checks) override;

    //! Get the kind of tests polygons do for visibility against the camera
    virtual EOCTREE_POLYGON_CHECKS getPolygonChecks() const override;

  private:

    void deleteTree();

    core::aabbox3d<float> Box;

    Octree<video::S3DVertex>* StdOctree;
    std::vector< Octree<video::S3DVertex>::SMeshChunk > StdMeshes;

    Octree<video::S3DVertex2TCoords>* LightMapOctree;
    std::vector< Octree<video::S3DVertex2TCoords>::SMeshChunk > LightMapMeshes;

    Octree<video::S3DVertexTangents>* TangentsOctree;
    std::vector< Octree<video::S3DVertexTangents>::SMeshChunk > TangentsMeshes;

    scene::E_VERTEX_TYPE VertexType;
    std::vector< video::SMaterial > Materials;

    std::string MeshName;
    std::int32_t MinimalPolysPerNode;
    std::int32_t PassCount;

    IMesh * Mesh;
    IShadowVolumeSceneNode* Shadow;

    EOCTREENODE_VBO UseVBOs;
    EOCTREE_POLYGON_CHECKS PolygonChecks;
  };

} // namespace scene
} // namespace saga

#endif

