// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_MESH_SCENE_NODE_H_INCLUDED__
#define __C_MESH_SCENE_NODE_H_INCLUDED__

#include "IMeshSceneNode.h"
#include "IMesh.h"

namespace saga
{
namespace scene
{

  class CMeshSceneNode : public IMeshSceneNode
  {
  public:

    //! constructor
    CMeshSceneNode(const std::shared_ptr<IMesh>& mesh, const std::shared_ptr<ISceneNode>& parent,
      const std::shared_ptr<ISceneManager>& mgr,
      const glm::vec3& position = glm::vec3(0,0,0),
      const glm::vec3& rotation = glm::vec3(0,0,0),
      const glm::vec3& scale = glm::vec3(1.0f, 1.0f, 1.0f));

    //! destructor
    virtual ~CMeshSceneNode();

    virtual void OnRegisterSceneNode(video::RenderPassHandle pass) override;

    //! returns the axis aligned bounding box of this node
    virtual const core::aabbox3d<float>& getBoundingBox() const override;

    //! returns the material based on the zero based index i. To get the amount
    //! of materials used by this scene node, use getMaterialCount().
    //! This function is needed for inserting the node into the scene hirachy on a
    //! optimal position for minimizing renderstate changes, but can also be used
    //! to directly modify the material of a scene node.
    // virtual video::SMaterial& getMaterial(std::uint32_t i) override;

    //! Returns type of the scene node
    virtual E_SCENE_NODE_TYPE getType() const override { return E_SCENE_NODE_TYPE::MESH; }

    //! Sets a new mesh
    virtual void setMesh(const std::shared_ptr<IMesh>& mesh) override { Mesh = mesh; }

    //! Returns the current mesh
    virtual const std::shared_ptr<IMesh>& getMesh() const override { return Mesh; }

    //! Creates a clone of this scene node and its children.
    // virtual ISceneNode* clone(ISceneNode* newParent= 0, ISceneManager* newManager= 0) override;

    //! Removes a child from this scene node.
    //! Implemented here, to be able to remove the shadow properly, if there is one,
    //! or to remove attached childs.
    // virtual bool removeChild(ISceneNode* child) override;

  protected:
    std::shared_ptr<IMesh> Mesh = nullptr;
    core::aabbox3d<float> Box;
  };

} // namespace scene
} // namespace saga

#endif

