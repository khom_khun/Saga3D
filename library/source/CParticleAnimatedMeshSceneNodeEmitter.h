// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_PARTICLE_ANIMATED_MESH_SCENE_NODE_EMITTER_H_INCLUDED__
#define __C_PARTICLE_ANIMATED_MESH_SCENE_NODE_EMITTER_H_INCLUDED__

#include "IParticleAnimatedMeshSceneNodeEmitter.h"


namespace saga
{
namespace scene
{

//! An animated mesh emitter
class CParticleAnimatedMeshSceneNodeEmitter : public IParticleAnimatedMeshSceneNodeEmitter
{
public:

  //! constructor
  CParticleAnimatedMeshSceneNodeEmitter(
    IAnimatedMeshSceneNode* node,
    bool useNormalDirection = true,
    const glm::vec3& direction = glm::vec3(0.0f,0.0f,-1.0f),
    float normalDirectionModifier = 100.0f,
    std::int32_t mbNumber = -1,
    bool everyMeshVertex = false,
    std::uint32_t minParticlesPerSecond = 20,
    std::uint32_t maxParticlesPerSecond = 40,
    const video::SColor& minStartColor = video::SColor(255,0,0,0),
    const video::SColor& maxStartColor = video::SColor(255,255,255,255),
    std::uint32_t lifeTimeMin = 2000,
    std::uint32_t lifeTimeMax = 4000,
    std::int32_t maxAngleDegrees = 0,
    const glm::vec2& minStartSize = glm::vec2(5.0f,5.0f),
    const glm::vec2& maxStartSize = glm::vec2(5.0f,5.0f)
 );

  //! Prepares an array with new particles to emitt into the system
  //! and returns how much new particles there are.
  virtual std::int32_t emitt(std::uint32_t now, std::uint32_t timeSinceLastCall, SParticle*& outArray) override;

  //! Set Mesh to emit particles from
  virtual void setAnimatedMeshSceneNode(IAnimatedMeshSceneNode* node) override;

  //! Set whether to use vertex normal for direction, or direction specified
  virtual void setUseNormalDirection(bool useNormalDirection) override { UseNormalDirection = useNormalDirection; }

  //! Set direction the emitter emits particles
  virtual void setDirection(const glm::vec3& newDirection) override { Direction = newDirection; }

  //! Set the amount that the normal is divided by for getting a particles direction
  virtual void setNormalDirectionModifier(float normalDirectionModifier) override { NormalDirectionModifier = normalDirectionModifier; }

  //! Sets whether to emit min<->max particles for every vertex per second, or to pick
  //! min<->max vertices every second
  virtual void setEveryMeshVertex(bool everyMeshVertex) override { EveryMeshVertex = everyMeshVertex; }

  //! Set minimum number of particles the emitter emits per second
  virtual void setMinParticlesPerSecond(std::uint32_t minPPS) override { MinParticlesPerSecond = minPPS; }

  //! Set maximum number of particles the emitter emits per second
  virtual void setMaxParticlesPerSecond(std::uint32_t maxPPS) override { MaxParticlesPerSecond = maxPPS; }

  //! Set minimum starting color for particles
  virtual void setMinStartColor(const video::SColor& color) override { MinStartColor = color; }

  //! Set maximum starting color for particles
  virtual void setMaxStartColor(const video::SColor& color) override { MaxStartColor = color; }

  //! Set the maximum starting size for particles
  virtual void setMaxStartSize(const glm::vec2& size) override { MaxStartSize = size; }

  //! Set the minimum starting size for particles
  virtual void setMinStartSize(const glm::vec2& size) override { MinStartSize = size; }

  //! Set the minimum particle life-time in milliseconds
  virtual void setMinLifeTime(std::uint32_t lifeTimeMin) override { MinLifeTime = lifeTimeMin; }

  //! Set the maximum particle life-time in milliseconds
  virtual void setMaxLifeTime(std::uint32_t lifeTimeMax) override { MaxLifeTime = lifeTimeMax; }

  //!  Maximal random derivation from the direction
  virtual void setMaxAngleDegrees(std::int32_t maxAngleDegrees) override { MaxAngleDegrees = maxAngleDegrees; }

  //! Get Mesh we're emitting particles from
  virtual const IAnimatedMeshSceneNode* getAnimatedMeshSceneNode() const override { return Node; }

  //! Get whether to use vertex normal for direciton, or direction specified
  virtual bool isUsingNormalDirection() const override { return UseNormalDirection; }

  //! Get direction the emitter emits particles
  virtual const glm::vec3& getDirection() const override { return Direction; }

  //! Get the amount that the normal is divided by for getting a particles direction
  virtual float getNormalDirectionModifier() const override { return NormalDirectionModifier; }

  //! Gets whether to emit min<->max particles for every vertex per second, or to pick
  //! min<->max vertices every second
  virtual bool getEveryMeshVertex() const override { return EveryMeshVertex; }

  //! Get the minimum number of particles the emitter emits per second
  virtual std::uint32_t getMinParticlesPerSecond() const override { return MinParticlesPerSecond; }

  //! Get the maximum number of particles the emitter emits per second
  virtual std::uint32_t getMaxParticlesPerSecond() const override { return MaxParticlesPerSecond; }

  //! Get the minimum starting color for particles
  virtual const video::SColor& getMinStartColor() const override { return MinStartColor; }

  //! Get the maximum starting color for particles
  virtual const video::SColor& getMaxStartColor() const override { return MaxStartColor; }

  //! Get the maximum starting size for particles
  virtual const glm::vec2& getMaxStartSize() const override { return MaxStartSize; }

  //! Get the minimum starting size for particles
  virtual const glm::vec2& getMinStartSize() const override { return MinStartSize; }

  //! Get the minimum particle life-time in milliseconds
  virtual std::uint32_t getMinLifeTime() const override { return MinLifeTime; }

  //! Get the maximum particle life-time in milliseconds
  virtual std::uint32_t getMaxLifeTime() const override { return MaxLifeTime; }

  //!  Maximal random derivation from the direction
  virtual std::int32_t getMaxAngleDegrees() const override { return MaxAngleDegrees; }

private:

  IAnimatedMeshSceneNode* Node;
  IAnimatedMesh*    AnimatedMesh;
  const IMesh*    BaseMesh;
  std::int32_t      TotalVertices;
  std::uint32_t      MBCount;
  std::int32_t      MBNumber;
  std::vector<std::int32_t>  VertexPerMeshBufferList;

  std::vector<SParticle> Particles;
  glm::vec3 Direction;
  float NormalDirectionModifier;
  std::uint32_t MinParticlesPerSecond, MaxParticlesPerSecond;
  video::SColor MinStartColor, MaxStartColor;
  std::uint32_t MinLifeTime, MaxLifeTime;
  glm::vec2 MaxStartSize, MinStartSize;

  std::uint32_t Time;
  std::int32_t MaxAngleDegrees;

  bool EveryMeshVertex;
  bool UseNormalDirection;
};

} // namespace scene
} // namespace saga


#endif // __C_PARTICLE_ANIMATED_MESH_SCENE_NODE_EMITTER_H_INCLUDED__

