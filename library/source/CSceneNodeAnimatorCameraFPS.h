// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_SCENE_NODE_ANIMATOR_CAMERA_FPS_H_INCLUDED__
#define __C_SCENE_NODE_ANIMATOR_CAMERA_FPS_H_INCLUDED__

#include "ISceneNodeAnimatorCameraFPS.h"
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

namespace saga
{
namespace scene
{

  //! Special scene node animator for FPS cameras
  class CSceneNodeAnimatorCameraFPS : public ISceneNodeAnimatorCameraFPS
  {
  public:

    //! Constructor
    CSceneNodeAnimatorCameraFPS(
      float moveSpeed = .5f, float rotateSpeed = 1.0f
    );

    //! Destructor
    virtual ~CSceneNodeAnimatorCameraFPS();

    //! Animates the scene node, currently only works on cameras
    virtual void animateNode(ISceneNode& node, std::uint32_t timeMs) override;

    //! Event receiver
    virtual void OnEvent(const SDL_Event& event) override;

    //! Returns the speed of movement in units per second
    virtual float getMoveSpeed() const override;

    //! Sets the speed of movement in units per second
    virtual void setMoveSpeed(float moveSpeed) override;

    //! Returns the rotation speed when moving mouse
    virtual float getRotateSpeed() const override;

    //! Set the rotation speed when moving mouse
    virtual void setRotateSpeed(float rotateSpeed) override;

    //! Sets whether the Y axis of the mouse should be inverted.
    /** If enabled then moving the mouse down will cause
    the camera to look up. It is disabled by default. */
    virtual void setInvertMouse(bool invert) override;

    //! Returns the type of this animator
    virtual E_SCENE_NODE_ANIMATOR_TYPE getType() const override
    {
      return E_SCENE_NODE_ANIMATOR_TYPE::CAMERA_FPS;
    }

  private:
    glm::vec3 Position = {};
    glm::vec3 Target = {};
    glm::vec3 Rotation = {};
    glm::ivec2 MousePosition = {};

    float MoveSpeed;
    float RotateSpeed;

    std::int32_t LastAnimationTime = 0;
  };

} // namespace scene
} // namespace saga

#endif // __C_SCENE_NODE_ANIMATOR_CAMERA_FPS_H_INCLUDED__
