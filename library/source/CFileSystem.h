// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_FILE_SYSTEM_H_INCLUDED__
#define __C_FILE_SYSTEM_H_INCLUDED__

#include "IFileSystem.h"


namespace saga
{
namespace io
{

  class CZipReader;
  class CPakReader;
  class CMountPointReader;

/*!
  FileSystem which uses normal files and one zipfile
*/
class CFileSystem : public IFileSystem
{
public:

  //! constructor
  CFileSystem();

  //! destructor
  virtual ~CFileSystem();

  //! opens a file for read access
  virtual IReadFile* createAndOpenFile(const std::string& filename) override;

  //! Creates an IReadFile interface for accessing memory like a file.
  virtual IReadFile* createMemoryReadFile(const void* memory, std::int32_t len, const std::string& fileName, bool deleteMemoryWhenDropped = false) override;

  //! Creates an IReadFile interface for accessing files inside files
  virtual IReadFile* createLimitReadFile(const std::string& fileName, IReadFile* alreadyOpenedFile, long pos, long areaSize) override;

  //! Creates an IWriteFile interface for accessing memory like a file.
  virtual IWriteFile* createMemoryWriteFile(void* memory, std::int32_t len, const std::string& fileName, bool deleteMemoryWhenDropped=false) override;

  //! Opens a file for write access.
  virtual IWriteFile* createAndWriteFile(const std::string& filename, bool append=false) override;

  //! Adds an archive to the file system.
  virtual bool addFileArchive(const std::string& filename,
      bool ignoreCase = true, bool ignorePaths = true,
      E_FILE_ARCHIVE_TYPE archiveType = EFAT_UNKNOWN,
      const std::string& password="",
      IFileArchive** retArchive = 0) override;

  //! Adds an archive to the file system.
  virtual bool addFileArchive(IReadFile* file, bool ignoreCase=true,
      bool ignorePaths=true,
      E_FILE_ARCHIVE_TYPE archiveType=EFAT_UNKNOWN,
      const std::string& password="",
      IFileArchive** retArchive = 0) override;

  //! Adds an archive to the file system.
  virtual bool addFileArchive(IFileArchive* archive) override;

  //! move the hirarchy of the filesystem. moves sourceIndex relative up or down
  virtual bool moveFileArchive(std::uint32_t sourceIndex, std::int32_t relative) override;

  //! Adds an external archive loader to the engine.
  virtual void addArchiveLoader(IArchiveLoader* loader) override;

  //! Returns the total number of archive loaders added.
  virtual std::uint32_t getArchiveLoaderCount() const override;

  //! Gets the archive loader by index.
  virtual IArchiveLoader* getArchiveLoader(std::uint32_t index) const override;

  //! gets the file archive count
  virtual std::uint32_t getFileArchiveCount() const override;

  //! gets an archive
  virtual IFileArchive* getFileArchive(std::uint32_t index) override;

  //! removes an archive from the file system.
  virtual bool removeFileArchive(std::uint32_t index) override;

  //! removes an archive from the file system.
  virtual bool removeFileArchive(const std::string& filename) override;

  //! Removes an archive from the file system.
  virtual bool removeFileArchive(const IFileArchive* archive) override;

  //! Returns the string of the current working directory
  virtual const std::string& getWorkingDirectory() override;

  //! Changes the current Working Directory to the string given.
  //! The string is operating system dependent. Under Windows it will look
  //! like this: "drive:\directory\sudirectory\"
  virtual bool changeWorkingDirectoryTo(const std::string& newDirectory) override;

  //! Converts a relative path to an absolute (unique) path, resolving symbolic links
  virtual std::string getAbsolutePath(const std::string& filename) const override;

  //! Returns the directory a file is located in.
  /** \param filename: The file to get the directory from */
  virtual std::string getFileDir(const std::string& filename) const override;

  //! Returns the base part of a filename, i.e. the name without the directory
  //! part. If no directory is prefixed, the full name is returned.
  /** \param filename: The file to get the basename from */
  virtual std::string getFileBasename(const std::string& filename, bool keepExtension=true) const override;

  //! flatten a path and file name for example: "/you/me/../." becomes "/you"
  virtual std::string& flattenFilename(std::string& directory, const std::string& root = "/") const override;

  //! Get the relative filename, relative to the given directory
  virtual path getRelativeFilename(const std::string& filename, const std::string& directory) const override;

  virtual EFileSystemType setFileListSystem(EFileSystemType listType) override;

  //! Creates a list of files and directories in the current working directory
  //! and returns it.
  virtual IFileList* createFileList() override;

  //! Creates an empty filelist
  virtual IFileList* createEmptyFileList(const std::string& path, bool ignoreCase, bool ignorePaths) override;

  //! determines if a file exists and would be able to be opened.
  virtual bool existFile(const std::string& filename) const override;

  //! Creates a XML Reader from a file.
  virtual IXMLReader* createXMLReader(const std::string& filename) override;

  //! Creates a XML Reader from a file.
  virtual IXMLReader* createXMLReader(IReadFile* file) override;

  //! Creates a XML Reader from a file.
  virtual IXMLReaderUTF8* createXMLReaderUTF8(const std::string& filename) override;

  //! Creates a XML Reader from a file.
  virtual IXMLReaderUTF8* createXMLReaderUTF8(IReadFile* file) override;

  //! Creates a XML Writer from a file.
  virtual IXMLWriter* createXMLWriter(const std::string& filename) override;

  //! Creates a XML Writer from a file.
  virtual IXMLWriter* createXMLWriter(IWriteFile* file) override;

  //! Creates a new empty collection of attributes, usable for serialization and more.
  virtual IAttributes* createEmptyAttributes(video::IVideoDriver* driver) override;

private:

  // don't expose, needs refactoring
  bool changeArchivePassword(const std::string& filename,
      const std::string& password,
      IFileArchive** archive = 0);

  //! Currently used FileSystemType
  EFileSystemType FileSystemType;
  //! WorkingDirectory for Native and Virtual filesystems
  std::string WorkingDirectory [2];
  //! currently attached ArchiveLoaders
  std::vector<IArchiveLoader*> ArchiveLoader;
  //! currently attached Archives
  std::vector<IFileArchive*> FileArchives;
};


} // namespace saga
} // namespace io

#endif

