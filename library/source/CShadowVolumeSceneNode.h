// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_SHADOW_VOLUME_SCENE_NODE_H_INCLUDED__
#define __C_SHADOW_VOLUME_SCENE_NODE_H_INCLUDED__

#include "IShadowVolumeSceneNode.h"

namespace saga
{
namespace scene
{

  //! Scene node for rendering a shadow volume into a stencil buffer.
  class CShadowVolumeSceneNode : public IShadowVolumeSceneNode
  {
  public:

    //! constructor
    CShadowVolumeSceneNode(const IMesh* shadowMesh, ISceneNode* parent, ISceneManager* mgr,
      std::int32_t id, bool zfailmethod=true, float infinity=10000.0f);

    //! destructor
    virtual ~CShadowVolumeSceneNode();

    //! Sets the mesh from which the shadow volume should be generated.
    /** To optimize shadow rendering, use a simpler mesh for shadows.
    */
    virtual void setShadowMesh(const IMesh* mesh) override;

    //! Updates the shadow volumes for current light positions.
    /** Called each render cycle from Animated Mesh SceneNode render method. */
    virtual void updateShadowVolumes() override;

    //! pre render method
    virtual void OnRegisterSceneNode(video::RenderPassHandle pass) override;

    //! renders the node.
    virtual void render() override;

    //! returns the axis aligned bounding box of this node
    virtual const core::aabbox3d<float>& getBoundingBox() const override;

    //! Returns type of the scene node
    virtual E_SCENE_NODE_TYPE getType() const override { return E_SCENE_NODE_TYPE::SHADOW_VOLUME; }

  private:

    typedef std::vector<glm::vec3> SShadowVolume;

    void createShadowVolume(const glm::vec3& pos, bool isDirectional=false);
    std::uint32_t createEdgesAndCaps(const glm::vec3& light, SShadowVolume* svp, core::aabbox3d<float>* bb);

    //! Generates adjacency information based on mesh indices.
    void calculateAdjacency();

    core::aabbox3d<float> Box;

    // a shadow volume for every light
    std::vector<SShadowVolume> ShadowVolumes;

    // a back cap bounding box for every light
    std::vector<core::aabbox3d<float> > ShadowBBox;

    std::vector<glm::vec3> Vertices;
    std::vector<std::uint16_t> Indices;
    std::vector<std::uint16_t> Adjacency;
    std::vector<std::uint16_t> Edges;
    // tells if face is front facing
    std::vector<bool> FaceData;

    const scene::IMesh* ShadowMesh;

    std::uint32_t IndexCount;
    std::uint32_t VertexCount;
    std::uint32_t ShadowVolumesUsed;

    float Infinity;

    bool UseZFailMethod;
  };

} // namespace scene
} // namespace saga

#endif
