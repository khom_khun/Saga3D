// Copyright (C) 2002-2012 Nikolaus Gebhardt / Thomas Alten
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#include "CImage.h"

#include "CColorConverter.h"
#include "CBlit.h"
#include "os.h"

namespace saga
{
namespace video
{

//! Constructor from raw data
CImage::CImage(E_PIXEL_FORMAT format, const glm::uvec2& size, void* data,
  bool ownForeignMemory, bool deleteMemory) : IImage(format, size, deleteMemory)
{
  if (ownForeignMemory)
  {
    Data = (u8*)data;
  }
  else
  {
    const std::uint32_t dataSize = getDataSizeFromFormat(Format, Size.Width, Size.Height);

    Data = new u8[dataSize];
    memcpy(Data, data, dataSize);
    DeleteMemory = true;
  }
}


//! Constructor of empty image
CImage::CImage(E_PIXEL_FORMAT format, const glm::uvec2& size) : IImage(format, size, true)
{
  Data = new u8[getDataSizeFromFormat(Format, Size.Width, Size.Height)];
  DeleteMemory = true;
}


//! sets a pixel
void CImage::setPixel(std::uint32_t x, std::uint32_t y, const SColor &color, bool blend)
{
  if (IImage::isCompressedFormat(Format))
  {
    os::Printer::log("IImage::setPixel method doesn't work with compressed images.", ELL_WARNING);
    return;
  }

  if (x >= Size.Width || y >= Size.Height)
    return;

  switch(Format)
  {
    case E_PIXEL_FORMAT::A1R5G5B5:
    {
      std::uint16_t * dest = (std::uint16_t*) (Data + (y * Pitch) + (x << 1));
      *dest = video::A8R8G8B8toA1R5G5B5(color.color);
    } break;

    case E_PIXEL_FORMAT::R5G6B5:
    {
      std::uint16_t * dest = (std::uint16_t*) (Data + (y * Pitch) + (x << 1));
      *dest = video::A8R8G8B8toR5G6B5(color.color);
    } break;

    case E_PIXEL_FORMAT::R8G8B8:
    {
      u8* dest = Data + (y * Pitch) + (x * 3);
      dest[0] = (u8)color.getRed();
      dest[1] = (u8)color.getGreen();
      dest[2] = (u8)color.getBlue();
    } break;

    case E_PIXEL_FORMAT::R8G8B8A8:
    {
      std::uint32_t * dest = (std::uint32_t*) (Data + (y * Pitch) + (x << 2));
      *dest = blend ? PixelBlend32 (*dest, color.color) : color.color;
    } break;
#ifndef _DEBUG
    default:
      break;
#endif
  }
}


//! returns a pixel
SColor CImage::getPixel(std::uint32_t x, std::uint32_t y) const
{
  if (IImage::isCompressedFormat(Format))
  {
    os::Printer::log("IImage::getPixel method doesn't work with compressed images.", ELL_WARNING);
    return SColor(0);
  }

  if (x >= Size.Width || y >= Size.Height)
    return SColor(0);

  switch(Format)
  {
  case E_PIXEL_FORMAT::A1R5G5B5:
    return A1R5G5B5toA8R8G8B8(((std::uint16_t*)Data)[y*Size.Width + x]);
  case E_PIXEL_FORMAT::R5G6B5:
    return R5G6B5toA8R8G8B8(((std::uint16_t*)Data)[y*Size.Width + x]);
  case E_PIXEL_FORMAT::R8G8B8A8:
    return ((std::uint32_t*)Data)[y*Size.Width + x];
  case E_PIXEL_FORMAT::R8G8B8:
    {
      u8* p = Data+(y*3)*Size.Width + (x*3);
      return SColor(255,p[0],p[1],p[2]);
    }
#ifndef _DEBUG
  default:
    break;
#endif
  }

  return SColor(0);
}


//! copies this surface into another at given position
void CImage::copyTo(IImage* target, const glm::ivec2& pos)
{
  if (IImage::isCompressedFormat(Format))
  {
    os::Printer::log("IImage::copyTo method doesn't work with compressed images.", ELL_WARNING);
    return;
  }

  Blit(BLITTER_TEXTURE, target, 0, &pos, this, 0, 0);
}


//! copies this surface partially into another at given position
void CImage::copyTo(IImage* target, const glm::ivec2& pos, const core::rect<std::int32_t>& sourceRect, const core::rect<std::int32_t>* clipRect)
{
  if (IImage::isCompressedFormat(Format))
  {
    os::Printer::log("IImage::copyTo method doesn't work with compressed images.", ELL_WARNING);
    return;
  }

  Blit(BLITTER_TEXTURE, target, clipRect, &pos, this, &sourceRect, 0);
}


//! copies this surface into another, using the alpha mask, a cliprect and a color to add with
void CImage::copyToWithAlpha(IImage* target, const glm::ivec2& pos, const core::rect<std::int32_t>& sourceRect, const SColor &color, const core::rect<std::int32_t>* clipRect, bool combineAlpha)
{
  if (IImage::isCompressedFormat(Format))
  {
    os::Printer::log("IImage::copyToWithAlpha method doesn't work with compressed images.", ELL_WARNING);
    return;
  }

  if (combineAlpha)
  {
    Blit(BLITTER_TEXTURE_COMBINE_ALPHA, target, clipRect, &pos, this, &sourceRect, color.color);
  }
  else
  {
    // color blend only necessary on not full spectrum aka. color.color != 0xFFFFFFFF
    Blit(color.color == 0xFFFFFFFF ? BLITTER_TEXTURE_ALPHA_BLEND: BLITTER_TEXTURE_ALPHA_COLOR_BLEND,
        target, clipRect, &pos, this, &sourceRect, color.color);
  }
}


//! copies this surface into another, scaling it to the target image size
// note: this is very very slow.
void CImage::copyToScaling(void* target, std::uint32_t width, std::uint32_t height, E_PIXEL_FORMAT format, std::uint32_t pitch)
{
  if (IImage::isCompressedFormat(Format))
  {
    os::Printer::log("IImage::copyToScaling method doesn't work with compressed images.", ELL_WARNING);
    return;
  }

  if (!target || !width || !height)
    return;

  const std::uint32_t bpp=getBitsPerPixelFromFormat(format)/8;
  if (0==pitch)
    pitch = width*bpp;

  if (Format==format && Size.Width==width && Size.Height==height)
  {
    if (pitch==Pitch)
    {
      memcpy(target, Data, height*pitch);
      return;
    }
    else
    {
      u8* tgtpos = (u8*) target;
      u8* srcpos = Data;
      const std::uint32_t bwidth = width*bpp;
      const std::uint32_t rest = pitch-bwidth;
      for (std::uint32_t y= 0; y<height; ++y)
      {
        // copy scanline
        memcpy(tgtpos, srcpos, bwidth);
        // clear pitch
        memset(tgtpos+bwidth, 0, rest);
        tgtpos += pitch;
        srcpos += Pitch;
      }
      return;
    }
  }

  const float sourceXStep = (float)Size.Width / (float)width;
  const float sourceYStep = (float)Size.Height / (float)height;
  std::int32_t yval= 0, syval= 0;
  float sy = 0.0f;
  for (std::uint32_t y= 0; y<height; ++y)
  {
    float sx = 0.0f;
    for (std::uint32_t x= 0; x<width; ++x)
    {
      CColorConverter::convE_RENDER_TARGET_TYPE::viaFormat(Data+ syval + ((std::int32_t)sx)*BytesPerPixel, Format, 1, ((u8*)target)+ yval + (x*bpp), format);
      sx+=sourceXStep;
    }
    sy+=sourceYStep;
    syval=((std::int32_t)sy)*Pitch;
    yval+=pitch;
  }
}


//! copies this surface into another, scaling it to the target image size
// note: this is very very slow.
void CImage::copyToScaling(IImage* target)
{
  if (IImage::isCompressedFormat(Format))
  {
    os::Printer::log("IImage::copyToScaling method doesn't work with compressed images.", ELL_WARNING);
    return;
  }

  if (!target)
    return;

  const glm::uvec2& targetSize = target->getDimension();

  if (targetSize==Size)
  {
    copyTo(target);
    return;
  }

  copyToScaling(target->getData(), targetSize.Width, targetSize.Height, target->getColorFormat());
}


//! copies this surface into another, scaling it to fit it.
void CImage::copyToScalingBoxFilter(IImage* target, std::int32_t bias, bool blend)
{
  if (IImage::isCompressedFormat(Format))
  {
    os::Printer::log("IImage::copyToScalingBoxFilter method doesn't work with compressed images.", ELL_WARNING);
    return;
  }

  const glm::uvec2 destSize = target->getDimension();

  const float sourceXStep = (float) Size.Width / (float) destSize.Width;
  const float sourceYStep = (float) Size.Height / (float) destSize.Height;

  target->getData();

  std::int32_t fx = core::ceil32(sourceXStep);
  std::int32_t fy = core::ceil32(sourceYStep);
  float sx;
  float sy;

  sy = 0.f;
  for (std::uint32_t y = 0; y != destSize.Height; ++y)
  {
    sx = 0.f;
    for (std::uint32_t x = 0; x != destSize.Width; ++x)
    {
      target->setPixel(x, y,
        getPixelBox(core::floor32(sx), core::floor32(sy), fx, fy, bias), blend);
      sx += sourceXStep;
    }
    sy += sourceYStep;
  }
}


//! fills the surface with given color
void CImage::fill(const SColor &color)
{
  if (IImage::isCompressedFormat(Format))
  {
    os::Printer::log("IImage::fill method doesn't work with compressed images.", ELL_WARNING);
    return;
  }

  std::uint32_t c;

  switch (Format)
  {
    case E_PIXEL_FORMAT::A1R5G5B5:
      c = color.toA1R5G5B5();
      c |= c << 16;
      break;
    case E_PIXEL_FORMAT::R5G6B5:
      c = video::A8R8G8B8toR5G6B5(color.color);
      c |= c << 16;
      break;
    case E_PIXEL_FORMAT::R8G8B8A8:
      c = color.color;
      break;
    case E_PIXEL_FORMAT::R8G8B8:
    {
      u8 rgb[3];
      CColorConverter::convE_RENDER_TARGET_TYPE::A8R8G8B8toR8G8B8(&color, 1, rgb);
      const std::uint32_t size = getImageDataSizeInBytes();
      for (std::uint32_t i = 0; i < size; i+=3)
      {
        memcpy(Data+i, rgb, 3);
      }
      return;
    }
    break;
    default:
    // TODO: Handle other formats
      return;
  }
  memset32(Data, c, getImageDataSizeInBytes());
}


//! get a filtered pixel
inline SColor CImage::getPixelBox(std::int32_t x, std::int32_t y, std::int32_t fx, std::int32_t fy, std::int32_t bias) const
{
  if (IImage::isCompressedFormat(Format))
  {
    os::Printer::log("IImage::getPixelBox method doesn't work with compressed images.", ELL_WARNING);
    return SColor(0);
  }

  SColor c;
  std::int32_t a = 0, r = 0, g = 0, b = 0;

  for (std::int32_t dx = 0; dx != fx; ++dx)
  {
    for (std::int32_t dy = 0; dy != fy; ++dy)
    {
      c = getPixel( std::int32_t_min (x + dx, Size.Width - 1) ,
              std::int32_t_min (y + dy, Size.Height - 1)
           );

      a += c.getAlpha();
      r += c.getRed();
      g += c.getGreen();
      b += c.getBlue();
    }

  }

  std::int32_t sdiv = std::int32_t_log2_std::int32_t(fx * fy);

  a = std::int32_t_clamp((a >> sdiv) + bias, 0, 255);
  r = std::int32_t_clamp((r >> sdiv) + bias, 0, 255);
  g = std::int32_t_clamp((g >> sdiv) + bias, 0, 255);
  b = std::int32_t_clamp((b >> sdiv) + bias, 0, 255);

  c.set(a, r, g, b);
  return c;
}


} // namespace video
} // namespace saga
