// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#include "CSceneNodeAnimatorFollowSpline.h"

namespace saga
{
namespace scene
{


//! constructor
CSceneNodeAnimatorFollowSpline::CSceneNodeAnimatorFollowSpline(std::uint32_t time,
  const std::vector<glm::vec3>& points, float speed,
  float tightness, bool loop, bool pingpong)
: ISceneNodeAnimatorFinishing(0), Points(points), Speed(speed), Tightness(tightness)
, Loop(loop), PingPong(pingpong)
{
  #ifdef _DEBUG
  setDebugName("CSceneNodeAnimatorFollowSpline");
  #endif

  StartTime = time;
}


inline std::int32_t CSceneNodeAnimatorFollowSpline::clamp(std::int32_t idx, std::int32_t size)
{
  return (idx<0 ? size+idx : (idx>=size ? idx-size : idx));
}


//! animates a scene node
void CSceneNodeAnimatorFollowSpline::animateNode(ISceneNode* node, std::uint32_t timeMs)
{
  if(!node)
    return;

  const std::uint32_t pSize = Points.size();
  if (pSize== 0)
  {
    if (!Loop)
      HasFinished = true;
    return;
  }
  if (pSize==1)
  {
    if (timeMs > (StartTime+PauseTimeSum))
    {
      node->setPosition(Points[0]);
      if (!Loop)
        HasFinished = true;
    }
    return;
  }

  const float dt = ((timeMs-(StartTime+PauseTimeSum)) * Speed * 0.001f);
  const std::int32_t unwrappedIdx = core::floor32(dt);
  if (!Loop && unwrappedIdx >= (std::int32_t)pSize-1)
  {
    node->setPosition(Points[pSize-1]);
    HasFinished = true;
    return;
  }
  const bool pong = PingPong && (unwrappedIdx/(pSize-1))%2;
  const float u =  pong ? 1.f-core::fract (dt) : core::fract (dt);
  const std::int32_t idx = pong ?  (pSize-2) - (unwrappedIdx % (pSize-1))
            : (PingPong ? unwrappedIdx % (pSize-1)
                  : unwrappedIdx % pSize);
  //const float u = 0.001f * fmodf(dt, 1000.0f);

  const glm::vec3& p0 = Points[ clamp(idx - 1, pSize) ];
  const glm::vec3& p1 = Points[ clamp(idx + 0, pSize) ]; // starting point
  const glm::vec3& p2 = Points[ clamp(idx + 1, pSize) ]; // end point
  const glm::vec3& p3 = Points[ clamp(idx + 2, pSize) ];

  // hermite polynomials
  const float h1 = 2.0f * u * u * u - 3.0f * u * u + 1.0f;
  const float h2 = -2.0f * u * u * u + 3.0f * u * u;
  const float h3 = u * u * u - 2.0f * u * u + u;
  const float h4 = u * u * u - u * u;

  // tangents
  const glm::vec3 t1 = (p2 - p0) * Tightness;
  const glm::vec3 t2 = (p3 - p1) * Tightness;

  // interpolated point
  node->setPosition(p1 * h1 + p2 * h2 + t1 * h3 + t2 * h4);
}


//! Writes attributes of the scene node animator.
void CSceneNodeAnimatorFollowSpline::serializeAttributes(io::IAttributes* out, io::SAttributeReadWriteOptions* options) const
{
  ISceneNodeAnimatorFinishing::serializeAttributes(out, options);

  out->addFloat("Speed", Speed);
  out->addFloat("Tightness", Tightness);
  out->addBool("Loop", Loop);
  out->addBool("PingPong", PingPong);

  std::uint32_t count = Points.size();

  if (options && (options->Flags & io::EARWF_FOR_EDITOR))
  {
    // add one point in addition when serializing for editors
    // to make it easier to add points quickly
    count += 1;
  }

  for (std::uint32_t i = 0; i < count; ++i)
  {
    std::string tname = "Point";
    tname += (int)(i+1);

    out->addVector3d(tname.c_str(), i < Points.size() ? Points[i] : glm::vec3(0,0,0));
  }
}


//! Reads attributes of the scene node animator.
void CSceneNodeAnimatorFollowSpline::deserializeAttributes(io::IAttributes* in, io::SAttributeReadWriteOptions* options)
{
  ISceneNodeAnimatorFinishing::deserializeAttributes(in, options);

  Speed = in->getAttributeAsFloat("Speed");
  Tightness = in->getAttributeAsFloat("Tightness");
  Loop = in->getAttributeAsBool("Loop");
  PingPong = in->getAttributeAsBool("PingPong");
  Points.clear();

  for(std::uint32_t i=1; true; ++i)
  {
    std::string pname = "Point";
    pname += i;

    if (in->existsAttribute(pname.c_str()))
      Points.push_back(in->getAttributeAsVector3d(pname.c_str()));
    else
      break;
  }

  // remove last point if double entry from editor
  if (options && (options->Flags & io::EARWF_FOR_EDITOR) &&
    Points.size() > 2 && Points.getLast() == glm::vec3(0,0,0))
  {
    Points.erase(Points.size()-1);

    if (Points.size() > 2 && Points.getLast() == glm::vec3(0,0,0))
      Points.erase(Points.size()-1);
  }
}


ISceneNodeAnimator* CSceneNodeAnimatorFollowSpline::createClone(ISceneNode* node, ISceneManager* newManager)
{
  CSceneNodeAnimatorFollowSpline * newAnimator =
    new CSceneNodeAnimatorFollowSpline(StartTime, Points, Speed, Tightness);
  newAnimator->cloneMembers(this);

  return newAnimator;
}


} // namespace scene
} // namespace saga

