// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#include "CMeshSceneNode.h"
#include "IVideoDriver.h"
#include "ISceneManager.h"
#include "S3DVertex.h"
// #include "ICameraSceneNode.h"
#include "IMeshCache.h"
#include "IAnimatedMesh.h"
#include "IMeshBuffer.h"
#include "CVideoDriver.h"

namespace saga
{
namespace scene
{

//! constructor
CMeshSceneNode::CMeshSceneNode(const std::shared_ptr<IMesh>& mesh, const std::shared_ptr<ISceneNode>& parent,
  const std::shared_ptr<ISceneManager>& mgr,
  const glm::vec3& position, const glm::vec3& rotation,
  const glm::vec3& scale)
  :  IMeshSceneNode(parent, mgr, position, rotation, scale)
{
  setMesh(mesh);
}


//! destructor
CMeshSceneNode::~CMeshSceneNode()
{

}

void CMeshSceneNode::OnRegisterSceneNode(video::RenderPassHandle pass)
{
  if (IsVisible)
  {
    auto& mesh = getMesh();
    auto& meshBuffer = mesh->getMeshBuffer();
    auto& driver = *static_cast<video::CVideoDriver*>(SceneManager->getVideoDriver().get());
    meshBuffer.buildBuffer(pass, driver.getPipeline(Pipeline).Layout);
    driver.createGPUMeshBuffer(meshBuffer, pass);
    ISceneNode::OnRegisterSceneNode(pass);
  }
}

// //! Removes a child from this scene node.
// //! Implemented here, to be able to remove the shadow properly, if there is one,
// //! or to remove attached childs.
// bool CMeshSceneNode::removeChild(ISceneNode* child)
// {
//   if (child && Shadow == child)
//   {
//     Shadow->drop();
//     Shadow = 0;
//   }

//   return ISceneNode::removeChild(child);
// }

//! returns the axis aligned bounding box of this node
const core::aabbox3d<float>& CMeshSceneNode::getBoundingBox() const
{
  return Mesh ? Mesh->getBoundingBox() : Box;
}

//! Creates a clone of this scene node and its children.
// ISceneNode* CMeshSceneNode::clone(ISceneNode* newParent, ISceneManager* newManager)
// {
//   if (!newParent)
//     newParent = Parent;
//   if (!newManager)
//     newManager = SceneManager;

//   CMeshSceneNode* nb = new CMeshSceneNode(Mesh, newParent,
//     newManager, ID, RelativeTranslation, RelativeRotation, RelativeScale);

//   nb->cloneMembers(this, newManager);
//   nb->ReadOnlyMaterials = ReadOnlyMaterials;
//   nb->Materials = Materials;
//   nb->Shadow = Shadow;
//   if (nb->Shadow)
//     nb->Shadow->grab();

//   if (newParent)
//     nb->drop();
//   return nb;
// }


} // namespace scene
} // namespace saga

