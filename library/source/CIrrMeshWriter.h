// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __IRR_IRR_MESH_WRITER_H_INCLUDED__
#define __IRR_IRR_MESH_WRITER_H_INCLUDED__

#include "IMeshWriter.h"
#include "S3DVertex.h"
#include "IVideoDriver.h"
#include "IFileSystem.h"

namespace saga
{
namespace io
{
  class IXMLWriter;
}
namespace scene
{
  class IMeshBuffer;


  //! class to write meshes, implementing a IrrMesh (.irrmesh, .xml) writer
  /** This writer implementation has been originally developed for irrEdit and then
  merged out to the Irrlicht Engine */
  class CIrrMeshWriter : public IMeshWriter
  {
  public:

    CIrrMeshWriter(video::IVideoDriver* driver, io::IFileSystem* fs);
    virtual ~CIrrMeshWriter();

    //! Returns the type of the mesh writer
    virtual EMESH_WRITER_TYPE getType() const override;

    //! writes a mesh
    virtual bool writeMesh(io::IWriteFile* file, scene::IMesh* mesh, std::int32_t flags=EMWF_NONE) override;

  protected:

    void writeBoundingBox(const core::aabbox3df& box);

    void writeMeshBuffer(const scene::IMeshBuffer* buffer);

    void writeMaterial(const video::SMaterial& material);

    std::wstring getVectorAsStringLine(const glm::vec3& v) const;

    std::wstring getVectorAsStringLine(const glm::vec2& v) const;

    // member variables:

    io::IFileSystem* FileSystem;
    video::IVideoDriver* VideoDriver;
    io::IXMLWriter* Writer;
  };

} // namespace
} // namespace

#endif

