// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_OCTREE_H_INCLUDED__
#define __C_OCTREE_H_INCLUDED__

#include "SViewFrustum.h"
#include "S3DVertex.h"
#include "aabbox3d.h"

#include "CMeshBuffer.h"

/**
  Flags for Octree
*/
//! bypass full invisible/visible test
#define OCTREE_PARENTTEST

namespace saga
{

//! template octree.
/** T must be a vertex type which has a member
called .Pos, which is a core::vertex3df position. */
template <class T>
class Octree
{
public:

  struct SMeshChunk : public scene::CMeshBuffer<T>
  {
    SMeshChunk ()
      : scene::CMeshBuffer<T>(), MaterialId(0)
    {
      scene::CMeshBuffer<T>::grab();
    }

    virtual ~SMeshChunk ()
    {
      //removeAllHardwareBuffers
    }

    std::int32_t MaterialId;
  };

  struct SIndexChunk
  {
    std::vector<std::uint16_t> Indices;
    std::int32_t MaterialId;
  };

  struct SIndexData
  {
    std::uint16_t* Indices;
    std::int32_t CurrentSize;
    std::int32_t MaxSize;
  };


  //! Constructor
  Octree(const std::vector<SMeshChunk>& meshes, std::int32_t minimalPolysPerNode=128) :
    IndexData(0), IndexDataCount(meshes.size()), NodeCount(0)
  {
    IndexData = new SIndexData[IndexDataCount];

    // construct array of all indices

    std::vector<SIndexChunk>* indexChunks = new std::vector<SIndexChunk>;
    indexChunks->reallocate(meshes.size());
    for (std::uint32_t i = 0; i!=meshes.size(); ++i)
    {
      IndexData[i].CurrentSize = 0;
      IndexData[i].MaxSize = meshes[i].Indices.size();
      IndexData[i].Indices = new std::uint16_t[IndexData[i].MaxSize];

      indexChunks->push_back(SIndexChunk());
      SIndexChunk& tic = indexChunks->getLast();

      tic.MaterialId = meshes[i].MaterialId;
      tic.Indices = meshes[i].Indices;
    }

    // create tree
    Root = new OctreeNode(NodeCount, 0, meshes, indexChunks, minimalPolysPerNode);
  }

  //! returns all ids of polygons partially or fully enclosed
  //! by this bounding box.
  void calculatePolys(const core::aabbox3d<float>& box)
  {
    for (std::uint32_t i = 0; i!=IndexDataCount; ++i)
      IndexData[i].CurrentSize = 0;

    Root->getPolys(box, IndexData, 0);
  }

  //! returns all ids of polygons partially or fully enclosed
  //! by a view frustum.
  void calculatePolys(const scene::SViewFrustum& frustum)
  {
    for (std::uint32_t i = 0; i!=IndexDataCount; ++i)
      IndexData[i].CurrentSize = 0;

    Root->getPolys(frustum, IndexData, 0);
  }

  const SIndexData* getIndexData() const
  {
    return IndexData;
  }

  std::uint32_t getIndexDataCount() const
  {
    return IndexDataCount;
  }

  std::uint32_t getNodeCount() const
  {
    return NodeCount;
  }

  //! for debug purposes only, collects the bounding boxes of the tree
  void getBoundingBoxes(const core::aabbox3d<float>& box,
    std::vector< const core::aabbox3d<float>* >&outBoxes) const
  {
    Root->getBoundingBoxes(box, outBoxes);
  }

  //! destructor
  ~Octree()
  {
    for (std::uint32_t i = 0; i < IndexDataCount; ++i)
      delete [] IndexData[i].Indices;

    delete [] IndexData;
    delete Root;
  }

private:
  // private inner class
  class OctreeNode
  {
  public:

    // constructor
    OctreeNode(std::uint32_t& nodeCount, std::uint32_t currentdepth,
      const std::vector<SMeshChunk>& allmeshdata,
      std::vector<SIndexChunk>* indices,
      std::int32_t minimalPolysPerNode) : IndexData(0),
      Depth(currentdepth+1)
    {
      ++nodeCount;

      std::uint32_t i; // new ISO for scoping problem with different compilers

      for (i = 0; i!=8; ++i)
        Children[i] = 0;

      if (indices->empty())
      {
        delete indices;
        return;
      }

      bool found = false;

      // find first point for bounding box

      for (i = 0; i < indices->size(); ++i)
      {
        if (!(*indices)[i].Indices.empty())
        {
          Box.reset(allmeshdata[i].Vertices[(*indices)[i].Indices[0]].Pos);
          found = true;
          break;
        }
      }

      if (!found)
      {
        delete indices;
        return;
      }

      std::int32_t totalPrimitives = 0;

      // now lets calculate our bounding box
      for (i = 0; i < indices->size(); ++i)
      {
        totalPrimitives += (*indices)[i].Indices.size();
        for (std::uint32_t j= 0; j<(*indices)[i].Indices.size(); ++j)
          Box.addInternalPoint(allmeshdata[i].Vertices[(*indices)[i].Indices[j]].Pos);
      }

      glm::vec3 middle = Box.getCenter();
      glm::vec3 edges[8];
      Box.getEdges(edges);

      // calculate all children
      core::aabbox3d<float> box;
      std::vector<std::uint16_t> keepIndices;

      if (totalPrimitives > minimalPolysPerNode && !Box.isEmpty())
      for (std::uint32_t ch= 0; ch!=8; ++ch)
      {
        box.reset(middle);
        box.addInternalPoint(edges[ch]);

        // create indices for child
        bool added = false;
        std::vector<SIndexChunk>* cindexChunks = new std::vector<SIndexChunk>;
        cindexChunks->reallocate(allmeshdata.size());
        for (i = 0; i < allmeshdata.size(); ++i)
        {
          cindexChunks->push_back(SIndexChunk());
          SIndexChunk& tic = cindexChunks->getLast();
          tic.MaterialId = allmeshdata[i].MaterialId;

          for (std::uint32_t t= 0; t<(*indices)[i].Indices.size(); t+=3)
          {
            if (box.isPointInside(allmeshdata[i].Vertices[(*indices)[i].Indices[t]].Pos) &&
              box.isPointInside(allmeshdata[i].Vertices[(*indices)[i].Indices[t+1]].Pos) &&
              box.isPointInside(allmeshdata[i].Vertices[(*indices)[i].Indices[t+2]].Pos))
            {
              tic.Indices.push_back((*indices)[i].Indices[t]);
              tic.Indices.push_back((*indices)[i].Indices[t+1]);
              tic.Indices.push_back((*indices)[i].Indices[t+2]);

              added = true;
            }
            else
            {
              keepIndices.push_back((*indices)[i].Indices[t]);
              keepIndices.push_back((*indices)[i].Indices[t+1]);
              keepIndices.push_back((*indices)[i].Indices[t+2]);
            }
          }

          (*indices)[i].Indices.set_used(keepIndices.size());
          memcpy((*indices)[i].Indices.pointer(), keepIndices.data(), keepIndices.size()*sizeof(std::uint16_t));
          // keepIndices.set_used(0);
        }

        if (added)
          Children[ch] = new OctreeNode(nodeCount, Depth,
            allmeshdata, cindexChunks, minimalPolysPerNode);
        else
          delete cindexChunks;

      } // end for all possible children

      IndexData = indices;
    }

    // destructor
    ~OctreeNode()
    {
      delete IndexData;

      for (std::uint32_t i = 0; i < 8; ++i)
        delete Children[i];
    }

    // returns all ids of polygons partially or full enclosed
    // by this bounding box.
    void getPolys(const core::aabbox3d<float>& box, SIndexData* idxdata, std::uint32_t parentTest) const
    {
#if defined (OCTREE_PARENTTEST)
      // if not full inside
      if (parentTest != 2)
      {
        // partially inside ?
        if (!Box.intersectsWithBox(box))
          return;

        // fully inside ?
        parentTest = Box.isFullInside(box)?2:1;
      }
#else
      if (Box.intersectsWithBox(box))
#endif
      {
        const std::uint32_t cnt = IndexData->size();
        std::uint32_t i; // new ISO for scoping problem in some compilers

        for (i = 0; i < cnt; ++i)
        {
          const std::int32_t idxcnt = (*IndexData)[i].Indices.size();

          if (idxcnt)
          {
            memcpy(&idxdata[i].Indices[idxdata[i].CurrentSize],
              &(*IndexData)[i].Indices[0], idxcnt * sizeof(std::int16_t));
            idxdata[i].CurrentSize += idxcnt;
          }
        }

        for (i = 0; i!=8; ++i)
          if (Children[i])
            Children[i]->getPolys(box, idxdata,parentTest);
      }
    }

    // returns all ids of polygons partially or full enclosed
    // by the view frustum.
    void getPolys(const scene::SViewFrustum& frustum, SIndexData* idxdata,std::uint32_t parentTest) const
    {
      std::uint32_t i; // new ISO for scoping problem in some compilers

      // if parent is fully inside, no further check for the children is needed
#if defined (OCTREE_PARENTTEST)
      if (parentTest != 2)
#endif
      {
#if defined (OCTREE_PARENTTEST)
        parentTest = 2;
#endif
        for (i = 0; i!=scene::SViewFrustum::SViewFrustum::VIEW_FRUSTUM_PLANE_COUNT; ++i)
        {
          core::EIntersectionRelation3D r = Box.classifyPlaneRelation(frustum.planes[i]);
          if (r == core::ISREL3D_FRONT)
            return;
#if defined (OCTREE_PARENTTEST)
          if (r == core::ISREL3D_CLIPPED)
            parentTest = 1;  // must still check children
#endif
        }
      }


      const std::uint32_t cnt = IndexData->size();

      for (i = 0; i!=cnt; ++i)
      {
        std::int32_t idxcnt = (*IndexData)[i].Indices.size();

        if (idxcnt)
        {
          memcpy(&idxdata[i].Indices[idxdata[i].CurrentSize],
            &(*IndexData)[i].Indices[0], idxcnt * sizeof(std::int16_t));
          idxdata[i].CurrentSize += idxcnt;
        }
      }

      for (i = 0; i!=8; ++i)
        if (Children[i])
          Children[i]->getPolys(frustum, idxdata,parentTest);
    }

    //! for debug purposes only, collects the bounding boxes of the node
    void getBoundingBoxes(const core::aabbox3d<float>& box,
      std::vector< const core::aabbox3d<float>* >&outBoxes) const
    {
      if (Box.intersectsWithBox(box))
      {
        outBoxes.push_back(&Box);

        for (std::uint32_t i = 0; i!=8; ++i)
          if (Children[i])
            Children[i]->getBoundingBoxes(box, outBoxes);
      }
    }

  private:

    core::aabbox3df Box;
    std::vector<SIndexChunk>* IndexData;
    OctreeNode* Children[8];
    std::uint32_t Depth;
  };

  OctreeNode* Root;
  SIndexData* IndexData;
  std::uint32_t IndexDataCount;
  std::uint32_t NodeCount;
};

} // namespace

#endif

