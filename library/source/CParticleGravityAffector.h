// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_PARTICLE_GRAVITY_AFFECTOR_H_INCLUDED__
#define __C_PARTICLE_GRAVITY_AFFECTOR_H_INCLUDED__


#ifdef _IRR_COMPILE_WITH_PARTICLES_

#include "IParticleGravityAffector.h"
#include "SColor.h"

namespace saga
{
namespace scene
{

//! Particle Affector for affecting direction of particle
class CParticleGravityAffector : public IParticleGravityAffector
{
public:

  CParticleGravityAffector(
    const glm::vec3& gravity = glm::vec3(0.0f,-0.03f,0.0f),
    std::uint32_t timeForceLost = 1000);

  //! Affects a particle.
  virtual void affect(std::uint32_t now, SParticle* particlearray, std::uint32_t count) override;

  //! Set the time in milliseconds when the gravity force is totally
  //! lost and the particle does not move any more.
  virtual void setTimeForceLost(float timeForceLost) override { TimeForceLost = timeForceLost; }

  //! Set the direction and force of gravity.
  virtual void setGravity(const glm::vec3& gravity) override { Gravity = gravity; }

  //! Set the time in milliseconds when the gravity force is totally
  //! lost and the particle does not move any more.
  virtual float getTimeForceLost() const override { return TimeForceLost; }

  //! Set the direction and force of gravity.
  virtual const glm::vec3& getGravity() const override { return Gravity; }

  //! Writes attributes of the object.
  //! Implement this to expose the attributes of your scene node animator for
  //! scripting languages, editors, debuggers or xml serialization purposes.
  virtual void serializeAttributes(io::IAttributes* out, io::SAttributeReadWriteOptions* options) const override;

  //! Reads attributes of the object.
  //! Implement this to set the attributes of your scene node animator for
  //! scripting languages, editors, debuggers or xml deserialization purposes.
  //! \param startIndex: start index where to start reading attributes.
  //! \return: returns last index of an attribute read by this affector
  virtual void deserializeAttributes(io::IAttributes* in, io::SAttributeReadWriteOptions* options) override;

private:
  float TimeForceLost;
  glm::vec3 Gravity;
};

} // namespace scene
} // namespace saga

#endif // _IRR_COMPILE_WITH_PARTICLES_

#endif

