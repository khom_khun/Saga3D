// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#include "CBoneSceneNode.h"

namespace saga
{
namespace scene
{

//! constructor
CBoneSceneNode::CBoneSceneNode(const std::shared_ptr<ISceneNode>& parent, const std::shared_ptr<ISceneManager>& mgr,
  std::uint32_t boneIndex, const std::string& boneName)
: IBoneSceneNode(parent, mgr), BoneIndex(boneIndex),
  AnimationMode(E_BONE_ANIMATION_MODE::AUTOMATIC), SkinningSpace(E_BONE_SKINNING_SPACE::LOCAL)
{
  setName(boneName);
}

//! Returns the index of the bone
std::uint32_t CBoneSceneNode::getBoneIndex() const
{
  return BoneIndex;
}

//! Sets the animation mode of the bone. Returns true if successful.
bool CBoneSceneNode::setAnimationMode(E_BONE_ANIMATION_MODE mode)
{
  AnimationMode = mode;
  return true;
}

//! Gets the current animation mode of the bone
E_BONE_ANIMATION_MODE CBoneSceneNode::getAnimationMode() const
{
  return AnimationMode;
}

//! returns the axis aligned bounding box of this node
const core::aabbox3d<float>& CBoneSceneNode::getBoundingBox() const
{
  return Box;
}

/*
//! Returns the relative transformation of the scene node.
glm::mat4 CBoneSceneNode::getRelativeTransformation() const
{
  return glm::mat4(); // RelativeTransformation;
}
*/

void CBoneSceneNode::OnAnimate(std::uint32_t timeMs)
{
  // if (IsVisible)
  // {
  //   // animate this node with all animators

  //   ISceneNodeAnimatorList::Iterator ait = Animators.begin();
  //   for (; ait != Animators.end(); ++ait)
  //     (*ait)->animateNode(this, timeMs);

  //   // update absolute position
  //   //updateAbsolutePosition();

  //   // perform the post render process on all children
  //   ISceneNodeList::Iterator it = Children.begin();
  //   for (; it != Children.end(); ++it)
  //     (*it)->OnAnimate(timeMs);
  // }
}


void CBoneSceneNode::helper_updateAbsolutePositionOfAllChildren(const std::shared_ptr<ISceneNode>& Node)
{
  // Node->updateAbsolutePosition();

  // ISceneNodeList::ConstIterator it = Node->getChildren().begin();
  // for (; it != Node->getChildren().end(); ++it)
  // {
  //   helper_updateAbsolutePositionOfAllChildren((*it));
  // }
}


void CBoneSceneNode::updateAbsolutePositionOfAllChildren()
{
  // helper_updateAbsolutePositionOfAllChildren(this);
}

} // namespace scene
} // namespace saga
