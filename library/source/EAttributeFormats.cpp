#include "EAttributeFormats.h"

namespace saga
{
namespace video
{

int GetVertexAttributeSize(const E_ATTRIBUTE_FORMAT format)
{
  switch (format)
  {
    // FIXME(manh): add more attribute format
    case E_ATTRIBUTE_FORMAT::FLOAT: return sizeof(float);
    case E_ATTRIBUTE_FORMAT::FLOAT2: return sizeof(float) * 2;
    case E_ATTRIBUTE_FORMAT::FLOAT3: return sizeof(float) * 3;
    case E_ATTRIBUTE_FORMAT::FLOAT4: return sizeof(float) * 4;
    case E_ATTRIBUTE_FORMAT::SHORT2: return sizeof(short) * 2;
  }
}

} // namespace video
} // namespace saga
