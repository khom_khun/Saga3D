// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

// The code for the TerrainSceneNode is based on the GeoMipMapSceneNode
// developed by Spintz. He made it available for Irrlicht and allowed it to be
// distributed under this licence. I only modified some parts. A lot of thanks go to him.

#ifndef __C_TERRAIN_SCENE_NODE_H__
#define __C_TERRAIN_SCENE_NODE_H__

#include "ITerrainSceneNode.h"
#include "IDynamicMeshBuffer.h"

namespace saga
{
namespace io
{
  class IFileSystem;
  class IReadFile;
}
namespace scene
{
  struct SMesh;
  class ITextSceneNode;

  //! A scene node for displaying terrain using the geo mip map algorithm.
  class CTerrainSceneNode : public ITerrainSceneNode
  {
  public:

    //! constructor
    //! \param parent: The node which this node is a child of.  Making this node a child of another node, or
    //! making it a parent of another node is yet untested and most likely does not work properly.
    //! \param mgr: Pointer to the scene manager.
    //! \param id: The id of the node
    //! \param maxLOD: The maximum LOD (Level of Detail) for the node.
    //! \param patchSize: An E_GEOMIPMAP_PATCH_SIZE enumeration defining the size of each patch of the terrain.
    //! \param position: The absolute position of this node.
    //! \param rotation: The absolute rotation of this node. (NOT YET IMPLEMENTED)
    //! \param scale: The scale factor for the terrain.  If you're using a heightmap of size 128x128 and would like
    //! your terrain to be 12800x12800 in game units, then use a scale factor of (core::vector (100.0f, 100.0f, 100.0f).
    //! If you use a Y scaling factor of 0.0f, then your terrain will be flat.
    CTerrainSceneNode(ISceneNode* parent, ISceneManager* mgr, io::IFileSystem* fs, std::int32_t id,
      std::int32_t maxLOD = 4, E_TERRAIN_PATCH_SIZE patchSize = E_TERRAIN_PATCH_SIZE::SIZE_17,
      const glm::vec3& position = glm::vec3(0.0f, 0.0f, 0.0f),
      const glm::vec3& rotation = glm::vec3(0.0f, 0.0f, 0.0f),
      const glm::vec3& scale = glm::vec3(1.0f, 1.0f, 1.0f));

    virtual ~CTerrainSceneNode();

    //! Initializes the terrain data.  Loads the vertices from the heightMapFile.
    virtual bool loadHeightMap(io::IReadFile* file,
      video::SColor vertexColor = video::SColor (255, 255, 255, 255), std::int32_t smoothFactor = 0) override;

    //! Initializes the terrain data.  Loads the vertices from the heightMapFile.
    virtual bool loadHeightMapRAW(io::IReadFile* file, std::int32_t bitsPerPixel = 16,
      bool signedData=true, bool floatVals=false, std::int32_t width= 0,
      video::SColor vertexColor = video::SColor (255, 255, 255, 255), std::int32_t smoothFactor = 0) override;

    //! Returns the material based on the zero based index i. This scene node only uses
    //! 1 material.
    //! \param i: Zero based index i. UNUSED, left in for virtual purposes.
    //! \return Returns the single material this scene node uses.
    // virtual video::SMaterial& getMaterial(std::uint32_t i) override;

    //! Returns amount of materials used by this scene node (always 1)
    //! \return Returns current count of materials used by this scene node (always 1)
    virtual std::uint32_t getMaterialCount() const override;

    //! Gets the last scaling factor applied to the scene node.  This value only represents the
    //! last scaling factor presented to the node.  For instance, if you make create the node
    //! with a scale factor of (1.0f, 1.0f, 1.0f) then call setScale (50.0f, 5.0f, 50.0f),
    //! then make another call to setScale with the values (2.0f, 2.0f, 2.0f), this will return
    //! glm::vec3 (2.0f, 2.0f, 2.0f), although the total scaling of the scene node is
    //! glm::vec3 (100.0f, 10.0f, 100.0f).
    //! \return Returns the last scaling factor passed to the scene node.
    virtual const glm::vec3& getScale() const override
    {
      return TerrainData.Scale;
    }

    //! Scales the scene nodes vertices by the vector specified.
    //! \param scale: Scaling factor to apply to the node.
    virtual void setScale(const glm::vec3& scale) override;

    //! Gets the last rotation factor applied to the scene node.
    //! \return Returns the last rotation factor applied to the scene node.
    virtual const glm::vec3& getRotation() const override
    {
      return TerrainData.Rotation;
    }

    //! Rotates the node. This only modifies the relative rotation of the node.
    //! \param rotation: New rotation of the node in degrees.
    virtual void setRotation(const glm::vec3& rotation) override;

    //! Sets the pivot point for rotation of this node.
    //! NOTE: The default for the RotationPivot will be the center of the individual tile.
    virtual void setRotationPivot(const glm::vec3& pivot);

    //! Gets the last positioning vector applied to the scene node.
    //! \return Returns the last position vector applied to the scene node.
    virtual const glm::vec3& getPosition() const override
    {
      return TerrainData.Position;
    }

    //! Moves the scene nodes vertices by the vector specified.
    //! \param newpos: Vector specifying how much to move each vertex of the scene node.
    virtual void setPosition(const glm::vec3& newpos) override;

    //! Updates the scene nodes indices if the camera has moved or rotated by a certain
    //! threshold, which can be changed using the SetCameraMovementDeltaThreshold and
    //! SetCameraRotationDeltaThreshold functions.  This also determines if a given patch
    //! for the scene node is within the view frustum and if it's not the indices are not
    //! generated for that patch.
    virtual void OnRegisterSceneNode(video::RenderPassHandle pass) override;

    //! Render the scene node
    virtual void render() override;

    //! Return the bounding box of the entire terrain.
    virtual const core::aabbox3d<float>& getBoundingBox() const override;

    //! Return the bounding box of a patch
    virtual const core::aabbox3d<float>& getBoundingBox(std::int32_t patchX, std::int32_t patchZ) const override;

    //! Return the number of indices currently used to draw the scene node.
    virtual std::uint32_t getIndexCount() const override { return IndicesToRender; }

    //! Returns the mesh
    virtual IMesh* getMesh() override;

    //! Returns a pointer to the buffer used by the terrain (most users will not need this)
    virtual IMeshBuffer* getRenderBuffer() override { return RenderBuffer; }

    //! Gets the meshbuffer data based on a specified Level of Detail.
    //! \param mb: A reference to an IDynamicMeshBuffer object
    //! \param LOD: The Level Of Detail you want the indices from.
    virtual void getMeshBufferForLOD(IDynamicMeshBuffer& mb, std::int32_t LOD= 0) const override;

    //! Gets the indices for a specified patch at a specified Level of Detail.
    //! \param indices: A reference to an array of std::uint32_t indices.
    //! \param patchX: Patch x coordinate.
    //! \param patchZ: Patch z coordinate.
    //! \param LOD: The level of detail to get for that patch.  If -1, then get
    //! the CurrentLOD.  If the CurrentLOD is set to -1, meaning it's not shown,
    //! then it will retrieve the triangles at the highest LOD (0).
    //! \return: Number of indices put into the buffer.
    virtual std::int32_t getIndicesForPatch(std::vector<std::uint32_t>& indices,
      std::int32_t patchX, std::int32_t patchZ, std::int32_t LOD= 0) override;

    //! Populates an array with the CurrentLOD of each patch.
    //! \param LODs: A reference to a std::vector<std::int32_t> to hold the values
    //! \return Returns the number of elements in the array
    virtual std::int32_t getCurrentLODOfPatches(std::vector<std::int32_t>& LODs) const override;

    //! Manually sets the LOD of a patch
    //! \param patchX: Patch x coordinate.
    //! \param patchZ: Patch z coordinate.
    //! \param LOD: The level of detail to set the patch to.
    virtual void setLODOfPatch(std::int32_t patchX, std::int32_t patchZ, std::int32_t LOD= 0) override;

    //! Returns center of terrain.
    virtual const glm::vec3& getTerrainCenter() const override
    {
      return TerrainData.Center;
    }

    //! Returns center of terrain.
    virtual float getHeight(float x, float y) const override;

    //! Sets the movement camera threshold which is used to determine when to recalculate
    //! indices for the scene node.  The default value is 10.0f.
    virtual void setCameraMovementDelta(float delta) override
    {
      CameraMovementDelta = delta;
    }

    //! Sets the rotation camera threshold which is used to determine when to recalculate
    //! indices for the scene node.  The default value is 1.0f.
    virtual void setCameraRotationDelta(float delta) override
    {
      CameraRotationDelta = delta;
    }

    //! Sets whether or not the node should dynamically update it its associated selector when
    //! the geomipmap data changes.
    //! param bVal: Boolean value representing whether or not to update selector dynamically.
    //! NOTE: Temporarily disabled while working out issues with DynamicSelectorUpdate
    virtual void setDynamicSelectorUpdate(bool bVal) override { DynamicSelectorUpdate = false; }

    //! Override the default generation of distance thresholds for determining the LOD a patch
    //! is rendered at. If any LOD is overridden, then the scene node will no longer apply
    //! scaling factors to these values. If you override these distances and then apply
    //! a scale to the scene node, it is your responsibility to update the new distances to
    //! work best with your new terrain size.
    virtual bool overrideLODDistance(std::int32_t LOD, double newDistance) override;

    //! Scales the two textures
    virtual void scaleTexture(float scale = 1.0f, float scale2 = 0.0f) override;

    //! Force node to use a fixed LOD level at the borders of the terrain.
    virtual void setFixedBorderLOD(std::int32_t borderLOD)  override
    {
      FixedBorderLOD = borderLOD;
    }

    //! Returns type of the scene node
    virtual E_SCENE_NODE_TYPE getType() const override {return E_SCENE_NODE_TYPE::TERRAIN;}

    //! Writes attributes of the scene node.
    virtual void serializeAttributes(io::IAttributes* out,
        io::SAttributeReadWriteOptions* options= 0) const override;

    //! Reads attributes of the scene node.
    virtual void deserializeAttributes(io::IAttributes* in,
        io::SAttributeReadWriteOptions* options= 0) override;

    //! Creates a clone of this scene node and its children.
    virtual ISceneNode* clone(ISceneNode* newParent,
        ISceneManager* newManager) override;

  private:
    friend class CTerrainTriangleSelector;

    struct SPatch
    {
      SPatch()
      : Top(0), Bottom(0), Right(0), Left(0), CurrentLOD(-1)
      {
      }

      SPatch* Top;
      SPatch* Bottom;
      SPatch* Right;
      SPatch* Left;
      std::int32_t CurrentLOD;
      core::aabbox3df BoundingBox;
      glm::vec3 Center;
    };

    struct STerrainData
    {
      STerrainData(std::int32_t patchSize, std::int32_t maxLOD, const glm::vec3& position, const glm::vec3& rotation, const glm::vec3& scale)
      : Patches(0), Size(0), Position(position), Rotation(rotation),
        Scale(scale), PatchSize(patchSize), CalcPatchSize(patchSize-1),
        PatchCount(0), MaxLOD(maxLOD)
      {
      }

      SPatch*    Patches;
      std::int32_t    Size;
      glm::vec3  Position;
      glm::vec3  Rotation;
      glm::vec3 RotationPivot;
      glm::vec3  Scale;
      glm::vec3 Center;
      std::int32_t    PatchSize;
      std::int32_t    CalcPatchSize;
      std::int32_t    PatchCount;
      std::int32_t    MaxLOD;
      core::aabbox3df  BoundingBox;
      std::vector<double> LODDistanceThreshold;
    };

    void preRenderCalculationsIfNeeded();
    void preRenderLODCalculations();
    void preRenderIndicesCalculations();

    //! get indices when generating index data for patches at varying levels of detail.
    std::uint32_t getIndex(const std::int32_t PatchX, const std::int32_t PatchZ, const std::int32_t PatchIndex, std::uint32_t vX, std::uint32_t vZ) const;

    //! smooth the terrain
    void smoothTerrain(IDynamicMeshBuffer* mb, std::int32_t smoothFactor);

    //! calculate smooth normals
    void calculateNormals(IDynamicMeshBuffer* mb);

    //! create patches, stuff that needs to only be done once for patches goes here.
    void createPatches();

    //! calculate the internal STerrainData structure
    void calculatePatchData();

    //! calculate or recalculate the distance thresholds
    void calculateDistanceThresholds(bool scalechanged = false);

    //! sets the CurrentLOD of all patches to the specified LOD
    void setCurrentLODOfPatches(std::int32_t i);

    //! sets the CurrentLOD of TerrainData patches to the LODs specified in the array
    void setCurrentLODOfPatches(const std::vector<std::int32_t>& lodarray);

    //! Apply transformation changes(scale, position, rotation)
    void applyTransformation();

    STerrainData TerrainData;
    SMesh* Mesh;

    IDynamicMeshBuffer *RenderBuffer;

    std::uint32_t VerticesToRender;
    std::uint32_t IndicesToRender;

    bool DynamicSelectorUpdate;
    bool OverrideDistanceThreshold;
    bool UseDefaultRotationPivot;
    bool ForceRecalculation;
    std::int32_t FixedBorderLOD;

    glm::vec3  OldCameraPosition;
    glm::vec3  OldCameraRotation;
    glm::vec3  OldCameraUp;
    float             OldCameraFOV;
    float CameraMovementDelta;
    float CameraRotationDelta;
    float CameraFOVDelta;

    // needed for (de)serialization
    float TCoordScale1;
    float TCoordScale2;
    std::int32_t SmoothFactor;
    std::string HeightmapFile;
    io::IFileSystem* FileSystem;
  };


} // namespace scene
} // namespace saga

#endif // __C_TERRAIN_SCENE_NODE_H__


