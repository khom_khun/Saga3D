// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#include "CCameraSceneNode.h"
#include "ISceneManager.h"
#include "IVideoDriver.h"
#include "irrMath.h"
#include <glm/gtc/matrix_transform.hpp>

namespace saga
{
namespace scene
{

//! constructor
CCameraSceneNode::CCameraSceneNode(const std::shared_ptr<ISceneNode>& parent, const std::shared_ptr<ISceneManager>& mgr,
  const glm::vec3& position, const glm::vec3& lookAt)
  : ICameraSceneNode(parent, mgr, position),
  BoundingBox(glm::vec3(0, 0, 0)),
  Target(lookAt), UpVector(0.0f, 1.0f, 0.0f), ZNear(0.1f), ZFar(100.0f)
{
  Fovy = 80;
  updateProjectionMatrix();
}

//! Sets the projection matrix of the camera.
//! \param projection: The new projection matrix of the camera.
void CCameraSceneNode::setProjectionMatrix(const glm::mat4& projection, bool isOrthogonal)
{
  IsOrthogonal = isOrthogonal;
  Projection = projection;
}

//! Gets the current projection matrix of the camera
//! \return Returns the current projection matrix of the camera.
const glm::mat4& CCameraSceneNode::getProjectionMatrix() const
{
  return Projection;
}

//! Gets the current view matrix of the camera
//! \return Returns the current view matrix of the camera.
const glm::mat4& CCameraSceneNode::getViewMatrix() const
{
  return View;
}

//! It is possible to send mouse and key events to the camera. Most cameras
//! may ignore this input, but camera scene nodes which are created for
//! example with scene::ISceneManager::addMayaCameraSceneNode or
//! scene::ISceneManager::addFPSCameraSceneNode, may want to get this input
//! for changing their position, look at target or whatever.
void CCameraSceneNode::OnEvent(const SDL_Event& event)
{
  for (auto& a : Animators)
  {
    a->OnEvent(event);
  }
}

//! sets the look at target of the camera
//! \param pos: Look at target of the camera.
void CCameraSceneNode::setTarget(const glm::vec3& newTarget)
{
  Target = newTarget;
}

//! Sets the rotation of the node.
/** This only modifies the relative rotation of the node.
If the camera's target and rotation are bound (@see bindTargetAndRotation())
then calling this will also change the camera's target to match the rotation.
\param rotation New rotation of the node in degrees. */
void CCameraSceneNode::setRotation(const glm::vec3& rotation)
{
  ISceneNode::setRotation(rotation);
}

//! Gets the current look at target of the camera
//! \return Returns the current look at target of the camera
const glm::vec3& CCameraSceneNode::getTarget() const
{
  return Target;
}

//! sets the up vector of the camera
//! \param pos: New upvector of the camera.
void CCameraSceneNode::setUpVector(const glm::vec3& pos)
{
  UpVector = pos;
}

//! Gets the up vector of the camera.
//! \return Returns the up vector of the camera.
const glm::vec3& CCameraSceneNode::getUpVector() const
{
  return UpVector;
}

float CCameraSceneNode::getNearValue() const
{
  return ZNear;
}

float CCameraSceneNode::getFarValue() const
{
  return ZFar;
}

float CCameraSceneNode::getAspectRatio() const
{
  return Aspect;
}

float CCameraSceneNode::getFOV() const
{
  return Fovy;
}

void CCameraSceneNode::setNearValue(float f)
{
  ZNear = f;
  updateProjectionMatrix();
}

void CCameraSceneNode::setFarValue(float f)
{
  ZFar = f;
  updateProjectionMatrix();
}

void CCameraSceneNode::setAspectRatio(float f)
{
  Aspect = f;
  updateProjectionMatrix();
}

void CCameraSceneNode::setFOV(float f)
{
  Fovy = f;
  updateProjectionMatrix();
}

void CCameraSceneNode::updateViewMatrix()
{
  const auto pos = getAbsolutePosition();
  View = glm::rotate(glm::mat4(1.f), glm::radians(RelativeRotation.x), {1.f, 0.f, 0.f});
  View = glm::rotate(View, glm::radians(RelativeRotation.y), {0.f, 1.f, 0.f});
  View = glm::rotate(View, glm::radians(RelativeRotation.z), {0.f, 0.f, 1.f});
  View = glm::translate(View, -pos);
}

void CCameraSceneNode::updateProjectionMatrix()
{
  auto& driver = SceneManager->getVideoDriver();
  float width = driver->getWidth();
  float height = driver->getHeight();
  Aspect = width / height;

  Projection = glm::perspective(glm::radians(Fovy), Aspect, ZNear, ZFar);
  Projection[1][1] *= -1.f;
}

//! returns the axis aligned bounding box of this node
const core::aabbox3d<float>& CCameraSceneNode::getBoundingBox() const
{
  return BoundingBox;
}

//! Creates a clone of this scene node and its children.
// ISceneNode* CCameraSceneNode::clone(ISceneNode* newParent, ISceneManager* newManager)
// {
//   ICameraSceneNode::clone(newParent, newManager);

//   if (!newParent)
//     newParent = Parent;
//   if (!newManager)
//     newManager = SceneManager;

//   CCameraSceneNode* nb = new CCameraSceneNode(newParent,
//     newManager, ID, RelativeTranslation, Target);

//   nb->ISceneNode::cloneMembers(this, newManager);
//   nb->ICameraSceneNode::cloneMembers(this);

//   nb->Target = Target;
//   nb->UpVector = UpVector;
//   nb->Fovy = Fovy;
//   nb->Aspect = Aspect;
//   nb->ZNear = ZNear;
//   nb->ZFar = ZFar;
//   nb->ViewArea = ViewArea;
//   nb->Affector = Affector;
//   nb->InputReceiverEnabled = InputReceiverEnabled;
//   nb->TargetAndRotationAreBound = TargetAndRotationAreBound;

//   if (newParent)
//     nb->drop();
//   return nb;
// }


} // namespace
} // namespace

