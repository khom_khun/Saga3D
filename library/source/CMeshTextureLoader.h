#ifndef IRR_C_MESH_TEXTURE_LOADER_H_INCLUDED
#define IRR_C_MESH_TEXTURE_LOADER_H_INCLUDED

#include "IMeshTextureLoader.h"

namespace saga
{
namespace io
{
  class IFileSystem;
} // namespace io
namespace video
{
  class IVideoDriver;
}

namespace scene
{

class CMeshTextureLoader : public IMeshTextureLoader
{
public:
  CMeshTextureLoader(saga::io::IFileSystem* fs, saga::video::IVideoDriver* driver);

  //! Set a custom texture path.
    /**  This is the first path the texture-loader should search.  */
  virtual void setTexturePath(const saga::std::string& path)  override;

  //! Get the current custom texture path.
  virtual const saga::std::string& getTexturePath() const  override;

  //! Get the texture by searching for it in all paths that makes sense for the given textureName.
  /** Usually you do not have to use this method, it is used internally by IMeshLoader's.
  \param textureName Texturename as used in the mesh-format
  \return Pointer to the texture. Returns 0 if loading failed.*/
  virtual saga::video::ITexture* getTexture(const saga::std::string& textureName)  override;

  //! Meshloaders will search paths relative to the meshFile.
  /** Usually you do not have to use this method, it is used internally by IMeshLoader's.
    Any values you set here will likely be overwritten internally. */
  virtual void setMeshFile(const saga::io::IReadFile* meshFile) override;

  //! Meshloaders will try to look relative to the path of the materialFile
  /** Usually you do not have to use this method, it is used internally by IMeshLoader's.
  Any values you set here will likely be overwritten internally.  */
  virtual void setMaterialFile(const saga::io::IReadFile* materialFile) override;

protected:
  // make certain path's have a certain internal format
  void preparePath(saga::std::string& directory)
  {
    if (!directory.empty())
    {
      if (directory == _IRR_TEXT("."))
        directory = _IRR_TEXT("");

      directory.replace(_IRR_TEXT('\\'),_IRR_TEXT('/'));
      if (directory.lastChar() != _IRR_TEXT('/'))
        directory.append(_IRR_TEXT('/'));
    }
  }

  // Save the texturename when it's a an existing file
  bool checkTextureName(const saga::std::string& filename);

private:
  saga::io::IFileSystem * FileSystem;
  saga::video::IVideoDriver* VideoDriver;
  saga::std::string TexturePath;
  const saga::io::IReadFile* MeshFile;
  saga::std::string MeshPath;
  const saga::io::IReadFile* MaterialFile;
  saga::std::string MaterialPath;
  saga::std::string TextureName;
};

} // namespace scene
} // namespace saga

#endif

