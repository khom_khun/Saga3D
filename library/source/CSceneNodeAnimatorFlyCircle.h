// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_SCENE_NODE_ANIMATOR_FLY_CIRCLE_H_INCLUDED__
#define __C_SCENE_NODE_ANIMATOR_FLY_CIRCLE_H_INCLUDED__

#include "ISceneNode.h"

namespace saga
{
namespace scene
{
  class CSceneNodeAnimatorFlyCircle : public ISceneNodeAnimator
  {
  public:

    //! constructor
    CSceneNodeAnimatorFlyCircle(std::uint32_t time,
        const glm::vec3& center, float radius,
        float speed, const glm::vec3& direction,
        float radiusEllipsoid);

    //! animates a scene node
    virtual void animateNode(ISceneNode* node, std::uint32_t timeMs) override;

    //! Writes attributes of the scene node animator.
    virtual void serializeAttributes(io::IAttributes* out, io::SAttributeReadWriteOptions* options= 0) const override;

    //! Reads attributes of the scene node animator.
    virtual void deserializeAttributes(io::IAttributes* in, io::SAttributeReadWriteOptions* options= 0) override;

    //! Returns type of the scene node animator
    virtual ESCENE_NODE_ANIMATOR_TYPE getType() const override { return ESNAT_FLY_CIRCLE; }

    //! Creates a clone of this animator.
    /** Please note that you will have to drop
    (IReferenceCounted::drop()) the returned pointer after calling
    this. */
    virtual ISceneNodeAnimator* createClone(ISceneNode* node, ISceneManager* newManager= 0) override;

  private:
    // do some initial calculations
    void init();

    // circle center
    glm::vec3 Center;
    // up-vector, normal to the circle's plane
    glm::vec3 Direction;
    // Two helper vectors
    glm::vec3 VecU;
    glm::vec3 VecV;
    float Radius;
    float RadiusEllipsoid;
    float Speed;
  };


} // namespace scene
} // namespace saga

#endif

