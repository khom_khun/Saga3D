// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_ANIMATED_MESH_SCENE_NODE_H_INCLUDED__
#define __C_ANIMATED_MESH_SCENE_NODE_H_INCLUDED__

#include "IAnimatedMeshSceneNode.h"
#include "IAnimatedMesh.h"

namespace saga
{
namespace scene
{
  class IDummyTransformationSceneNode;

  class CAnimatedMeshSceneNode : public IAnimatedMeshSceneNode
  {
  public:

    //! constructor
    CAnimatedMeshSceneNode(IAnimatedMesh* mesh, ISceneNode* parent, ISceneManager* mgr,  std::int32_t id,
      const glm::vec3& position = glm::vec3(0,0,0),
      const glm::vec3& rotation = glm::vec3(0,0,0),
      const glm::vec3& scale = glm::vec3(1.0f, 1.0f, 1.0f));

    //! destructor
    virtual ~CAnimatedMeshSceneNode();

    //! sets the current frame. from now on the animation is played from this frame.
    virtual void setCurrentFrame(float frame) override;

    //! frame
    virtual void OnRegisterSceneNode(video::RenderPassHandle pass) override;

    //! OnAnimate() is called just before rendering the whole scene.
    virtual void OnAnimate(std::uint32_t timeMs) override;

    //! renders the node.
    virtual void render() override;

    //! returns the axis aligned bounding box of this node
    virtual const core::aabbox3d<float>& getBoundingBox() const override;

    //! sets the frames between the animation is looped.
    //! the default is 0 - MaximalFrameCount of the mesh.
    //! NOTE: setMesh will also change this value and set it to the full range of animations of the mesh
    virtual bool setFrameLoop(std::int32_t begin, std::int32_t end) override;

    //! Sets looping mode which is on by default. If set to false,
    //! animations will not be looped.
    virtual void setLoopMode(bool playAnimationLooped) override;

    //! returns the current loop mode
    virtual bool getLoopMode() const override;

    //! Sets a callback interface which will be called if an animation
    //! playback has ended. Set this to 0 to disable the callback again.
    virtual void setAnimationEndCallback(IAnimationEndCallBack* callback= 0) override;

    //! sets the speed with which the animation is played
    //! NOTE: setMesh will also change this value and set it to the default speed of the mesh
    virtual void setAnimationSpeed(float framesPerSecond) override;

    //! gets the speed with which the animation is played
    virtual float getAnimationSpeed() const override;

    //! returns the material based on the zero based index i. To get the amount
    //! of materials used by this scene node, use getMaterialCount().
    //! This function is needed for inserting the node into the scene hirachy on a
    //! optimal position for minimizing renderstate changes, but can also be used
    //! to directly modify the material of a scene node.
    // virtual video::SMaterial& getMaterial(std::uint32_t i) override;

    //! returns amount of materials used by this scene node.
    virtual std::uint32_t getMaterialCount() const override;

    //! Creates shadow volume scene node as child of this node
    //! and returns a pointer to it.
    virtual IShadowVolumeSceneNode* addShadowVolumeSceneNode(const IMesh* shadowMesh,
      std::int32_t id, bool zfailmethod=true, float infinity=1000.0f) override;

    //! Returns a pointer to a child node, which has the same transformation as
    //! the corrsesponding joint, if the mesh in this scene node is a skinned mesh.
    virtual IBoneSceneNode* getJointNode(const char* jointName) override;

    //! same as getJointNode(const char* jointName), but based on id
    virtual IBoneSceneNode* getJointNode(std::uint32_t jointID) override;

    //! Gets joint count.
    virtual std::uint32_t getJointCount() const override;

    //! Removes a child from this scene node.
    //! Implemented here, to be able to remove the shadow properly, if there is one,
    //! or to remove attached childs.
    virtual bool removeChild(ISceneNode* child) override;

    //! Starts a MD2 animation.
    // virtual bool setMD2Animation(EMD2_ANIMATION_TYPE anim) override;

    //! Starts a special MD2 animation.
    // virtual bool setMD2Animation(const char* animationName) override;

    //! Returns the current displayed frame number.
    virtual float getFrameNr() const override;
    //! Returns the current start frame number.
    virtual std::int32_t getStartFrame() const override;
    //! Returns the current end frame number.
    virtual std::int32_t getEndFrame() const override;

    //! Sets if the scene node should not copy the materials of the mesh but use them in a read only style.
    /* In this way it is possible to change the materials a mesh causing all mesh scene nodes
    referencing this mesh to change too. */
    virtual void setReadOnlyMaterials(bool readonly) override;

    //! Returns if the scene node should not copy the materials of the mesh but use them in a read only style
    virtual bool isReadOnlyMaterials() const override;

    //! Sets a new mesh
    virtual void setMesh(IAnimatedMesh* mesh) override;

    //! Returns the current mesh
    virtual IAnimatedMesh* getMesh(void) override { return Mesh; }

    //! Writes attributes of the scene node.
    virtual void serializeAttributes(io::IAttributes* out, io::SAttributeReadWriteOptions* options= 0) const override;

    //! Reads attributes of the scene node.
    virtual void deserializeAttributes(io::IAttributes* in, io::SAttributeReadWriteOptions* options= 0) override;

    //! Returns type of the scene node
    virtual E_SCENE_NODE_TYPE getType() const override { return E_SCENE_NODE_TYPE::ANIMATED_MESH; }

    // returns the absolute transformation for a special MD3 Tag if the mesh is a md3 mesh,
    // or the absolutetransformation if it's a normal scenenode
    // const SMD3QuaternionTag* getMD3TagTransformation(const std::string & tagname) override;

    //! updates the absolute position based on the relative and the parents position
    virtual void updateAbsolutePosition() override;


    //! Set the joint update mode (0-unused, 1-get joints only, 2-set joints only, 3-move and set)
    virtual void setJointMode(E_JOINT_UPDATE_ON_RENDER mode) override;

    //! Sets the transition time in seconds (note: This needs to enable joints, and setJointmode maybe set to 2)
    //! you must call animateJoints(), or the mesh will not animate
    virtual void setTransitionTime(float Time) override;

    //! updates the joint positions of this mesh
    virtual void animateJoints(bool CalculateAbsolutePositions=true) override;

    //! render mesh ignoring its transformation. Used with ragdolls. (culling is unaffected)
    virtual void setRenderFromIdentity(bool On) override;

    //! Creates a clone of this scene node and its children.
    /** \param newParent An optional new parent.
    \param newManager An optional new scene manager.
    \return The newly created clone of this node. */
    virtual ISceneNode* clone(ISceneNode* newParent= 0, ISceneManager* newManager= 0) override;

  private:

    //! Get a static mesh for the current frame of this animated mesh
    IMesh* getMeshForCurrentFrame();

    void buildFrameNr(std::uint32_t timeMs);
    void checkJoints();
    void beginTransition();

    std::vector<video::SMaterial> Materials;
    core::aabbox3d<float> Box;
    IAnimatedMesh* Mesh;

    std::int32_t StartFrame;
    std::int32_t EndFrame;
    float FramesPerSecond;
    float CurrentFrameNr;

    std::uint32_t LastTimeMs;
    std::uint32_t TransitionTime; //Transition time in millisecs
    float Transiting; //is mesh transiting (plus cache of TransitionTime)
    float TransitingBlend; //0-1, calculated on buildFrameNr

    //0-unused, 1-get joints only, 2-set joints only, 3-move and set
    E_JOINT_UPDATE_ON_RENDER JointMode;
    bool JointsUsed;

    bool Looping;
    bool RenderFromIdentity;

    IAnimationEndCallBack* LoopCallBack;
    std::vector<IBoneSceneNode* > JointChildSceneNodes;
    std::vector<glm::mat4> PretransitingSave;
  };

} // namespace scene
} // namespace saga

#endif

