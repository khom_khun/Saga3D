// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

// The code for the TerrainTriangleSelector is based on the GeoMipMapSelector
// developed by Spintz. He made it available for Irrlicht and allowed it to be
// distributed under this licence. I only modified some parts. A lot of thanks go to him.

#ifndef __C_TERRAIN_TRIANGLE_SELECTOR_H__
#define __C_TERRAIN_TRIANGLE_SELECTOR_H__

#include "ITriangleSelector.h"


namespace saga
{
namespace scene
{

class ITerrainSceneNode;

//! Triangle Selector for the TerrainSceneNode
/** The code for the TerrainTriangleSelector is based on the GeoMipMapSelector
developed by Spintz. He made it available for Irrlicht and allowed it to be
distributed under this license. I only modified some parts. A lot of thanks go
to him.
*/
class CTerrainTriangleSelector : public ITriangleSelector
{
public:

  //! Constructs a selector based on an ITerrainSceneNode
  CTerrainTriangleSelector(ITerrainSceneNode* node, std::int32_t LOD);

  //! Destructor
  virtual ~CTerrainTriangleSelector();

  //! Clears and sets triangle data
  virtual void setTriangleData(ITerrainSceneNode* node, std::int32_t LOD);

  //! Gets all triangles.
  void getTriangles(core::triangle3df* triangles, std::int32_t arraySize, std::int32_t& outTriangleCount,
    const glm::mat4* transform, bool useNodeTransform,
    std::vector<SCollisionTriangleRange>* outTriangleInfo) const override;

  //! Gets all triangles which lie within a specific bounding box.
  void getTriangles(core::triangle3df* triangles, std::int32_t arraySize, std::int32_t& outTriangleCount,
    const core::aabbox3d<float>& box, const glm::mat4* transform, bool useNodeTransform,
    std::vector<SCollisionTriangleRange>* outTriangleInfo) const override;

  //! Gets all triangles which have or may have contact with a 3d line.
  virtual void getTriangles(core::triangle3df* triangles, std::int32_t arraySize,
    std::int32_t& outTriangleCount, const core::line3d<float>& line,
    const glm::mat4* transform, bool useNodeTransform,
    std::vector<SCollisionTriangleRange>* outTriangleInfo) const override;

  //! Returns amount of all available triangles in this selector
  virtual std::int32_t getTriangleCount() const override;

  //! Return the scene node associated with a given triangle.
  virtual ISceneNode* getSceneNodeForTriangle(std::uint32_t triangleIndex) const override;

  // Get the number of TriangleSelectors that are part of this one
  virtual std::uint32_t getSelectorCount() const override;

  // Get the TriangleSelector based on index based on getSelectorCount
  virtual ITriangleSelector* getSelector(std::uint32_t index) override;

  // Get the TriangleSelector based on index based on getSelectorCount
  virtual const ITriangleSelector* getSelector(std::uint32_t index) const override;

private:

  friend class CTerrainSceneNode;

  struct SGeoMipMapTrianglePatch
  {
    std::vector<core::triangle3df> Triangles;
    std::int32_t NumTriangles;
    core::aabbox3df Box;
  };

  struct SGeoMipMapTrianglePatches
  {
    SGeoMipMapTrianglePatches() :
      NumPatches(0), TotalTriangles(0)
    {
    }

    std::vector<SGeoMipMapTrianglePatch> TrianglePatchArray;
    std::int32_t NumPatches;
    std::uint32_t TotalTriangles;
  };

  ITerrainSceneNode* SceneNode;
  SGeoMipMapTrianglePatches TrianglePatches;
};

} // namespace scene
} // namespace saga


#endif // __C_TERRAIN_TRIANGLE_SELECTOR_H__
