// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_PARTICLE_SYSTEM_SCENE_NODE_H_INCLUDED__
#define __C_PARTICLE_SYSTEM_SCENE_NODE_H_INCLUDED__


#ifdef _IRR_COMPILE_WITH_PARTICLES_

#include "IParticleSystemSceneNode.h"

#include "irrList.h"
#include "SMeshBuffer.h"

namespace saga
{
namespace scene
{

//! A particle system scene node.
/** A scene node controlling a particle system. The behavior of the particles
can be controlled by setting the right particle emitters and affectors.
*/
class CParticleSystemSceneNode : public IParticleSystemSceneNode
{
public:

  //! constructor
  CParticleSystemSceneNode(bool createDefaultEmitter,
    ISceneNode* parent, ISceneManager* mgr, std::int32_t id,
    const glm::vec3& position,
    const glm::vec3& rotation,
    const glm::vec3& scale);

  //! destructor
  virtual ~CParticleSystemSceneNode();

  //! Gets the particle emitter, which creates the particles.
  virtual IParticleEmitter* getEmitter() override;

  //! Sets the particle emitter, which creates the particles.
  virtual void setEmitter(IParticleEmitter* emitter) override;

  //! Adds new particle affector to the particle system.
  virtual void addAffector(IParticleAffector* affector) override;

  //! Get a list of all particle affectors.
  virtual const core::list<IParticleAffector*>& getAffectors() const override;

  //! Removes all particle affectors in the particle system.
  virtual void removeAllAffectors() override;

  //! Returns the material based on the zero based index i.
  // virtual video::SMaterial& getMaterial(std::uint32_t i) override;

  //! Returns amount of materials used by this scene node.
  virtual std::uint32_t getMaterialCount() const override;

  //! pre render event
  virtual void OnRegisterSceneNode(video::RenderPassHandle pass) override;

  //! render
  virtual void render() override;

  //! returns the axis aligned bounding box of this node
  virtual const core::aabbox3d<float>& getBoundingBox() const override;

  //! Creates a particle emitter for an animated mesh scene node
  virtual IParticleAnimatedMeshSceneNodeEmitter* createAnimatedMeshSceneNodeEmitter(
    scene::IAnimatedMeshSceneNode* node, bool useNormalDirection = true,
    const glm::vec3& direction = glm::vec3(0.0f,0.03f,0.0f),
    float normalDirectionModifier = 100.0f, std::int32_t mbNumber = -1,
    bool everyMeshVertex = false, std::uint32_t minParticlesPerSecond = 5,
    std::uint32_t maxParticlesPerSecond = 10,
    const video::SColor& minStartColor = video::SColor(255,0,0,0),
    const video::SColor& maxStartColor = video::SColor(255,255,255,255),
    std::uint32_t lifeTimeMin = 2000, std::uint32_t lifeTimeMax = 4000,
    std::int32_t maxAngleDegrees = 0,
    const glm::vec2& minStartSize = glm::vec2(5.0f,5.0f),
    const glm::vec2& maxStartSize = glm::vec2(5.0f,5.0f)) override;

  //! Creates a box particle emitter.
  virtual IParticleBoxEmitter* createBoxEmitter(
    const core::aabbox3df& box = core::aabbox3d<float>(-10,0,-10,5,30,10),
    const glm::vec3& direction = glm::vec3(0.0f,0.03f,0.0f),
    std::uint32_t minParticlesPerSecond = 5,
    std::uint32_t maxParticlesPerSecond = 10,
    const video::SColor& minStartColor = video::SColor(255,0,0,0),
    const video::SColor& maxStartColor = video::SColor(255,255,255,255),
    std::uint32_t lifeTimeMin=2000, std::uint32_t lifeTimeMax=4000,
    std::int32_t maxAngleDegrees= 0,
    const glm::vec2& minStartSize = glm::vec2(5.0f,5.0f),
    const glm::vec2& maxStartSize = glm::vec2(5.0f,5.0f)) override;

  //! Creates a particle emitter for emitting from a cylinder
  virtual IParticleCylinderEmitter* createCylinderEmitter(
    const glm::vec3& center, float radius,
    const glm::vec3& normal, float length,
    bool outlineOnly = false, const glm::vec3& direction = glm::vec3(0.0f,0.5f,0.0f),
    std::uint32_t minParticlesPerSecond = 5, std::uint32_t maxParticlesPersSecond = 10,
    const video::SColor& minStartColor = video::SColor(255,0,0,0),
    const video::SColor& maxStartColor = video::SColor(255,255,255,255),
    std::uint32_t lifeTimeMin = 2000, std::uint32_t lifeTimeMax = 4000,
    std::int32_t maxAngleDegrees = 0,
    const glm::vec2& minStartSize = glm::vec2(5.0f,5.0f),
    const glm::vec2& maxStartSize = glm::vec2(5.0f,5.0f)) override;

  //! Creates a mesh particle emitter.
  virtual IParticleMeshEmitter* createMeshEmitter(
    scene::IMesh* mesh, bool useNormalDirection = true,
    const glm::vec3& direction = glm::vec3(0.0f,0.03f,0.0f),
    float normalDirectionModifier = 100.0f, std::int32_t mbNumber = -1,
    bool everyMeshVertex = false,
    std::uint32_t minParticlesPerSecond = 5,
    std::uint32_t maxParticlesPerSecond = 10,
    const video::SColor& minStartColor = video::SColor(255,0,0,0),
    const video::SColor& maxStartColor = video::SColor(255,255,255,255),
    std::uint32_t lifeTimeMin = 2000, std::uint32_t lifeTimeMax = 4000,
    std::int32_t maxAngleDegrees = 0,
    const glm::vec2& minStartSize = glm::vec2(5.0f,5.0f),
    const glm::vec2& maxStartSize = glm::vec2(5.0f,5.0f)) override;

  //! Creates a point particle emitter.
  virtual IParticlePointEmitter* createPointEmitter(
    const glm::vec3& direction = glm::vec3(0.0f,0.03f,0.0f),
    std::uint32_t minParticlesPerSecond = 5,
    std::uint32_t maxParticlesPerSecond = 10,
    const video::SColor& minStartColor = video::SColor(255,0,0,0),
    const video::SColor& maxStartColor = video::SColor(255,255,255,255),
    std::uint32_t lifeTimeMin=2000, std::uint32_t lifeTimeMax=4000,
    std::int32_t maxAngleDegrees= 0,
    const glm::vec2& minStartSize = glm::vec2(5.0f,5.0f),
    const glm::vec2& maxStartSize = glm::vec2(5.0f,5.0f)) override;

  //! Creates a ring particle emitter.
  virtual IParticleRingEmitter* createRingEmitter(
    const glm::vec3& center, float radius, float ringThickness,
    const glm::vec3& direction = glm::vec3(0.0f,0.03f,0.0f),
    std::uint32_t minParticlesPerSecond = 5,
    std::uint32_t maxParticlesPerSecond = 10,
    const video::SColor& minStartColor = video::SColor(255,0,0,0),
    const video::SColor& maxStartColor = video::SColor(255,255,255,255),
    std::uint32_t lifeTimeMin=2000, std::uint32_t lifeTimeMax=4000,
    std::int32_t maxAngleDegrees= 0,
    const glm::vec2& minStartSize = glm::vec2(5.0f,5.0f),
    const glm::vec2& maxStartSize = glm::vec2(5.0f,5.0f)) override;

  //! Creates a sphere particle emitter.
  virtual IParticleSphereEmitter* createSphereEmitter(
    const glm::vec3& center, float radius,
    const glm::vec3& direction = glm::vec3(0.0f,0.03f,0.0f),
    std::uint32_t minParticlesPerSecond = 5,
    std::uint32_t maxParticlesPerSecond = 10,
    const video::SColor& minStartColor = video::SColor(255,0,0,0),
    const video::SColor& maxStartColor = video::SColor(255,255,255,255),
    std::uint32_t lifeTimeMin=2000, std::uint32_t lifeTimeMax=4000,
    std::int32_t maxAngleDegrees= 0,
    const glm::vec2& minStartSize = glm::vec2(5.0f,5.0f),
    const glm::vec2& maxStartSize = glm::vec2(5.0f,5.0f)) override;

  //! Creates a point attraction affector. This affector modifies the positions of the
  //! particles and attracts them to a specified point at a specified speed per second.
  virtual IParticleAttractionAffector* createAttractionAffector(
    const glm::vec3& point, float speed = 1.0f, bool attract = true,
    bool affectX = true, bool affectY = true, bool affectZ = true) override;

  //! Creates a scale particle affector.
  virtual IParticleAffector* createScaleParticleAffector(
    const glm::vec2& scaleTo = glm::vec2(1.0f, 1.0f)) override;

  //! Creates a fade out particle affector.
  virtual IParticleFadeOutAffector* createFadeOutParticleAffector(
    const video::SColor& targetColor = video::SColor(0,0,0,0),
    std::uint32_t timeNeededToFadeOut = 1000) override;

  //! Creates a gravity affector.
  virtual IParticleGravityAffector* createGravityAffector(
    const glm::vec3& gravity = glm::vec3(0.0f,-0.03f,0.0f),
    std::uint32_t timeForceLost = 1000) override;

  //! Creates a rotation affector. This affector rotates the particles
  //! around a specified pivot point. The speed is in Degrees per second.
  virtual IParticleRotationAffector* createRotationAffector(
    const glm::vec3& speed = glm::vec3(5.0f,5.0f,5.0f),
    const glm::vec3& pivotPoint = glm::vec3(0.0f,0.0f,0.0f)) override;

  //! Sets the size of all particles.
  virtual void setParticleSize(
    const glm::vec2 &size = glm::vec2(5.0f, 5.0f)) override;

  //! Sets if the particles should be global. If they are, the particles are affected by
  //! the movement of the particle system scene node too, otherwise they completely
  //! ignore it. Default is true.
  virtual void setParticlesAreGlobal(bool global=true) override;

  //! Remove all currently visible particles
  virtual void clearParticles() override;

  //! Sets if the node should be visible or not.
  virtual void setVisible(bool isVisible) override;

  //! Do manually update the particles.
  //! This should only be called when you want to render the node outside the scenegraph,
  //! as the node will care about this otherwise automatically.
  virtual void doParticleSystem(std::uint32_t time) override;

  //! Writes attributes of the scene node.
  virtual void serializeAttributes(io::IAttributes* out, io::SAttributeReadWriteOptions* options= 0) const override;

  //! Reads attributes of the scene node.
  virtual void deserializeAttributes(io::IAttributes* in, io::SAttributeReadWriteOptions* options= 0) override;

  //! Returns type of the scene node
  virtual E_SCENE_NODE_TYPE getType() const override { return E_SCENE_NODE_TYPE::PARTICLE_SYSTEM; }

private:

  void reallocateBuffers();

  core::list<IParticleAffector*> AffectorList;
  IParticleEmitter* Emitter;
  std::vector<SParticle> Particles;
  glm::vec2 ParticleSize;
  std::uint32_t LastEmitTime;
  glm::mat4 LastAbsoluteTransformation;

  SMeshBuffer* Buffer;

// TODO: That was obviously planned by someone at some point and sounds like a good idea.
// But seems it was never implemented.
//  enum E_PARTICLES_PRIMITIVE
//  {
//    EPP_POINT= 0,
//    EPP_BILLBOARD,
//    EPP_POINTSPRITE
//  };
//  E_PARTICLES_PRIMITIVE ParticlePrimitive;

  bool ParticlesAreGlobal;
};

} // namespace scene
} // namespace saga

#endif // _IRR_COMPILE_WITH_PARTICLES_

#endif

