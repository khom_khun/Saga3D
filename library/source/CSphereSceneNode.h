// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_SPHERE_SCENE_NODE_H_INCLUDED__
#define __C_SPHERE_SCENE_NODE_H_INCLUDED__

#include "IMeshSceneNode.h"
#include "IMesh.h"

namespace saga
{
namespace scene
{
  class CSphereSceneNode : public IMeshSceneNode
  {
  public:

    //! constructor
    CSphereSceneNode(float size, std::uint32_t polyCountX, std::uint32_t polyCountY, ISceneNode* parent, ISceneManager* mgr, std::int32_t id,
      const glm::vec3& position = glm::vec3(0,0,0),
      const glm::vec3& rotation = glm::vec3(0,0,0),
      const glm::vec3& scale = glm::vec3(1.0f, 1.0f, 1.0f));

    //! destructor
    virtual ~CSphereSceneNode();

    virtual void OnRegisterSceneNode(video::RenderPassHandle pass) override;

    //! renders the node.
    virtual void render() override;

    //! returns the axis aligned bounding box of this node
    virtual const core::aabbox3d<float>& getBoundingBox() const override;

    //! returns the material based on the zero based index i. To get the amount
    //! of materials used by this scene node, use getMaterialCount().
    //! This function is needed for inserting the node into the scene hirachy on a
    //! optimal position for minimizing renderstate changes, but can also be used
    //! to directly modify the material of a scene node.
    // virtual video::SMaterial& getMaterial(std::uint32_t i) override;

    //! returns amount of materials used by this scene node.
    virtual std::uint32_t getMaterialCount() const override;

    //! Returns type of the scene node
    virtual E_SCENE_NODE_TYPE getType() const override { return E_SCENE_NODE_TYPE::SPHERE; }

    //! Writes attributes of the scene node.
    virtual void serializeAttributes(io::IAttributes* out, io::SAttributeReadWriteOptions* options= 0) const override;

    //! Reads attributes of the scene node.
    virtual void deserializeAttributes(io::IAttributes* in, io::SAttributeReadWriteOptions* options= 0) override;

    //! Creates a clone of this scene node and its children.
    virtual ISceneNode* clone(ISceneNode* newParent= 0, ISceneManager* newManager= 0) override;

    //! The mesh cannot be changed
    virtual void setMesh(IMesh* mesh) override {}

    //! Returns the current mesh
    virtual IMesh* getMesh() override { return Mesh; }

    //! Sets if the scene node should not copy the materials of the mesh but use them in a read only style.
    /* In this way it is possible to change the materials a mesh causing all mesh scene nodes
    referencing this mesh to change too. */
    virtual void setReadOnlyMaterials(bool readonly) override {}

    //! Returns if the scene node should not copy the materials of the mesh but use them in a read only style
    virtual bool isReadOnlyMaterials() const override { return false; }

    //! Creates shadow volume scene node as child of this node
    //! and returns a pointer to it.
    virtual IShadowVolumeSceneNode* addShadowVolumeSceneNode(const IMesh* shadowMesh,
      std::int32_t id, bool zfailmethod=true, float infinity=10000.0f) override;

    //! Removes a child from this scene node.
    //! Implemented here, to be able to remove the shadow properly, if there is one,
    //! or to remove attached child.
    virtual bool removeChild(ISceneNode* child) override;

  private:

    IMesh* Mesh;
    IShadowVolumeSceneNode* Shadow;
    core::aabbox3d<float> Box;
    float Radius;
    std::uint32_t PolyCountX;
    std::uint32_t PolyCountY;
  };

} // namespace scene
} // namespace saga

#endif

