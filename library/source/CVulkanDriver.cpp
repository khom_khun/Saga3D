#include "CVulkanDriver.h"
#include "SRenderPassState.h"
#include "SRenderPass.h"
#include "CSceneManager.h"
#include "IMeshSceneNode.h"
#include "IMeshBuffer.h"
#include "ICameraSceneNode.h"
#include <CIrrDeviceSDL.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_syswm.h>
#include <Core/Swapchain.h>
#include <glm/gtc/type_ptr.hpp>
#include <algorithm>

#ifdef _WIN32
#define PLATFORM_SURFACE_EXTENSION_NAME VK_KHR_WIN32_SURFACE_EXTENSION_NAME
#define PlatformSurfaceCreateInfo VkWin32SurfaceCreateInfoKHR
#define PLATFORM_SURFACE_CREATE_INFO VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
#define PLATFORM_ASSIGN_SURFACE_INFO \
  surfaceCreateInfo.hinstance = GetModuleHandle(NULL); \
  surfaceCreateInfo.hwnd = info.info.win.window
#define PlatformCreateSurface vkCreateWin32SurfaceKHR
#elif defined __linux__ 
  #ifdef __ANDROID__
  #define PLATFORM_SURFACE_EXTENSION_NAME VK_KHR_ANDROID_SURFACE_EXTENSION_NAME
  #define PlatformSurfaceCreateInfo VkAndroidSurfaceCreateInfoKHR
  #define PLATFORM_SURFACE_CREATE_INFO VK_STRUCTURE_TYPE_ANDROID_SURFACE_CREATE_INFO_KHR;
  #define PLATFORM_ASSIGN_SURFACE_INFO \
    surfaceCreateInfo.window = info.info.android.window
  #define PlatformCreateSurface vkCreateAndroidSurfaceKHR
#else
  #define PLATFORM_SURFACE_EXTENSION_NAME VK_KHR_XLIB_SURFACE_EXTENSION_NAME
  #define PlatformSurfaceCreateInfo VkXlibSurfaceCreateInfoKHR
  #define PLATFORM_SURFACE_CREATE_INFO VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR;
  #define PLATFORM_ASSIGN_SURFACE_INFO \
    surfaceCreateInfo.dpy = info.info.x11.display; \
    surfaceCreateInfo.window = info.info.x11.window
  #define PlatformCreateSurface vkCreateXlibSurfaceKHR
#endif
#endif

namespace saga
{
namespace video
{

CVulkanDriver::CVulkanDriver(const SIrrlichtCreationParameters& params, CIrrDeviceSDL& device)
  :  CVideoDriver(params), Device(device)
{

}

std::uint32_t CVulkanDriver::getWidth() const
{
  return Device.getWidth();
}

std::uint32_t CVulkanDriver::getHeight() const
{
  return Device.getHeight();
}

CVulkanDriver::~CVulkanDriver()
{
  //TODO: more vulkan cleanup
  // Wait for all device operations to complete.
  vezDeviceWaitIdle(VKDevice);
  
  // Call application's Cleanup method.
  // Cleanup();
  
  // Destroy framebuffer.
  if (ManageFramebuffer)
  {
    if (DefaultFramebuffer.handle)
    {
      vezDestroyFramebuffer(VKDevice, DefaultFramebuffer.handle);
      vezDestroyImageView(VKDevice, DefaultFramebuffer.colorImageView);
      vezDestroyImageView(VKDevice, DefaultFramebuffer.depthStencilImageView);
      vezDestroyImage(VKDevice, DefaultFramebuffer.colorImage);
      vezDestroyImage(VKDevice, DefaultFramebuffer.depthStencilImage);
    }
  }

  // Destroy the swapchain.
  vezDestroySwapchain(VKDevice, Swapchain);

  // Destroy device.
  vezDestroyDevice(VKDevice);

  // Destroy surface.
  vkDestroySurfaceKHR(Instance, Surface, nullptr);

  // Destroy instance.
  vezDestroyInstance(Instance);
}

bool CVulkanDriver::initDriver()
{
  // Enumerate all available instance layers.
  uint32_t layerCount = 0;
  vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

  std::vector<VkLayerProperties> layerProperties(layerCount);
  vkEnumerateInstanceLayerProperties(&layerCount, layerProperties.data());

  bool standardValidationFound = false;
  for (auto prop : layerProperties)
  {
    if (std::string(prop.layerName) == "VK_LAYER_LUNARG_standard_validation")
    {
      standardValidationFound = true;
      break;
    }
  }

  // TODO: check and load extensions
  // Initialize a Vulkan instance with the validation layers enabled and extensions required by glfw.
  constexpr uint32_t instanceExtensionCount = 2;
  const char* instanceExtensions[] =
  {
    "VK_KHR_surface",
    #ifdef __linux__
      #ifdef __ANDROID__
        "VK_KHR_android_surface"
      #else
        "VK_KHR_xlib_surface"
      #endif
    #elif defined _WIN32
      "VK_KHR_win32_surface"
    #endif
  };

  std::vector<const char*> instanceLayers;
  if (EnableValidationLayers && standardValidationFound)
    instanceLayers.push_back("VK_LAYER_LUNARG_standard_validation");
  
  VezApplicationInfo appInfo = { nullptr, "Saga3D", VK_MAKE_VERSION(1, 0, 0), "", VK_MAKE_VERSION(0, 0, 0) };

  VezInstanceCreateInfo createInfo = {
    nullptr, &appInfo, static_cast<uint32_t>(instanceLayers.size()),
    instanceLayers.data(),
    instanceExtensionCount, instanceExtensions
  };

  auto result = vezCreateInstance(&createInfo, &Instance);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vkCreateInstance failed", result);
    return false;
  }

  // Enumerate all attached physical devices.
  uint32_t physicalDeviceCount = 0;
  vezEnumeratePhysicalDevices(Instance, &physicalDeviceCount, nullptr);
  if (physicalDeviceCount == 0)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "No Vulkan physical devices found");
    return false;
  }

  std::vector<VkPhysicalDevice> physicalDevices(physicalDeviceCount);
  vezEnumeratePhysicalDevices(Instance, &physicalDeviceCount, physicalDevices.data());

  for (auto pd : physicalDevices)
  {
    VkPhysicalDeviceProperties properties = {};
    vezGetPhysicalDeviceProperties(pd, &properties);
    #ifndef __ANDROID__
    if (properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
    {
      PhysicalDevice = pd;
      break;
    }
    #else
    if (properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU)
    {
      PhysicalDevice = pd;
      break;
    }
    #endif
  }

  if (PhysicalDevice == VK_NULL_HANDLE)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "No discrete GPU found");
    return false;
  }

  vezGetPhysicalDeviceFeatures(PhysicalDevice, &DeviceFeatures);

  // Get the physical device information.
  VkPhysicalDeviceProperties properties = {};
  vezGetPhysicalDeviceProperties(PhysicalDevice, &properties);
  // std::cout << "Selected device: " << properties.deviceName << "\n";
  Name = properties.deviceName;

  // TODO: createSDL Surface
  SDL_Window* window = Device.getSDLWindow();
  SDL_SysWMinfo info;
  SDL_VERSION(&info.version);
  SDL_GetWindowWMInfo(window, &info);

  // Create Vulkan surface
  PlatformSurfaceCreateInfo surfaceCreateInfo;
  surfaceCreateInfo.sType = PLATFORM_SURFACE_CREATE_INFO;
  surfaceCreateInfo.pNext = nullptr;
  surfaceCreateInfo.flags = 0;
  PLATFORM_ASSIGN_SURFACE_INFO;

  result = PlatformCreateSurface(Instance, &surfaceCreateInfo, nullptr, &Surface);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "Platform surface creation failed");
    return false;
  }

  // Create the Vulkan device handle.
  std::vector<const char*> deviceExtensions = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };
  for (auto i = 0U; i < DeviceExtensions.size(); ++i)
    deviceExtensions.push_back(DeviceExtensions[i].c_str());
          
  VezDeviceCreateInfo deviceCreateInfo = { nullptr, 0, nullptr, static_cast<uint32_t>(deviceExtensions.size()), deviceExtensions.data() };
  result = vezCreateDevice(PhysicalDevice, &deviceCreateInfo, &VKDevice);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezCreateDevice failed");
    return false;
  }

  // Create the swapchain.
  VezSwapchainCreateInfo swapchainCreateInfo = {};
  swapchainCreateInfo.surface = Surface;
  swapchainCreateInfo.format = { VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
  swapchainCreateInfo.tripleBuffer = VK_TRUE;
  result = vezCreateSwapchain(VKDevice, &swapchainCreateInfo, &Swapchain);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezCreateSwapchain failed");
    return false;
  }

  auto vkSwapchain = reinterpret_cast<vez::Swapchain*>(Swapchain)->GetHandle();

  uint32_t actualImageCount = 0;
  if (vkGetSwapchainImagesKHR(VKDevice, vkSwapchain, &actualImageCount, nullptr) != VK_SUCCESS || actualImageCount == 0) {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "Failed to acquire number of swap chain images");
    exit(1);
  }

  SwapchainImages.resize(actualImageCount);

  if (vkGetSwapchainImagesKHR(VKDevice, vkSwapchain, &actualImageCount, SwapchainImages.data()) != VK_SUCCESS) {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "Failed to acquire swap chain images");
    exit(1);
  }

  // vkGetPhysicalDeviceMemoryProperties(PhysicalDevice, &MemoryProperties);

  // Create framebuffer.
  if (ManageFramebuffer)
    createFramebuffer();
  createCommandBuffer();

  return true;
}

void CVulkanDriver::createFramebuffer()
{
  // Free previous allocations.
  if (DefaultFramebuffer.handle)
  {
    vezDestroyFramebuffer(VKDevice, DefaultFramebuffer.handle);
    vezDestroyImageView(VKDevice, DefaultFramebuffer.colorImageView);
    vezDestroyImageView(VKDevice, DefaultFramebuffer.depthStencilImageView);
    vezDestroyImage(VKDevice, DefaultFramebuffer.colorImage);
    vezDestroyImage(VKDevice, DefaultFramebuffer.depthStencilImage);
  }

  // Get the current window dimension.
  auto width = Device.getWidth();
  auto height = Device.getHeight();

  // Get the swapchain's current surface format.
  VkSurfaceFormatKHR swapchainFormat = {};
  vezGetSwapchainSurfaceFormat(Swapchain, &swapchainFormat);

  // Create the color image for the Framebuffer.
  VezImageCreateInfo imageCreateInfo = {};
  imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
  imageCreateInfo.format = swapchainFormat.format;
  imageCreateInfo.extent = { static_cast<uint32_t>(width), static_cast<uint32_t>(height), 1 };
  imageCreateInfo.mipLevels = 1;
  imageCreateInfo.arrayLayers = 1;
  imageCreateInfo.samples = SampleCountFlag;
  imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
  imageCreateInfo.usage = VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
  auto result = vezCreateImage(VKDevice, VEZ_MEMORY_GPU_ONLY, &imageCreateInfo, &DefaultFramebuffer.colorImage);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vkCreateImage failed %d", result);
    return;
  }

  // Create the image view for binding the texture as a resource.
  VezImageViewCreateInfo imageViewCreateInfo = {};
  imageViewCreateInfo.image = DefaultFramebuffer.colorImage;
  imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
  imageViewCreateInfo.format = imageCreateInfo.format;
  imageViewCreateInfo.subresourceRange.layerCount = 1;
  imageViewCreateInfo.subresourceRange.levelCount = 1;
  result = vezCreateImageView(VKDevice, &imageViewCreateInfo, &DefaultFramebuffer.colorImageView);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vkCreateImageView failed %d", result);
    return;
  }

  // Create the depth image for the Framebuffer.
  imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
  imageCreateInfo.format = VK_FORMAT_D32_SFLOAT;
  imageCreateInfo.extent = { static_cast<uint32_t>(width), static_cast<uint32_t>(height), 1 };
  imageCreateInfo.mipLevels = 1;
  imageCreateInfo.arrayLayers = 1;
  imageCreateInfo.samples = SampleCountFlag;
  imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
  imageCreateInfo.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
  result = vezCreateImage(VKDevice, VEZ_MEMORY_GPU_ONLY, &imageCreateInfo, &DefaultFramebuffer.depthStencilImage);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vkCreateImage failed %d", result);
    return;
  }

  // Create the image view for binding the texture as a resource.
  imageViewCreateInfo.image = DefaultFramebuffer.depthStencilImage;
  imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
  imageViewCreateInfo.format = imageCreateInfo.format;
  imageViewCreateInfo.subresourceRange.layerCount = 1;
  imageViewCreateInfo.subresourceRange.levelCount = 1;
  result = vezCreateImageView(VKDevice, &imageViewCreateInfo, &DefaultFramebuffer.depthStencilImageView);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vkCreateImageView failed %d", result);
    return;
  }

  // Create the Framebuffer.
  std::array<VkImageView, 2> attachments = { DefaultFramebuffer.colorImageView, DefaultFramebuffer.depthStencilImageView };
  VezFramebufferCreateInfo framebufferCreateInfo = {};
  framebufferCreateInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
  framebufferCreateInfo.pAttachments = attachments.data();
  framebufferCreateInfo.width = width;
  framebufferCreateInfo.height = height;
  framebufferCreateInfo.layers = 1;
  result = vezCreateFramebuffer(VKDevice, &framebufferCreateInfo, &DefaultFramebuffer.handle);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vkCreateFramebuffer failed %d", result);
    return;
  }
}

void CVulkanDriver::createCommandBuffer()
{
  // Get the graphics queue handle.
  vezGetDeviceGraphicsQueue(VKDevice, 0, &GraphicsQueue);

  // Create a command buffer handle.
  VezCommandBufferAllocateInfo allocInfo = {};
  allocInfo.queue = GraphicsQueue;
  allocInfo.commandBufferCount = 1;
  if (vezAllocateCommandBuffers(VKDevice, &allocInfo, &CommandBuffer) != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezAllocateCommandBuffers failed");
    exit(1);
  }
}

void CVulkanDriver::render()
{
  // auto smgr = std::dynamic_pointer_cast<scene::CSceneManager>(Device.getSceneManager());
  auto& smgr = Device.getSceneManager();
  auto& pipelines = smgr->getPipelineGroupedSceneNodeList(CurrentPass);
  for (auto pipelineIt : pipelines)
  {
    const auto& pipeline = Pipelines.at(pipelineIt.first);
    auto& vulkanPipeline = VkPipelines[pipeline.Handle];
    // Bind the pipeline and associated resources.
    vezCmdBindPipeline(vulkanPipeline.Handle);

    // Set rasterization state.
    VezRasterizationState rasterizationState = {};
    rasterizationState.polygonMode = VK_POLYGON_MODE_FILL;
    rasterizationState.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;

    switch (pipeline.Rasterizer.CullMode)
    {
      case E_CULL_MODE::BACK_FACE: rasterizationState.cullMode = VK_CULL_MODE_BACK_BIT; break;
      case E_CULL_MODE::FRONT_FACE: rasterizationState.cullMode = VK_CULL_MODE_FRONT_BIT; break;
    }
    vezCmdSetRasterizationState(&rasterizationState);

    // Set depth stencil state.
    VezDepthStencilState depthStencilState = {};
    depthStencilState.depthTestEnable = VK_TRUE;
    depthStencilState.depthWriteEnable = pipeline.DepthStencil.EnableDepthWrite == true ? VK_TRUE : VK_FALSE;
    switch (pipeline.DepthStencil.DepthCompareFunc)
    {
      case E_COMPARE_FUNC::ALWAYS: depthStencilState.depthCompareOp = VK_COMPARE_OP_ALWAYS; break;
      case E_COMPARE_FUNC::EQUAL: depthStencilState.depthCompareOp = VK_COMPARE_OP_EQUAL; break;
      case E_COMPARE_FUNC::GREATER: depthStencilState.depthCompareOp = VK_COMPARE_OP_GREATER; break;
      case E_COMPARE_FUNC::GREATER_EQUAL: depthStencilState.depthCompareOp = VK_COMPARE_OP_GREATER_OR_EQUAL; break;
      case E_COMPARE_FUNC::LESS: depthStencilState.depthCompareOp = VK_COMPARE_OP_LESS; break;
      case E_COMPARE_FUNC::LESS_EQUAL: depthStencilState.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL; break;
      case E_COMPARE_FUNC::NEVER: depthStencilState.depthCompareOp = VK_COMPARE_OP_NEVER; break;
      case E_COMPARE_FUNC::NOT_EQUAL: depthStencilState.depthCompareOp = VK_COMPARE_OP_NOT_EQUAL; break;
    }
    vezCmdSetDepthStencilState(&depthStencilState);

    uint32_t i = 0;
    VkDeviceSize offsets[1] = { 0 };
    const auto& pass = RenderPasses.at(CurrentPass);

    if (PassMeshBuffers.empty() == false)
    {
      auto it = std::find_if(PassMeshBuffers.begin(), PassMeshBuffers.end(),
        [this] (const PassMeshBuffer& buffer) {
          return buffer.Pass == CurrentPass;
      });
      const auto MeshBuffers = it->MeshBuffers;

      for (auto& node : pipelineIt.second)
      {
        auto n = std::dynamic_pointer_cast<scene::IMeshSceneNode>(node);
        auto& mesh = n->getMesh();
        auto& meshBuffer = mesh->getMeshBuffer(0);
        const auto& sceneNodeData = MeshBuffers.at(meshBuffer.getID());

        vezCmdBindVertexBuffers(0, 1, &sceneNodeData.VertexBuffer, offsets);
        vezCmdBindIndexBuffer(sceneNodeData.IndexBuffer, 0, VK_INDEX_TYPE_UINT32);

        vezCmdSetVertexInputFormat(vulkanPipeline.VertexInputFormat);
        n->onRender();
        vezCmdDrawIndexed(meshBuffer.getIndexCount(), 1, 0, 0, 0);
      }
    }
    vezCmdNextSubpass();
  }
}

void CVulkanDriver::createGPUMeshBuffer(scene::IMeshBuffer& buffer, const RenderPassHandle pass)
{
  VkBuffer vertexBuffer;
  VezBufferCreateInfo bufferCreateInfo = {};
  bufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
  bufferCreateInfo.size = buffer.getDataSize(pass);
  auto result = vezCreateBuffer(VKDevice, VEZ_MEMORY_GPU_ONLY, &bufferCreateInfo, &vertexBuffer);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezCreateBuffer for vertex buffer error");
    exit(1);
  }

  result = vezBufferSubData(VKDevice, vertexBuffer, 0, bufferCreateInfo.size, buffer.getData(pass));

  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezBufferSubData for vertex buffer error");
    exit(1);
  }

  VkBuffer indexBuffer;
  bufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT;
  bufferCreateInfo.size = sizeof(std::uint32_t) * buffer.getIndexCount();
  result = vezCreateBuffer(VKDevice, VEZ_MEMORY_GPU_ONLY, &bufferCreateInfo, &indexBuffer);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezCreateBuffer for index buffer error");
    exit(1);
  }

  result = vezBufferSubData(VKDevice, indexBuffer, 0, bufferCreateInfo.size, static_cast<const void*>(buffer.getIndices()));
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezBufferSubData for index buffer error");
    exit(1);
  }

  auto it = std::find_if(PassMeshBuffers.begin(), PassMeshBuffers.end(), [pass] (const auto& buffer) {
    return buffer.Pass == pass;
  });
  if (it != PassMeshBuffers.end())
  {
    it->MeshBuffers[buffer.getID()] = { vertexBuffer, indexBuffer };
  }
  else
  {
    PassMeshBuffers.push_back({ pass });
    PassMeshBuffers.back().MeshBuffers[buffer.getID()] = { vertexBuffer, indexBuffer };
  }
}

SGPUResource::HandleType CVulkanDriver::createResource(SRenderPass&& pass)
{
  auto handle = CVideoDriver::createResource(std::move(pass));
  Pipelines[handle] = {};
  auto& renderPass = RenderPasses[handle];
  VezFramebuffer framebuffer;

  std::vector<VkImageView> attachments;
  for (auto& att : renderPass.ColorAttachments)
  {
    if (att != NULL_GPU_RESOURCE_HANDLE)
      attachments.push_back(VkTextures.at(att).ImageView);
  }
  if (renderPass.DepthStencilAttachment != NULL_GPU_RESOURCE_HANDLE)
    attachments.push_back(VkTextures.at(renderPass.DepthStencilAttachment).ImageView);

  if (attachments.empty())
    return handle;

  VezFramebufferCreateInfo framebufferCreateInfo = {};
  framebufferCreateInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
  framebufferCreateInfo.pAttachments = attachments.data();
  framebufferCreateInfo.width = getWidth();
  framebufferCreateInfo.height = getHeight();
  framebufferCreateInfo.layers = 1;
  auto result = vezCreateFramebuffer(VKDevice, &framebufferCreateInfo, &framebuffer);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vkCreateFramebuffer failed %d", result);
    return NULL_GPU_RESOURCE_HANDLE;
  }
  VkFramebuffers[handle] = framebuffer;
  return handle;
}

SGPUResource::HandleType CVulkanDriver::createResource(SPipeline&& pipeline)
{
  auto handle = CVideoDriver::createResource(std::move(pipeline));
  auto& shaderModules = VkShaderModules[Pipelines[handle].Shaders];
  std::vector<VezPipelineShaderStageCreateInfo> shaderStageCreateInfo;
  for (const auto& shaderModule : shaderModules)
  {
    VezPipelineShaderStageCreateInfo shaderStage;
    shaderStage.module = shaderModule;
    shaderStage.pEntryPoint = "main";
    shaderStage.pSpecializationInfo = nullptr;
    shaderStageCreateInfo.push_back(shaderStage);
  }
  
  VezGraphicsPipelineCreateInfo pipelineCreateInfo = {};
  pipelineCreateInfo.stageCount = static_cast<uint32_t>(shaderStageCreateInfo.size());
  pipelineCreateInfo.pStages = shaderStageCreateInfo.data();
  VulkanPipeline vulkanPipeline;
  if (vezCreateGraphicsPipeline(VKDevice, &pipelineCreateInfo, &vulkanPipeline.Handle) != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_ERROR, "vkCreateGraphicsPipeline failed");
    return false;
  }

  std::uint32_t vertexInputSize = 0;
  std::vector<VkVertexInputAttributeDescription> attribDesc;
  for (const auto& attr : pipeline.Layout.Attributes)
  {
    if (attr.Type != E_ATTRIBUTE_TYPE::INVALID)
    {
      vertexInputSize += GetVertexAttributeSize(attr.Format);
      attribDesc.push_back(
        { attr.Binding, 0, toVkFormat(attr.Format), attr.Offset }
      );
    }
  }

  vulkanPipeline.VertexInputBinding = {
    0, vertexInputSize, VK_VERTEX_INPUT_RATE_VERTEX
  };

  VezVertexInputFormatCreateInfo vertexInputFormatCreateInfo = {};
  vertexInputFormatCreateInfo.vertexBindingDescriptionCount = 1;
  vertexInputFormatCreateInfo.pVertexBindingDescriptions = &vulkanPipeline.VertexInputBinding;
  vertexInputFormatCreateInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attribDesc.size());
  vertexInputFormatCreateInfo.pVertexAttributeDescriptions = attribDesc.data();
  auto result = vezCreateVertexInputFormat(VKDevice, &vertexInputFormatCreateInfo, &vulkanPipeline.VertexInputFormat);

  VkPipelines[handle] = vulkanPipeline;
  return handle;
}

VkShaderModule CVulkanDriver::createShader(ShaderHandle shader, VkShaderStageFlagBits stage)
{
  auto& shaders = Shaders[shader];
  const char* code = nullptr;
  std::size_t codeSize = 0;
  bool isCompute = false;
  switch (stage)
  {
    case VK_SHADER_STAGE_VERTEX_BIT:
    {
      code = shaders.VSSource.c_str();
      codeSize = shaders.VSSource.size();
    } break;

    case VK_SHADER_STAGE_GEOMETRY_BIT:
    {
      code = shaders.GSSource.c_str();
      codeSize = shaders.GSSource.size();
    } break;

    case VK_SHADER_STAGE_FRAGMENT_BIT:
    {
      code = shaders.FSSource.c_str();
      codeSize = shaders.FSSource.size();
    } break;

    case VK_SHADER_STAGE_COMPUTE_BIT:
    {
      isCompute = true;
      code = shaders.CSource.c_str();
      codeSize = shaders.CSource.size();
    } break;
  }

  VezShaderModuleCreateInfo createInfo = {};
  createInfo.stage = stage;
  createInfo.pGLSLSource = code;
  createInfo.codeSize = codeSize;
  createInfo.pEntryPoint = "main";

  VkShaderModule shaderModule = VK_NULL_HANDLE;
  auto result = vezCreateShaderModule(VKDevice, &createInfo, &shaderModule);
  if (result != VK_SUCCESS && shaderModule != VK_NULL_HANDLE)
  {
    // If shader module creation failed but error is from GLSL compilation, get the error log.
    uint32_t infoLogSize = 0;
    vezGetShaderModuleInfoLog(shaderModule, &infoLogSize, nullptr);

    std::string infoLog(infoLogSize, '\0');
    vezGetShaderModuleInfoLog(shaderModule, &infoLogSize, &infoLog[0]);
    vezDestroyShaderModule(VKDevice, shaderModule);

    SDL_LogError(SDL_LOG_CATEGORY_RENDER, infoLog.c_str());
    return VK_NULL_HANDLE;
  }
  return shaderModule;
}

SGPUResource::HandleType CVulkanDriver::createResource(SShaderUniform&& uniform)
{
  auto handle = CVideoDriver::createResource(std::move(uniform));
  auto& uniformInfo = ShaderUniforms[handle];
  VkBuffer ubo;

  VezBufferCreateInfo bufferCreateInfo = {};
  bufferCreateInfo.size = uniformInfo.Size;
  bufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
  auto result = vezCreateBuffer(VKDevice, VEZ_MEMORY_CPU_TO_GPU, &bufferCreateInfo, &ubo);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezCreateBuffer for model matrix buffer error");
    exit(1);
  }
  VkShaderUniforms[handle] = ubo;
  return handle;
}

void CVulkanDriver::updateShaderUniform(ShaderUniformHandle uniformHandle, void* data)
{
  void* buffer = nullptr;
  auto& ubo = VkShaderUniforms[uniformHandle];
  auto& uniform = ShaderUniforms[uniformHandle];
  auto result = vezMapBuffer(VKDevice, ubo, 0, uniform.Size, &buffer);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vkMapBuffer for uniform failed");
    exit(1);
  }

  memcpy(buffer, data, uniform.Size);
  vezUnmapBuffer(VKDevice, ubo);
}

void CVulkanDriver::bindTexture(TextureHandle texture, int binding)
{
  const auto& vkTexture = VkTextures.at(texture);
  vezCmdBindImageView(vkTexture.ImageView, vkTexture.Sampler, 0, binding, 0);
}

void CVulkanDriver::bindShaderUniform(ShaderUniformHandle uniform, int binding)
{
  auto& ubo = VkShaderUniforms[uniform];
  vezCmdBindBuffer(ubo, 0, VK_WHOLE_SIZE, 0, binding, 0);
}

SGPUResource::HandleType CVulkanDriver::createResource(SShader&& shader)
{
  auto handle = CVideoDriver::createResource(std::move(shader));

  auto& shaderSource = Shaders[handle];
  auto& shaderModules = VkShaderModules[handle];

  if (shaderSource.VSSource.empty() == false)
    shaderModules.push_back(createShader(handle, VK_SHADER_STAGE_VERTEX_BIT));
  if (shaderSource.GSSource.empty() == false)
    shaderModules.push_back(createShader(handle, VK_SHADER_STAGE_GEOMETRY_BIT));
  if (shaderSource.FSSource.empty() == false)
    shaderModules.push_back(createShader(handle, VK_SHADER_STAGE_FRAGMENT_BIT));
  if (shaderSource.CSource.empty() == false)
    shaderModules.push_back(createShader(handle, VK_SHADER_STAGE_COMPUTE_BIT));

  return handle;
}

TextureHandle CVulkanDriver::createTexture(STexture&& texture)
{
  auto handle = CVideoDriver::createTexture(std::move(texture));
  CVulkanDriver::createTexture(handle);
  return handle;
}

TextureHandle CVulkanDriver::createTexture(const std::string& path)
{
  auto handle = CVideoDriver::createTexture(path);
  CVulkanDriver::createTexture(handle);
  return handle;
}

TextureHandle CVulkanDriver::createTexture(unsigned char* data, std::size_t size)
{
  auto handle = CVideoDriver::createTexture(data, size);
  CVulkanDriver::createTexture(handle);
  return handle;
}

void CVulkanDriver::createTexture(TextureHandle textureHandle)
{
  auto& texture = Textures[textureHandle];
  VulkanTexture vkTexture;
  auto& content = texture.Contents[0][0];

  VezImageCreateInfo imageCreateInfo = {};
  imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
  imageCreateInfo.format = toVkFormat(texture.Format);
  imageCreateInfo.extent = { static_cast<uint32_t>(texture.Width), static_cast<uint32_t>(texture.Height), 1 };
  imageCreateInfo.mipLevels = texture.MipMapCount;
  imageCreateInfo.arrayLayers = 1;
  imageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
  imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;

  if (texture.IsRenderTarget)
  {
    const auto usageFlag = VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
    if (texture.IsDepthAttachment)
    {
      imageCreateInfo.usage = usageFlag | VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
    }
    else
    {
      imageCreateInfo.usage = usageFlag | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    }
  }
  else
  {
    imageCreateInfo.usage = VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
  }

  auto result = vezCreateImage(VKDevice, VEZ_MEMORY_GPU_ONLY, &imageCreateInfo, &vkTexture.Image);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezCreateImage failed");
  }

  if (texture.IsRenderTarget == false)
  {
    VezImageSubDataInfo subDataInfo = {};
    subDataInfo.imageSubresource.mipLevel = 0;
    subDataInfo.imageSubresource.baseArrayLayer = 0;
    subDataInfo.imageSubresource.layerCount = 1;
    subDataInfo.imageOffset = { 0, 0, 0 };
    subDataInfo.imageExtent = { imageCreateInfo.extent.width, imageCreateInfo.extent.height, 1 };
    result = vezImageSubData(VKDevice, vkTexture.Image, &subDataInfo, reinterpret_cast<const void*>(content.Data.data()));
    if (result != VK_SUCCESS)
    {
      SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezImageSubData failed");
    }
  }

  VezImageViewCreateInfo imageViewCreateInfo = {};
  imageViewCreateInfo.image = vkTexture.Image;
  imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
  imageViewCreateInfo.format = imageCreateInfo.format;
  imageViewCreateInfo.subresourceRange.layerCount = 1;
  imageViewCreateInfo.subresourceRange.levelCount = imageCreateInfo.mipLevels;
  result = vezCreateImageView(VKDevice, &imageViewCreateInfo, &vkTexture.ImageView);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezCreateImageView failed");
  }

  // FIXME(manh): use 1 sampler for all images?
  VezSamplerCreateInfo samplerCreateInfo = {};
  samplerCreateInfo.magFilter = VK_FILTER_LINEAR;
  samplerCreateInfo.minFilter = VK_FILTER_LINEAR;
  samplerCreateInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
  samplerCreateInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
  samplerCreateInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
  samplerCreateInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
  samplerCreateInfo.minLod = 0.0f;
  samplerCreateInfo.maxLod = imageCreateInfo.mipLevels;
  result = vezCreateSampler(VKDevice, &samplerCreateInfo, &vkTexture.Sampler);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezCreateSampler failed");
  } else
  {
    VkTextures[textureHandle] = vkTexture;
  }
}

void CVulkanDriver::beginPass(RenderPassHandle handle)
{
  CVideoDriver::beginPass(handle);
  vezResetCommandBuffer(CommandBuffer);

  // Set the viewport state and dimensions.
  auto width = Device.getWidth();
  auto height = Device.getHeight();

  const auto& pass = RenderPasses.at(CurrentPass);
  const auto& clear = pass.State.Colors[0].Values;

  VkClearColorValue clearColor = {
    { clear.x, clear.y, clear.z, clear. w }
  };

  // Begin command buffer recording.
  auto result  = vezBeginCommandBuffer(CommandBuffer, VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT);
  if (result != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezBeginCommandBuffer failed");
    exit(1);
  }

  VkViewport viewport = { 0.0f, 0.0f, static_cast<float>(width), static_cast<float>(height), 0.0f, 1.0f };
  VkRect2D scissor = { { 0, 0 },{ static_cast<uint32_t>(width), static_cast<uint32_t>(height) } };
  vezCmdSetViewport(0, 1, &viewport);
  vezCmdSetScissor(0, 1, &scissor);
  vezCmdSetViewportState(1);

  // Define clear values for the swapchain's color and depth attachments.
  std::vector<VezAttachmentReference> attachmentReferences;

  if (pass.UseDefaultAttachments)
  {
    attachmentReferences.resize(2);
    attachmentReferences[0].clearValue.color = clearColor;
    attachmentReferences[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    attachmentReferences[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    attachmentReferences[1].clearValue.depthStencil.depth = pass.State.Depth.Value;
    attachmentReferences[1].clearValue.depthStencil.stencil = pass.State.Stencil.Value;
    attachmentReferences[1].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    attachmentReferences[1].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
  }
  else
  {
    int i = 0;
    for (const auto& att: pass.ColorAttachments)
    {
      if (att != NULL_GPU_RESOURCE_HANDLE)
      {
        const auto& color = pass.State.Colors.at(i).Values;
        VezAttachmentReference ref;
        ref.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        ref.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        ref.clearValue.color = { color.x, color.y, color.z, color.w };
        attachmentReferences.push_back(std::move(ref));
      }
      ++i;
    }
    if (pass.DepthStencilAttachment != NULL_GPU_RESOURCE_HANDLE)
    {
      VezAttachmentReference ref;
      ref.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
      ref.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
      ref.clearValue.depthStencil.depth = pass.State.Depth.Value;
      ref.clearValue.depthStencil.stencil = pass.State.Stencil.Value;
      attachmentReferences.push_back(std::move(ref));
    }
  }

  VezImageSubresourceRange range = {};
  range.layerCount = 1;
  range.levelCount = 1;

  auto smgr = std::dynamic_pointer_cast<scene::CSceneManager>(Device.getSceneManager());

  if (smgr->isEmptyPass(pass.Handle))
  {
    vezCmdClearColorImage(DefaultFramebuffer.colorImage, &clearColor, 1, &range);
    return;
  }

  // Begin a render pass.
  VezRenderPassBeginInfo beginInfo = {};
  beginInfo.framebuffer = pass.UseDefaultAttachments ? DefaultFramebuffer.handle : VkFramebuffers[pass.Handle];
  beginInfo.attachmentCount = static_cast<uint32_t>(attachmentReferences.size());
  beginInfo.pAttachments = attachmentReferences.data();
  vezCmdBeginRenderPass(&beginInfo);
}

void CVulkanDriver::endPass(bool render)
{
  CVideoDriver::endPass();
  // End the render pass.
  if (render)
    vezCmdEndRenderPass();

  // End command buffer recording.
  if (vezEndCommandBuffer() != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezEndCommandBuffer failed");
    exit(1);
  }
}

void CVulkanDriver::submit()
{
  // Submit the command buffer to the graphics queue.
  VezSubmitInfo submitInfo = {};
  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers = &CommandBuffer;

  // Request a wait semaphore to pass to present so it waits for rendering to complete.
  submitInfo.signalSemaphoreCount = 1;
  submitInfo.pSignalSemaphores = &Semaphore;
  if (vezQueueSubmit(GraphicsQueue, 1, &submitInfo, nullptr) != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezQueueSubmit failed");
    exit(1);
  }
}

void CVulkanDriver::present(TextureHandle texture)
{
  vezDeviceWaitIdle(VKDevice);

  // Present the swapchain framebuffer to the window.
  VkPipelineStageFlags waitDstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

  VezPresentInfo presentInfo = {};
  presentInfo.waitSemaphoreCount = 1;
  presentInfo.pWaitSemaphores = &Semaphore;
  presentInfo.pWaitDstStageMask = &waitDstStageMask;
  presentInfo.swapchainCount = 1;
  presentInfo.pSwapchains = &Swapchain;
  presentInfo.pImages = {
    texture == NULL_GPU_RESOURCE_HANDLE ?
    &DefaultFramebuffer.colorImage :
    &VkTextures[texture].Image
  };

  if (vezQueuePresent(GraphicsQueue, &presentInfo) != VK_SUCCESS)
  {
    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "vezQueuePresentKHR failed");
    exit(1);
  }
}

void CVulkanDriver::copyTexture(
  TextureHandle srcTex, TextureHandle dstTex,
  const glm::ivec2& srcOffset, const glm::ivec2& dstOffset, const glm::ivec2& size)
{
  VezImageCopy copyInfo = {};
  copyInfo.srcSubresource.mipLevel = 0;
  copyInfo.srcSubresource.baseArrayLayer = 0;
  copyInfo.srcSubresource.layerCount = 1;
  copyInfo.srcOffset.x = srcOffset.x;
  copyInfo.srcOffset.y = srcOffset.y;
  copyInfo.srcOffset.z = 0;

  copyInfo.dstSubresource.mipLevel = 0;
  copyInfo.dstSubresource.baseArrayLayer = 0;
  copyInfo.dstSubresource.layerCount = 1;
  copyInfo.dstOffset.x = dstOffset.x;
  copyInfo.dstOffset.y = dstOffset.y;
  copyInfo.dstOffset.z = 0;
  copyInfo.extent.width = size.x != 0 ? size.x : Textures.at(srcTex).Width;
  copyInfo.extent.height = size.y != 0 ? size.y : Textures.at(srcTex).Height;
  copyInfo.extent.depth = 1;

  vezCmdCopyImage(VkTextures.at(srcTex).Image, VkTextures.at(dstTex).Image, 1, &copyInfo);
}

void CVulkanDriver::blitTexture(
  TextureHandle srcTex, TextureHandle dstTex,
  const glm::ivec2& srcOffset, const glm::ivec2& dstOffset)
{
  VezImageBlit blitInfo = {};
  blitInfo.srcSubresource.mipLevel = 0;
  blitInfo.srcSubresource.baseArrayLayer = 0;
  blitInfo.srcSubresource.layerCount = 1;
  blitInfo.srcOffsets[1].x = srcOffset.x != 0 ? srcOffset.x : Textures.at(srcTex).Width;
  blitInfo.srcOffsets[1].y = srcOffset.y != 0 ? srcOffset.y : Textures.at(srcTex).Height;
  blitInfo.srcOffsets[1].z = 1;

  blitInfo.dstSubresource.mipLevel = 0;
  blitInfo.dstSubresource.baseArrayLayer = 0;
  blitInfo.dstSubresource.layerCount = 1;
  blitInfo.dstOffsets[1].x = dstOffset.x != 0 ? dstOffset.x : Textures.at(dstTex).Width;
  blitInfo.dstOffsets[1].y = dstOffset.y != 0 ? dstOffset.y : Textures.at(dstTex).Height;
  blitInfo.dstOffsets[1].z = 1;

  vezCmdBlitImage(VkTextures.at(srcTex).Image, VkTextures.at(dstTex).Image, 1, &blitInfo, VK_FILTER_LINEAR);
}

VkFormat toVkFormat(const E_ATTRIBUTE_FORMAT format)
{
  switch (format)
  {
    // FIXME(manh): add more attribute format
    case E_ATTRIBUTE_FORMAT::FLOAT2: return VK_FORMAT_R32G32_SFLOAT;
    case E_ATTRIBUTE_FORMAT::FLOAT3: return VK_FORMAT_R32G32B32_SFLOAT;
  }
}

VkFormat toVkFormat(const E_PIXEL_FORMAT format)
{
  switch (format)
  {
    // FIXME(manh): add pixel attribute format
    case E_PIXEL_FORMAT::D32: return VK_FORMAT_D32_SFLOAT;
    case E_PIXEL_FORMAT::R8G8B8A8: return VK_FORMAT_R8G8B8A8_UNORM;
    case E_PIXEL_FORMAT::B8G8R8A8: return VK_FORMAT_B8G8R8A8_UNORM;
  }
}

} // namespace video
} // namespace saga
