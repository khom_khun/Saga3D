# Project Saga3D, led by [InnerPiece](https://innerpiece.io/open-source/)

[Mailing list](https://groups.google.com/forum/#!forum/innerpiece_oss)

[Discord](https://discord.gg/QdY7tuJ)

## Goals
Provide a minimal, simple and cross-platform graphics library for PC, Mobile and probably Web (WebGL/WASM or another modern API if we can wait for).

Architecture is based on [Irrlicht](http://irrlicht.sourceforge.net/)'s design.
API is inspired by [Sokol](https://github.com/floooh/sokol-samples/).

Based on Irrlicht (1.9 trunk branch, revision 5616)

## Why another graphics library?

Since Vulkan's release, the quest for leaving OpenGL behind has started for many people, including us. More over, I find most of cross-platform graphics libraries are either simple (and not flexible or cross-platform) or complex (too many supported APIs and dependencies).

Saga3D is designed to have the simplicity in API like Irrlicht and cross-platform power of Vulkan and SDL2. We try to avoid as many dependencies as possible and provide ability to extend Saga3D to your own case.

## Status

Initial work happens on Linux, then will be ported to other platforms. I use Ubuntu 18.04 and haven't started to test on other OSs yet.

See some working [Samples](https://gitlab.com/InnerPieceOSS/Saga3D/tree/master/samples).

On-going Android [port](https://gitlab.com/InnerPieceOSS/Saga3D/issues/4).

Windows version is working (since Dec 05 2018) with [MSYS2](http://www.msys2.org/).

Visual Studio is also working (since Dec 27 2018, only tested with 64-bit configuration). Please use[vcpkg](https://github.com/Microsoft/vcpkg) to install dependencies.

See [milestones](https://gitlab.com/InnerPieceOSS/Saga3D/milestones).

## Start using Saga3D

Make sure you have learnt (and probably master) these topics:
- C++14 programming
- OpenGL Shading Language (GLSL) 4.x
- Graphics programming (depends on what you want to achieve with Saga3D)

## Development

I encourage contributors to follow example-oriented rule.
It's simple as "Always start with an example" :
- Think about what user will see on the screen
- Refer to folder `old/examples` to look for ideas
- Design engine behaviors
- Write up an examples in folder `samples`
- See how people do it in Sokol or other similar API
- Finish writing the sample
- Modify current code base's interface
- Start implementing features
- Remember to preserve coding style

And we would like to work with small commit, revertable change and clear commit message.

## Dependencies

Libraries you need to install: `libsdl2-dev, libglm-dev, libvulkan-dev, libassimp-dev`

Tools: Git, Git LFS, CMake 3.12 or later, C++14 compiler.

Other dependencies (build process will add automatically):
- [V-EZ](https://github.com/GPUOpen-LibrariesAndSDKs/V-EZ)
- [std_image](https://github.com/nothings/stb)

## Building

- Clone the repository
- git lfs pull
- mkdir build
- cd build
- cmake .. -DCMAKE_INSTALL_PREFIX=`_ABSOLUTE_INSTALL_PATH_` <br />
(This command will clone V-EZ or pull latest changes if it's the second CMake call, build type argument you enters is also used to build V-EZ) <br />
Add `-G "MinGW Makefiles"` if building on Windows with MSYS2
- make -j4 install <br/>
Library will be built and installed to your `_ABSOLUTE_INSTALL_PATH_`
- cd `_ABSOLUTE_INSTALL_PATH_/bin` <br />
(You can run some samples here now)

## Building on Windows with Visual Studio

- Install [vcpkg](https://github.com/Microsoft/vcpkg)
- Install [Vulkan SDK](https://vulkan.lunarg.com/sdk/home)
- Use vcpkg to install: assimp, sdl2, glm, vulkan
- Add additional parameters to CMake call: `-G "Visual Studio 15 2017 Win64" -DCMAKE_TOOLCHAIN_FILE=_VCPKG_INSTALL_PATH_/scripts/buildsystems/vcpkg.cmake`
- Build
- Execute Visual Studio target INSTALLL (right click on INSTALL -> Debug -> Start new instance)

# Android

Install Android Studio, SDK, NDK

Install CMake and Ninja and have symlinks at /usr/local/bin/


## Build Saga3D
```
export NDK_PATH=/home/manh/android/sdk/ndk-bundle
export CMAKE_TOOLCHAIN=$NDK_PATH/build/cmake/android.toolchain.cmake

cmake .. \
-DCMAKE_TOOLCHAIN_FILE=${CMAKE_TOOLCHAIN} \
-DCMAKE_SYSTEM_NAME=Android -DCMAKE_ANDROID_NDK=${NDK_PATH} \
-DANDROID_FORCE_ARM_BUILD=TRUE -DANDROID_STL=c++_shared -DANDROID_TOOLCHAIN=clang \
-DANDROID_NATIVE_API_LEVEL=27 -DCMAKE_ANDROID_ARCH_ABI=armeabi-v7a \
-DCMAKE_INSTALL_PREFIX=$PWD/install -DCMAKE_BUILD_TYPE=Release
```
## Build Android Dependencies
(Already included in the repo)

### Build assimp
```
export NDK_PATH=/home/manh/android/sdk/ndk-bundle
export CMAKE_TOOLCHAIN=$NDK_PATH/build/cmake/android.toolchain.cmake

cmake .. \
-DCMAKE_TOOLCHAIN_FILE=${CMAKE_TOOLCHAIN} \
-DCMAKE_SYSTEM_NAME=Android -DCMAKE_ANDROID_NDK=${NDK_PATH} \
-DANDROID_FORCE_ARM_BUILD=TRUE -DANDROID_STL=c++_shared -DANDROID_TOOLCHAIN=clang \
-DANDROID_NATIVE_API_LEVEL=27 -DCMAKE_ANDROID_ARCH_ABI=armeabi-v7a \
-DCMAKE_INSTALL_PREFIX=$PWD/install -DCMAKE_BUILD_TYPE=Release \
-DASSIMP_BUILD_TESTS=OFF -DASSIMP_BUILD_ASSIMP_TOOLS=OFF \
-DASSIMP_BUILD_ALL_IMPORTERS_BY_DEFAULT=FALSE \
-DASSIMP_NO_EXPORT=ON \
-DASSIMP_BUILD_OBJ_IMPORTER=ON

make install
```
### Build SDL2
```
ndk-build NDK_PROJECT_PATH=. APP_BUILD_SCRIPT=./Android.mk APP_PLATFORM=android-27
```