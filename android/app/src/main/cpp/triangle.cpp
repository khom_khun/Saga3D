#include <Saga.h>
#include <chrono>

using namespace saga;
using namespace core;
using namespace video;

int main(int argc, char* argv[]) {
  auto device = createDevice(E_DRIVER_TYPE::VULKAN, {800, 600}, 16, false, false, false);
  device->setWindowCaption("Saga 3D Triangle");

  auto& driver = device->getVideoDriver();
  auto& smgr = device->getSceneManager();

  std::vector<scene::S3DVertex> Vertices;

  Vertices.push_back({
    0.0f, 0.5f, 0.5f
  });

  Vertices.push_back({
    0.5f, -0.5f, 0.5f
  });

  Vertices.push_back({
    -0.5f, -0.5f, 0.5f
  });

  std::vector<std::uint32_t> Indices = { 0, 1, 2 };

  auto mesh = std::make_shared<scene::SMesh>();
  auto meshBuffer = std::make_unique<scene::CMeshBuffer>();
  auto vertCount = Vertices.size();
  auto indexCount = Indices.size();
  meshBuffer->append(std::move(Vertices), vertCount, std::move(Indices), indexCount);
  mesh->addMeshBuffer(std::move(meshBuffer));

  auto node = smgr->createMeshSceneNode(mesh);

  auto shaderInfo = driver->createShader();
  shaderInfo.VSSource = R"(
    #version 450
    #extension GL_ARB_separate_shader_objects : enable
    layout (location = 0) in vec4 position;
    void main() {
      gl_Position = position;
    }
  )";

  shaderInfo.FSSource = R"(
    #version 450
    #extension GL_ARB_separate_shader_objects : enable
    layout (location = 0) out vec4 frag_color;
    void main() {
      frag_color = vec4(0.0, 1.0, 0.0, 1.0);
    }
  )";

  auto pipelineInfo = driver->createPipeline();
  pipelineInfo.Shaders = driver->createResource(std::move(shaderInfo));

  pipelineInfo.Layout.Attributes[0] = {
    E_ATTRIBUTE_TYPE::POSITION,
    E_ATTRIBUTE_FORMAT::FLOAT3,
    0, 0
  };
  // Setting Attributes[0] is optional, default is exactly like above

  auto passInfo = driver->createRenderPass();
  passInfo.PassPipeline = driver->createResource(std::move(pipelineInfo));

  auto pass = driver->createResource(std::move(passInfo));
  smgr->registerSceneNode(node, pass);

  auto Then = std::chrono::high_resolution_clock::now();
  while (device->run()) {
    driver->beginPass(pass);
    auto Now = std::chrono::high_resolution_clock::now();
    auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(Now - Then).count();
    Then = Now;
    if (millis > 0)
    {
      auto sec = millis / 1000.f;
      int fps = 60.f / sec;
      device->setWindowCaption("Saga 3D Triangle- FPS: " + std::to_string(fps));
    }
    driver->endPass();
  }

  return 0;
}

